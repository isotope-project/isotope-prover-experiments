use isotope_core::cons::HashCtx;
use isotope_core::subst::SubstStack;
use isotope_core::{term::*, Error};
use smallvec::smallvec;

#[test]
fn succ_rid_subst() {
    let mut ctx = HashCtx::default();
    let nats = TermId::nats_in(&mut ctx);
    let unary = nats.unary_in(&mut ctx).unwrap();
    let zero = nats.con_in(0, &mut ctx).unwrap();
    let succ = nats.con_in(1, &mut ctx).unwrap();
    let one = succ.app_in(zero.clone(), &mut ctx).unwrap();
    let nats_0 = nats.var_in(0, &mut ctx).unwrap();
    let succ_branch = Lambda::try_new(
        nats.clone(),
        succ.app_in(
            unary
                .var_rec_in(2, 0, &mut ctx)
                .unwrap()
                .app_in(nats_0.clone(), &mut ctx)
                .unwrap(),
            &mut ctx,
        )
        .unwrap(),
        None,
        &mut ctx,
    )
    .unwrap()
    .into_id_in(&mut ctx);
    let rid_result = Case::try_new(
        nats.clone(),
        nats.const_over_in(nats.clone(), &mut ctx).unwrap(),
        None,
        &mut ctx,
    )
    .unwrap()
    .into_id_in(&mut ctx)
    .app_in(zero.clone(), &mut ctx)
    .unwrap()
    .app_in(succ_branch.clone(), &mut ctx)
    .unwrap()
    .app_in(nats_0.clone(), &mut ctx)
    .unwrap();
    let rid_fn = Lambda::try_new(nats.clone(), rid_result, None, &mut ctx)
        .unwrap()
        .into_id_in(&mut ctx);
    let rid_def = RecDef::try_new(smallvec![rid_fn])
        .unwrap()
        .into_id_in(&mut ctx);
    let rid = rid_def.rec_in(0, &mut ctx).unwrap();
    assert_eq!(rid.dec_args().unwrap(), Some(&[0][..]));

    let rid_v = rid.app_in(nats_0, &mut ctx).unwrap();
    let succ_rid_v = succ.app_in(rid_v, &mut ctx).unwrap();

    let rid_0 = rid.app_in(zero.clone(), &mut ctx).unwrap();
    let succ_rid_0 = succ.app_in(rid_0, &mut ctx).unwrap();

    let rid_1 = rid.app_in(one.clone(), &mut ctx).unwrap();
    let succ_rid_1 = succ.app_in(rid_1, &mut ctx).unwrap();

    assert_eq!(
        succ_rid_v
            .subst_slice_consed(std::slice::from_ref(&zero), 0, &mut ctx)
            .unwrap(),
        succ_rid_0
    );

    assert_eq!(
        succ_rid_v
            .subst_slice_consed(std::slice::from_ref(&one), 0, &mut ctx)
            .unwrap(),
        succ_rid_1
    );

    let succ_rid = Lambda::try_new(nats.clone(), succ_rid_v, None, &mut ctx)
        .unwrap()
        .into_id_in(&mut ctx);

    let mut ctx = SubstStack::new(&mut ctx);
    let mut argv = vec![zero.clone()];
    assert_eq!(succ_rid.applied(&mut argv, &mut ctx).unwrap(), succ_rid_0);
    assert_eq!(argv.len(), 0);
    argv.push(one.clone());
    assert_eq!(succ_rid.applied(&mut argv, &mut ctx).unwrap(), succ_rid_1);
    assert_eq!(argv.len(), 0);
}

/// The function
/// ```text
/// #let bad_mul: nats -> nats -> nats =  
///        λm: nats => λn: nats => #case nats (λ_:nats => nats) zero (λn' => add m (bad_mul m n)) n;
/// ```
#[test]
fn bad_mul() {
    let mut ctx = HashCtx::default();
    let nats = TermId::nats_in(&mut ctx);
    let add = TermId::left_add_in(&mut ctx);
    let nat_one = nats.var_in(1, &mut ctx).unwrap();
    let nat_two = nats.var_in(2, &mut ctx).unwrap();
    let zero = nats.con_in(0, &mut ctx).unwrap();
    let zero_branch = zero;
    let recursive_call = nats
        .binary_in(&mut ctx)
        .unwrap()
        .var_rec_in(3, 0, &mut ctx)
        .unwrap()
        .app_in(nat_two, &mut ctx)
        .unwrap()
        .app_in(nat_one.clone(), &mut ctx)
        .unwrap();
    let succ_branch = add
        .app_in(nat_one.clone(), &mut ctx)
        .unwrap()
        .app_in(recursive_call, &mut ctx)
        .unwrap()
        .abs_in(nats.clone(), &mut ctx)
        .unwrap();

    let case = nats
        .case_in(nats.const_over_in(nats.clone(), &mut ctx).unwrap(), &mut ctx)
        .unwrap()
        .app_in(zero_branch, &mut ctx)
        .unwrap()
        .app_in(succ_branch, &mut ctx)
        .unwrap()
        .app_in(nat_one, &mut ctx)
        .unwrap();
    let bad_mul = case
        .abs_in(nats.clone(), &mut ctx)
        .unwrap()
        .abs_in(nats, &mut ctx)
        .unwrap();
    let err = isotope_core::termination::make_def_graph(&[bad_mul.clone()]).unwrap_err();
    assert_eq!(err, Error::Nonterminating);
}
