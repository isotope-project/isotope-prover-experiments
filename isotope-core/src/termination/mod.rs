/*!
A termination checker implementation for `isotope` terms, based off [foetus](https://www.cs.cmu.edu/~abel/foetus.ps).
*/
use isotope_termck::{ArgRelation, CallGraph, CallMat, DefinitionGraph, Relation};

use crate::*;

/// A helper to construct the definition-graph for a set of mutually recursive definitions
pub fn make_def_graph(defs: &[TermRef]) -> Result<DefinitionGraph, Error> {
    let call_graph = make_call_graph(defs)?;
    let (definition_graph, failures) = call_graph.condense(true);
    if failures.is_some() {
        //TODO: improve
        Err(Error::Nonterminating)
    } else {
        Ok(definition_graph)
    }
}

/// A helpter to construct the call-graph for a set of mutually recursive definitions
pub fn make_call_graph(defs: &[TermRef]) -> Result<CallGraph, Error> {
    let no_defs = defs.len().try_into().map_err(|_| Error::VarIxOverflow)?;
    let no_args = defs
        .iter()
        .map(|def| {
            if let Term::Lambda(_) = &**def {
                def.params()
                    .count()
                    .try_into()
                    .map_err(|_| Error::VarIxOverflow)
            } else {
                Ok(0)
            }
        })
        .collect::<Result<Vec<VarIx>, Error>>()?;
    let mut call_graph = CallGraph::with_capacity(defs.len(), 0);
    for _ in 0..no_defs {
        call_graph.add_node();
    }

    let mut builder = RelationBuilder {
        defs,
        no_args,
        def_ix: 0,
        base: Filter::EMPTY,
        var_relations: Vec::new(),
        call_graph,
    };
    for (ix, def) in defs.iter().enumerate() {
        builder.register_def(ix as VarIx, def)?;
    }

    Ok(builder.call_graph)
}

/// A builder for relations between mutually recursive definitions
/// TODO: convert to a more generic call visitor?
#[derive(Debug, Clone)]
struct RelationBuilder<'a> {
    /// The provided set of mutually recursive definitions
    defs: &'a [TermRef<'a>],
    /// The number of arguments for each definition, stored along with the definition root
    no_args: Vec<VarIx>,
    /// The current definition index for this builder
    def_ix: VarIx,
    /// The base filter
    base: Filter,
    /// The current stack of relations for non-argument variables
    var_relations: Vec<(ArgRelation<VarIx>, Filter)>,
    /// The current call graph
    call_graph: CallGraph,
}

/// A recursive dependency between mutually recursive definition
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, PartialOrd, Ord)]
struct RecursiveDep {
    /// The caller
    pub caller: VarIx,
    /// The callee
    pub callee: VarIx,
    /// The caller argument being used
    pub caller_arg: VarIx,
    /// The callee argument
    pub callee_arg: VarIx,
}

impl RelationBuilder<'_> {
    /// Process a definition
    #[inline]
    fn register_def(&mut self, def_ix: VarIx, term: &TermRef<'_>) -> Result<(), Error> {
        let mut no_args = 0;
        let mut def = term;
        let mut base = Filter::singleton(0);
        while let Term::Lambda(lam) = &**def {
            def = lam.result();
            no_args += 1;
            base = base.shift_up(1).insert(0);
            if no_args == self.no_args[def_ix as usize] {
                break;
            }
        }
        //TODO: think about this...
        if no_args != self.no_args[def_ix as usize] {
            return Err(Error::ExpectedFn);
        }

        self.var_relations.clear();
        self.def_ix = def_ix;
        self.base = base;
        self.register_dep(def, self.level(), Relation::Unknown)?;
        Ok(())
    }

    /// Get the current number of arguments
    #[inline]
    fn no_args(&self) -> VarIx {
        self.no_args[self.def_ix as usize]
    }

    /// Push a variable with the given relations, at the given level
    #[inline]
    fn push_var(&mut self, relations: ArgRelation<VarIx>, level: VarIx) -> Result<(), Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);

        if !relations.is_unknown() {
            let mut filter = self.filter();
            while self.level() < level {
                filter = filter.shift_up(1);
                self.var_relations.push((ArgRelation::default(), filter))
            }
            self.var_relations
                .push((relations, filter.shift_up(1).insert(0)));
            debug_assert_eq!(self.level(), level + 1)
        }

        Ok(())
    }

    /// Pop a variable
    #[inline]
    fn pop_var(&mut self) -> Result<(), Error> {
        //TODO: error code change?
        self.var_relations.pop().ok_or(Error::ParamUnderflow)?;
        Ok(())
    }

    /// Get the current de-Bruijn level; equivalent to the de-Bruijn index of the definition
    #[inline]
    fn level(&self) -> VarIx {
        self.no_args() + self.var_relations.len() as VarIx
    }

    /// Get the current filter
    #[inline]
    fn filter(&self) -> Filter {
        if let Some((_, filter)) = self.var_relations.last() {
            *filter
        } else {
            //TODO: recompute base filter each time? Need an optimized range => filter constructor...
            self.base
        }
    }

    /// Get the relations for a given variable at a given level, multiplied by a value `r`
    #[inline]
    fn relations(
        &self,
        var: VarIx,
        level: VarIx,
        r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        if r == Relation::Unknown || var > level {
            return Ok(ArgRelation::default());
        } else if var == level {
            //TODO: pick a proper error...
            return Err(Error::ParamUnderflow);
        }
        let rev_var = level - 1 - var;
        if rev_var >= self.no_args() {
            if let Some((relations, _)) =
                self.var_relations.get((rev_var - self.no_args()) as usize)
            {
                Ok(r * relations)
            } else {
                Ok(ArgRelation::default())
            }
        } else {
            Ok(ArgRelation {
                arg: rev_var,
                rel: r,
            })
        }
    }

    /// Check whether there is a variable intersection for a given definition
    #[inline]
    fn intersects(&self, filter: Filter) -> bool {
        if let Some((_relations, def_filter)) = self.var_relations.last() {
            def_filter.intersects(filter).unwrap_or(true)
        } else {
            filter.intersects(self.base).unwrap_or(true)
        }
    }

    /// Register the recursive call relations for a given definition, for a given term, at a given level
    ///
    /// Note that all arguments must *already* be registered!
    #[inline]
    fn register_dep(
        &mut self,
        root: &TermRef<'_>,
        level: VarIx,
        r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);
        if !self.intersects(root.fv()) {
            return Ok(ArgRelation::default());
        }
        let mut callee: Args<'_> = root.args();
        let argv: SmallVec<[TermRef<'_>; 2]> = (&mut callee).collect();
        self.register_call(&callee.0, &argv[..], level, r)
    }

    /// Register the recursive call relations for the direct dependencies of a given term, but not the term itself
    #[inline]
    fn register_direct_deps(&mut self, term: &impl Value, level: VarIx) -> Result<(), Error> {
        term.visit_direct_deps(level, |is_ty, depth, dep| {
            // Ignore types
            if is_ty {
                return Ok(());
            }
            self.register_dep(&dep, depth, Relation::Unknown)?;
            Ok(())
        })
    }

    /// Register a dependency on a call for a given definition at a given depth
    #[inline]
    fn register_call(
        &mut self,
        callee: &TermRef<'_>,
        args: &[TermRef<'_>],
        level: VarIx,
        r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);
        match &**callee {
            Term::Var(callee) => return self.register_var_call(callee.ix(), args, level, r),
            Term::Case(callee) => return self.register_case(callee, args, level, r),
            Term::Rec(callee) => {
                if let Ok(var) = callee.def().as_var() {
                    if var.ix() == level {
                        //TODO: shifted comparator
                        if callee.base_ty().ok_or(Error::ExpectedTyped)?.shifted(
                            level as SVarIx,
                            0,
                            &mut (),
                        )? != self
                            .defs
                            .get(callee.ix() as usize)
                            .ok_or(Error::RecIxOob)?
                            .ty()
                            .ok_or(Error::ExpectedTyped)?
                        {
                            return Err(Error::RecVarMismatch);
                        }
                        if callee.is_rec_def_var(level) {
                            return self.register_rec(callee.ix(), args, level, r);
                        }
                    }
                }
            }
            //TODO: for coinduction, recognize constructors, and add a `Gt` relation?
            _ => {}
        }
        for arg in args.iter() {
            self.register_dep(arg, level, Relation::Unknown)?;
        }
        self.register_direct_deps(callee, level)?;
        Ok(ArgRelation::default())
    }

    /// Register a call on a variable for a given definition, for a given term, at a given depth
    #[inline]
    fn register_var_call(
        &mut self,
        var: VarIx,
        args: &[TermRef<'_>],
        level: VarIx,
        r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);

        for arg in args.iter() {
            self.register_dep(arg, level, Relation::Unknown)?;
        }

        if args.len() == 0 {
            self.relations(var, level, r)
        } else {
            //TODO: give relations to variable calls?
            Ok(ArgRelation::default())
        }
    }

    /// Register a recursive call of a given definition for a given definition at a given depth
    #[inline]
    fn register_rec(
        &mut self,
        callee: VarIx,
        args: &[TermRef<'_>],
        level: VarIx,
        _r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);

        let caller_args = self.no_args() as usize;
        let callee_args = self.no_args[callee as usize] as usize;

        //TODO: make sure we're inserting in the right order...
        let mut call_mat = CallMat::unknown(callee_args, caller_args);

        for (callee_arg, arg) in args.iter().rev().enumerate() {
            if callee_arg >= caller_args {
                self.register_dep(arg, level, Relation::Unknown)?;
                continue;
            }
            let dep = self.register_dep(arg, level, Relation::Equal)?;
            for (caller_arg, rel) in dep {
                call_mat.insert(callee_arg, caller_arg as usize, rel)
            }
        }

        if !self
            .call_graph
            .add_edge(self.def_ix as usize, callee as usize, call_mat, true)
        {
            return Err(Error::Nonterminating);
        }

        Ok(ArgRelation::default())
    }

    /// Register a `case` statement for a given definition at a given depth
    #[inline]
    fn register_case(
        &mut self,
        callee: &Case,
        mut args: &[TermRef<'_>],
        level: VarIx,
        _r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);

        //TODO: give relations to case statements?
        self.register_direct_deps(callee, level)?;
        let family = callee.family().as_ind_fam()?;
        let no_cons = family.no_cons()?;
        if args.len() > no_cons as usize {
            let (rest, branches) = args.split_at(args.len() - no_cons as usize);
            let disc = &rest[rest.len() - 1];
            let disc_rel = self.register_dep(disc, level, Relation::Equal)?;
            if !disc_rel.is_unknown() {
                for (ix, branch) in branches.iter().rev().enumerate() {
                    self.register_branch(
                        family,
                        branch,
                        ix as VarIx,
                        &disc_rel,
                        level,
                        Relation::Unknown,
                    )?;
                }
                args = &rest[..rest.len() - 1];
            }
        }
        for arg in args.iter() {
            self.register_dep(arg, level, Relation::Unknown)?;
        }
        Ok(ArgRelation::default())
    }

    /// Register a branch of a `case` statement for a given definition at a given depth
    #[inline]
    fn register_branch(
        &mut self,
        family: &IndFamily,
        branch: &TermRef<'_>,
        ix: VarIx,
        disc_rel: &ArgRelation<VarIx>,
        mut level: VarIx,
        r: Relation,
    ) -> Result<ArgRelation<VarIx>, Error> {
        self.var_relations
            .truncate((level - self.no_args()) as usize);

        // Push bound case variables to the current definition stack, *if* fully applied, otherwise, return (for now)

        let mut inner_branch = branch.params();
        let old_level = self.level();
        let family_params = family.no_params(ix)?;
        for _ in (&mut inner_branch).take(family_params as usize) {
            self.push_var(Relation::Less * disc_rel, level)?;
            level = level.checked_add(1).ok_or(Error::VarIxOverflow)?;
        }

        // Register this case's dependencies
        let result = self.register_dep(&inner_branch.0, level, r);

        // Pop bound case variables from the current definition stack
        //TODO: optimize?
        while self.level() > old_level {
            self.pop_var()?;
        }

        result
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn left_add_recursive_calls() {
        let mut ctx = HashCtx::default();

        let add = RecDef::left_add_unfixed(&mut ctx);
        let definition_graph = make_def_graph(&[add.borrow_id()]).unwrap();
        debug_assert_eq!(definition_graph.component(0).unwrap(), (0, 0));
        debug_assert_eq!(definition_graph.decreasing_args(0).unwrap().unwrap(), &[0]);
    }

    #[test]
    fn left_mul_recursive_calls() {
        let mut ctx = HashCtx::default();

        let mul = RecDef::left_mul_unfixed(&mut ctx);
        let definition_graph = make_def_graph(&[mul.borrow_id()]).unwrap();
        debug_assert_eq!(definition_graph.component(0).unwrap(), (0, 0));
        debug_assert_eq!(definition_graph.decreasing_args(0).unwrap().unwrap(), &[0]);
    }

    #[test]
    fn loop_fails_termck() {
        let mut ctx = HashCtx::default();
        let nats = TermId::nats_in(&mut ctx);
        let unary = nats.unary_in(&mut ctx).unwrap();
        let rec = unary.var_rec_in(1, 0, &mut ctx).unwrap();
        let nat_zero = nats.var_in(0, &mut ctx).unwrap();
        let rec_call = rec.app_in(nat_zero, &mut ctx).unwrap();
        let loop_ = rec_call.abs_in(nats, &mut ctx).unwrap();
        let definition_graph = make_def_graph(&[loop_]).unwrap_err();
        assert_eq!(definition_graph, Error::Nonterminating)
    }
}
