/*!
# isotope
The `isotope` language
*/
#![forbid(missing_debug_implementations, missing_docs, unsafe_code)]

pub use isotope_core as core;
