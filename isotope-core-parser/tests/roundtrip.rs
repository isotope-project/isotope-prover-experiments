#[test]
fn roundtrip_expr_parsing() {
    let examples = [
        "()",
        "(f x y z)",
        "{ #let x = true; x }",
        "λ x => x",
        "(a: A) -> B a",
        "#case nats (λ_ => nats) zero (λn => succ n)"
    ];
    let sizes = [1, 10, 80, 1024];
    for example in examples {
        let (rest, parse) = isotope_core_parser::expr(example).unwrap();
        assert_eq!(rest, "");
        let arena = pretty::Arena::<String>::new();
        let ast = parse.pretty(false, &arena);
        let atom = parse.pretty(true, &arena);
        for size in sizes {
            let render_ast = format!("{}", ast.pretty(size));
            let render_atom = format!("{}", atom.pretty(size));
            let (rest, parse_ast) = isotope_core_parser::expr(&render_ast).unwrap();
            assert_eq!(rest, "");
            let (rest, parse_atom) = isotope_core_parser::expr(&render_atom).unwrap();
            assert_eq!(rest, "");
            assert_eq!(parse, parse_ast);
            assert_eq!(parse, parse_atom);
        }
    }
}