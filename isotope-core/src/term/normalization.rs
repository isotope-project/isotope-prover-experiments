/*!
Normalization helper implementations
*/

use super::*;

//TODO: cache result of normalization, maybe CBV parallelization?

impl TermRef<'_> {
    /// Reduce this value to beta-normal form via call-by-value evaluation in a given  *null* evaluation context, applied to an initial argument vector
    #[inline]
    pub fn norm_apply_in(
        &self,
        args: &mut Vec<TermId>,
        steps: &mut u64,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        if !ctx.is_null() {
            return Err(Error::ExpectedNullSubst);
        }
        let mut result = None;
        loop {
            for arg in args.iter_mut() {
                let pre_steps = *steps;
                if let Some(norm) = arg.norm_in(steps, ctx)? {
                    debug_assert_ne!(pre_steps, 0);
                    *arg = norm;
                }
            }
            if let Some(app) = result.as_ref().unwrap_or(self).apply(args, ctx)? {
                if *steps == 0 {
                    return Err(Error::OutOfGas);
                }
                *steps -= 1;
                result = Some(app);
            } else {
                break;
            }
        }
        Ok(result)
    }

    /// Reduce this value to beta-normal form via call-by-value evaluation in a given  *null* evaluation context, applied to an initial argument vector
    ///
    /// Always consumes all of `args` on success; leaves `args` partially consumed on failure.
    #[inline]
    pub fn norm_applied_in(
        &self,
        args: &mut Vec<TermId>,
        steps: &mut u64,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        let mut result = self.norm_apply_in(args, steps, ctx)?;
        while let Some(arg) = args.pop() {
            result = Some(
                result
                    .unwrap_or_else(|| self.consed(ctx.cons_ctx()))
                    .into_app_in(arg, ctx.cons_ctx())?,
            );
        }
        Ok(result)
    }

    /// Reduce this value to beta-normal form via call-by-value evaluation in a given  *null* evaluation context, applied to an initial argument vector
    ///
    /// Always consumes all of `args` on success; leaves `args` partially consumed on failure.
    #[inline]
    pub fn norm_applied_cons_in(
        &self,
        args: &mut Vec<TermId>,
        steps: &mut u64,
        ctx: &mut impl EvalCtx,
    ) -> Result<TermId, Error> {
        Ok(self
            .norm_applied_in(args, steps, ctx)?
            .unwrap_or_else(|| self.consed(ctx.cons_ctx())))
    }

    /// Reduce this value to beta-normal form via call-by-value evaluation in a given *null* evaluation context
    #[inline(always)]
    pub fn norm_in(
        &self,
        steps: &mut u64,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        self.norm_applied_in(&mut Vec::new(), steps, ctx)
    }

    /// Reduce this value to beta-normal form via call-by-value evaluation in a given *null* evaluation context
    #[inline]
    pub fn norm_cons_in(&self, steps: &mut u64, ctx: &mut impl EvalCtx) -> Result<TermId, Error> {
        Ok(self
            .norm_in(steps, ctx)?
            .unwrap_or_else(|| self.consed(ctx.cons_ctx())))
    }

    /// Reduce this value to beta-normal form via call-by-value evaluation
    #[inline(always)]
    pub fn norm(&self, steps: &mut u64) -> Result<TermId, Error> {
        self.norm_cons_in(steps, &mut SubstStack::new(()))
    }
}
