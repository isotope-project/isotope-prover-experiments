/*!
The `Value` trait, implemented by all `isotope` terms
*/
use crate::subst::slice::SubstSlice;

use super::*;

/// An `isotope` value
pub trait Value:
    Clone
    + Equiv<Term>
    + Equiv<TermId>
    + Cons<Term>
    + Cons<TermId>
    + Cast<TermId>
    + Cast<Term>
    + Subst<TermId>
{
    // === Main interface ===

    // == Term construction ==

    /// Convert this value into a `Term`
    fn into_term(self) -> Term;

    // == Typing ==

    /// Get the type of this value
    #[inline(always)]
    fn ty(&self) -> Option<TermRef> {
        None
    }

    /// Get the base type of this value
    #[inline(always)]
    fn base_ty(&self) -> Option<TermRef> {
        None
    }

    /// Typecheck this term in the given context
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool>;

    /// Typecheck this term in the given context
    #[inline(always)]
    fn tyck(&self, fail_early: bool, mut ctx: &impl TyCtx) -> Option<bool> {
        self.tyck_mut(fail_early, &mut ctx)
    }

    /// Check whether this value is a subtype of another in it's current form
    #[inline(always)]
    fn is_subty(&self, _other: &Term) -> bool {
        false
    }

    /// Get the join of this type with another within a given context
    #[inline]
    fn join_ty(
        &self,
        _other: TermRef<'_>,
        _ctx: &mut impl ConsCtx,
    ) -> Result<Binary<TermId>, Error> {
        Err(Error::ExpectedUni)
    }

    // == Term properties ==

    /// Check whether this value may be used as a a typing universe in it's current form
    #[inline(always)]
    fn is_universe(&self) -> bool {
        self.flags().contains(Flags::IS_UNIV)
    }

    /// Check whether this value may be used as a a typing universe of propositions in it's current form
    #[inline(always)]
    fn is_prop_universe(&self) -> bool {
        self.flags().contains(Flags::IS_PROP_UNIV)
    }

    /// Check whether this value may be used as a a type in it's current form
    #[inline(always)]
    fn is_ty(&self) -> bool {
        self.flags().contains(Flags::IS_TYPE)
    }

    /// Check whether this value may be used as a a proposition in it's current form
    #[inline(always)]
    fn is_prop(&self) -> bool {
        self.flags().contains(Flags::IS_PROP)
    }

    /// Check whether this value may be used as a a set in it's current form
    #[inline(always)]
    fn is_set(&self) -> bool {
        self.flags().contains(Flags::IS_SET)
    }

    /// Check whether this value is irrelevant, i.e. equal to all other values of it's type
    #[inline(always)]
    fn is_irr(&self) -> bool {
        self.flags().contains(Flags::IS_IRR)
    }

    /// Check whether this value is constant, i.e. ignores the first argument it is applied to
    #[inline(always)]
    fn is_const(&self) -> bool {
        self.flags().contains(Flags::IS_CONST)
    }

    /// Check whether this value typechecks without any constraints
    #[inline(always)]
    fn nil_tyck(&self) -> bool {
        self.flags().contains(Flags::TYCK_NIL)
    }

    /// Get whether this value is closed, i.e. has no free variable dependencies
    #[inline(always)]
    fn is_closed(&self) -> bool {
        let result = self.flags().contains(Flags::IS_CLOSED);
        if result {
            debug_assert_eq!(self.fv(), Filter::EMPTY);
        }
        result
    }

    /// Get whether a term depends on index within a given equivalence class between `max` and `min`
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool;

    /// Get the free variable set of this value
    fn fv(&self) -> Filter;

    /// Get this value's flags
    fn flags(&self) -> Flags;

    /// Check whether this value is a unique head
    ///
    /// Two unique head terms of different kinds should never be made equal.
    //TODO: come up with a better name...
    #[inline(always)]
    fn is_unique_head(&self) -> bool {
        false
    }

    // == Reduction ==

    /// Apply this value to a vector of arguments in a given evaluation context
    #[inline(always)]
    fn apply(
        &self,
        _args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        self.subst(ctx)
    }

    /// Apply this value as a type to a vector of arguments in a given evaluation context
    #[inline]
    fn apply_ty(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        if args.arg_len() == 0 {
            if self.is_ty() {
                self.subst(ctx)
            } else {
                Err(Error::ExpectedType)
            }
        } else {
            Err(Error::ExpectedPi)
        }
    }

    // == Term components ==

    /// Get the application base, or root, of this value, or `None` if this term is itself a root
    #[inline(always)]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    /// Visit all dependencies of a term
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E>;

    /// Visit the direct dependencies of a term
    #[inline]
    fn visit_direct_deps<E>(
        &self,
        shift: VarIx,
        mut visitor: impl FnMut(bool, VarIx, TermRef) -> Result<(), E>,
    ) -> Result<(), E> {
        self.visit_transitive_deps(shift, &mut move |is_ty, shift, term| {
            visitor(is_ty, shift, term)?;
            Ok(false)
        })
    }

    // === Hashing and comparsion ===

    /// Hash the dependencies of this term
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef));

    /// Compare this value with another term of the same type
    fn cmp_term(&self, other: &Self) -> Ordering;

    /// Get the code of this value
    fn code(&self) -> Code;

    // === Utility methods ===

    /// Convert this value into a `TermId`
    #[inline(always)]
    fn into_id(self) -> TermId {
        self.into_id_in(&mut ())
    }

    /// Convert this value into a `TermId` consed using a given context
    ///
    /// Note that this method's result should be pointer-equal to the result of `self.into_term().into_id_in(ctx)`
    #[inline]
    fn into_id_in(self, ctx: &mut impl ConsCtx) -> TermId {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            TermId::shallow_cons(self.into_term(), ctx)
        }
    }

    /// Clone this value into a `Term`
    ///
    /// This method's result should be pointer-equal to that of `self.clone().into_term()`, but may be more easily computed.
    #[inline]
    fn clone_into_term(&self) -> Term {
        self.clone().into_term()
    }

    /// Clone this value into a `TermId`
    ///
    /// This method's result should be pointer-equal to that of `self.clone().into_id()`, but may be more easily computed.
    #[inline]
    fn clone_into_id(&self) -> TermId {
        self.clone().into_id()
    }

    /// Clone this value into a `TermId` consed using a given context
    ///
    /// This method's result should be pointer-equal to that of `self.clone().into_id_in(ctx)`, but may be more easily computed.
    #[inline]
    fn clone_into_id_in(&self, ctx: &mut impl ConsCtx) -> TermId {
        self.clone().into_id_in(ctx)
    }

    /// *Fully* apply this value to a vector of arguments in a given evaluation context
    ///
    /// Always consumes all of `args` on success; leaves `args` partially consumed on failure.
    #[inline]
    fn applied(&self, args: &mut impl ArgVec, ctx: &mut impl EvalCtx) -> Result<TermId, Error> {
        let mut result = self
            .apply(args, ctx)?
            .unwrap_or_else(|| self.into_id_in(ctx.cons_ctx()));
        while let Some(arg) = args.pop_arg()? {
            result = App::try_new(result, arg, None, ctx.cons_ctx())?.into_id_in(ctx.cons_ctx());
        }
        debug_assert_eq!(args.arg_len(), 0);
        Ok(result)
    }

    /// Substitute a slice of variables with base `base`
    #[inline]
    fn subst_slice(
        &self,
        substs: &[TermId],
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<Option<TermId>, Error> {
        self.subst(&mut SubstSlice {
            substs,
            base,
            cons: ctx.cons_ctx(),
        })
    }

    /// Substitute a slice of variables with base `base`
    #[inline]
    fn subst_slice_consed(
        &self,
        substs: &[TermId],
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        self.subst_cons(&mut SubstSlice {
            substs,
            base,
            cons: ctx.cons_ctx(),
        })
    }

    /// Shift this value by `shift` with base `base`
    #[inline]
    fn shift(
        &self,
        shift: SVarIx,
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<Option<TermId>, Error> {
        self.subst(&mut Shift {
            shift,
            base,
            cons: ctx.cons_ctx(),
        })
    }

    /// Get this value, shifted by `shift` with base `base`
    #[inline]
    fn into_shifted(
        self,
        shift: SVarIx,
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(self
            .subst(&mut Shift {
                shift,
                base,
                cons: ctx.cons_ctx(),
            })?
            .unwrap_or_else(|| self.into_id_in(ctx.cons_ctx())))
    }

    /// Get the free variable set of a value, shifted down by `n`
    #[inline]
    fn shift_down_fv(&self, shift: VarIx) -> Filter {
        self.fv()
            .shift_down(shift, |equiv, min, max| self.var_within(equiv, min, max))
    }

    /// Get this term's root if it is fully applied; return `None` if this term is a fully applied root, and `Error` otherwise
    #[inline]
    fn base_root(&self) -> Result<Option<&TermId>, Error> {
        //TODO: fix this base root check...
        if let Some(ty) = self.ty() {
            match ty.as_pi() {
                Ok(_) => return Err(Error::ExpectedBaseRoot),
                Err(err) => {
                    debug_assert_eq!(err, Error::ExpectedPi);
                }
            }
        }
        Ok(self.app_root())
    }

    /// Get whether a term depends on a given index
    #[inline]
    fn has_dep(&self, var: VarIx) -> bool {
        self.var_within(var, var, var)
    }
}

bitflags::bitflags! {
    /// Flags for a value
    pub struct Flags: u8 {
        /// Whether this value ignores it's first argument
        const IS_CONST = 0b00000001;
        /// Whether this value is irrelevant
        /// 
        /// Note that every irrelevant value is constant, but not every constant value is irrelevant (hence, it is impossible to set only the flag `0b00000010`).
        const IS_IRR = 0b00000011;
        /// Whether this value is a type
        const IS_TYPE = 0b00000100;
        /// Whether this value is a universe
        const IS_UNIV = 0b00001000;
        /// Whether this value is a set
        const IS_SET = 0b000100000;
        /// Whether this value is a proposition
        const IS_PROP = 0b0010000;
        /// Whether this value is closed
        const IS_CLOSED = 0b01000000;
        /// Whether this value typechecks in the nil context
        const TYCK_NIL =  0b10000000;
        /// Whether this value is a propositional universe
        ///
        /// Note this is true if and only if this value is both a universe and a set
        const IS_PROP_UNIV = 0b00011000;
        /// The set of flags which affect equality
        const EQ_FLAGS = 0b00000011;
    }
}

impl Default for Flags {
    #[inline(always)]
    fn default() -> Self {
        Self {
            bits: Default::default(),
        }
    }
}

/// Implement the traits necessary for a type `V` to implement `Value` from implementations of `Cons<V>`, `Subst<V>` and `Equiv<Term>`
#[macro_export]
macro_rules! value_impl {
    ($value:ident) => {
        value_cons!($value);
        value_cast!($value);
        value_subst!($value);
        eq_termid!($value);
    };
}

impl<V> Value for &'_ V
where
    V: Value,
{
    #[inline(always)]
    fn into_term(self) -> Term {
        (*self).clone_into_term()
    }

    #[inline(always)]
    fn ty(&self) -> Option<TermRef> {
        (**self).ty()
    }

    #[inline(always)]
    fn base_ty(&self) -> Option<TermRef> {
        (**self).base_ty()
    }

    #[inline(always)]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        (**self).tyck_mut(fail_early, ctx)
    }

    #[inline(always)]
    fn tyck(&self, fail_early: bool, ctx: &impl TyCtx) -> Option<bool> {
        (**self).tyck(fail_early, ctx)
    }

    #[inline(always)]
    fn is_subty(&self, other: &Term) -> bool {
        (**self).is_subty(other)
    }

    #[inline(always)]
    fn join_ty(&self, other: TermRef<'_>, ctx: &mut impl ConsCtx) -> Result<Binary<TermId>, Error> {
        (**self).join_ty(other, ctx)
    }

    #[inline(always)]
    fn is_universe(&self) -> bool {
        (**self).is_universe()
    }
    
    #[inline(always)]
    fn is_prop_universe(&self) -> bool {
        (**self).is_prop_universe()
    }

    #[inline(always)]
    fn is_ty(&self) -> bool {
        (**self).is_ty()
    }

    #[inline(always)]
    fn nil_tyck(&self) -> bool {
        (**self).nil_tyck()
    }

    #[inline(always)]
    fn is_closed(&self) -> bool {
        (**self).is_closed()
    }

    #[inline(always)]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        (**self).var_within(equiv, min, max)
    }

    #[inline(always)]
    fn fv(&self) -> Filter {
        (**self).fv()
    }

    #[inline(always)]
    fn flags(&self) -> Flags {
        (**self).flags()
    }

    #[inline(always)]
    fn is_unique_head(&self) -> bool {
        (**self).is_unique_head()
    }

    #[inline(always)]
    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        (**self).apply(args, ctx)
    }

    #[inline(always)]
    fn apply_ty(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        (**self).apply_ty(args, ctx)
    }

    #[inline(always)]
    fn app_root(&self) -> Option<&TermId> {
        (**self).app_root()
    }

    #[inline(always)]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        (**self).visit_transitive_deps(shift, visitor)
    }

    #[inline(always)]
    fn visit_direct_deps<E>(
        &self,
        shift: VarIx,
        visitor: impl FnMut(bool, VarIx, TermRef) -> Result<(), E>,
    ) -> Result<(), E> {
        (**self).visit_direct_deps(shift, visitor)
    }

    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        (**self).hash_deps(hasher, trans)
    }

    #[inline(always)]
    fn cmp_term(&self, other: &Self) -> Ordering {
        (**self).cmp_term(*other)
    }

    #[inline(always)]
    fn code(&self) -> Code {
        (**self).code()
    }

    #[inline(always)]
    fn into_id(self) -> TermId {
        (*self).clone_into_id()
    }

    #[inline(always)]
    fn into_id_in(self, ctx: &mut impl ConsCtx) -> TermId {
        (*self).clone_into_id_in(ctx)
    }

    #[inline(always)]
    fn clone_into_term(&self) -> Term {
        (**self).clone_into_term()
    }

    #[inline(always)]
    fn clone_into_id_in(&self, ctx: &mut impl ConsCtx) -> TermId {
        (**self).clone_into_id_in(ctx)
    }

    #[inline(always)]
    fn applied(&self, args: &mut impl ArgVec, ctx: &mut impl EvalCtx) -> Result<TermId, Error> {
        (**self).applied(args, ctx)
    }

    #[inline(always)]
    fn subst_slice(
        &self,
        substs: &[TermId],
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<Option<TermId>, Error> {
        (**self).subst_slice(substs, base, ctx)
    }

    #[inline(always)]
    fn subst_slice_consed(
        &self,
        substs: &[TermId],
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        (**self).subst_slice_consed(substs, base, ctx)
    }

    #[inline(always)]
    fn shift(
        &self,
        shift: SVarIx,
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<Option<TermId>, Error> {
        (**self).shift(shift, base, ctx)
    }

    #[inline(always)]
    fn into_shifted(
        self,
        shift: SVarIx,
        base: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        (*self).shifted(shift, base, ctx)
    }

    #[inline(always)]
    fn shift_down_fv(&self, shift: VarIx) -> Filter {
        (**self).shift_down_fv(shift)
    }

    #[inline(always)]
    fn base_root(&self) -> Result<Option<&TermId>, Error> {
        (**self).base_root()
    }

    #[inline(always)]
    fn has_dep(&self, var: VarIx) -> bool {
        (**self).has_dep(var)
    }
}

/// An optional `isotope` value
pub trait OptValue {
    /// Get whether a term typechecks in the empty context
    fn tyck_in_empty(&self) -> bool;

    /// Get whether a term depends on index within a given equivalence class between `max` and `min`
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool;

    /// Get the free variable set of this value
    fn fv(&self) -> Filter;

    /// Get whether this value is a subtype of another
    fn is_subty(&self, other: &Term) -> bool;

    /// Typecheck this value, if any
    fn tyck(&self, fail_early: bool, ctx: &impl TyCtx) -> Option<bool>;

    /// Typecheck this value, if any
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool>;

    /// Get the code of this value
    fn code(&self) -> Code;
}

impl<V: Value> OptValue for Option<V> {
    #[inline]
    fn tyck_in_empty(&self) -> bool {
        if let Some(val) = self {
            val.nil_tyck()
        } else {
            true
        }
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(val) = self {
            val.var_within(equiv, min, max)
        } else {
            false
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        if let Some(val) = self {
            val.fv()
        } else {
            Filter::EMPTY
        }
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        if let Some(val) = self {
            val.is_subty(other)
        } else {
            false
        }
    }

    #[inline]
    fn tyck(&self, fail_early: bool, ctx: &impl TyCtx) -> Option<bool> {
        if let Some(val) = self {
            val.tyck(fail_early, ctx)
        } else {
            Some(true)
        }
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        if let Some(val) = self {
            val.tyck_mut(fail_early, ctx)
        } else {
            Some(true)
        }
    }

    #[inline]
    fn code(&self) -> Code {
        if let Some(val) = self {
            val.code()
        } else {
            Code(0)
        }
    }
}

/// Get the join of a collection of values
pub fn join_iter<'a>(
    values: impl IntoIterator<Item = &'a TermId>,
    ctx: &mut impl ConsCtx,
) -> Result<Option<TermId>, Error> {
    let mut values = values.into_iter();
    let mut join: Option<TermRef<'a>> = None;
    while let Some(curr) = values.next() {
        let ty = curr.ty().ok_or(Error::CannotInfer)?;
        if let Some(join) = &mut join {
            match join.join_ty(ty.borrow_id(), ctx)? {
                Binary::This(this) => *join = this,
                Binary::Left => {}
                Binary::Right => *join = ty,
            }
        } else {
            join = Some(ty)
        }
    }
    Ok(join.map(|join| join.into_id_in(ctx)))
}
