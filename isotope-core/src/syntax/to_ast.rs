use hashbrown::HashMap;
use std::{fmt::Display, hash::BuildHasher};

use super::*;

/// An `isotope` term climber
pub trait TermClimber {
    /// Error when attempting to climb a term
    type Error;

    /// Push a parameter
    fn push_param(&mut self, ty: Option<TermRef>) -> Result<(), Self::Error>;

    /// Pop a parameter
    fn pop_param(&mut self) -> Result<(), Self::Error>;
}

/// An `isotope` AST variable resolver
pub trait ASTVarResolver: TermClimber {
    /// Resolve the most recent parameter name
    fn param_name(&mut self) -> Result<Option<SmolStr>, Self::Error>;

    /// Build a variable
    fn var(&mut self, ix: VarIx, ty: Option<TermRef>) -> Result<Arc<Expr>, Self::Error>;
}

/// An `isotope` AST builder
pub trait ASTBuilder: ASTVarResolver {
    /// Whether to first collapse arguments
    fn collapse_args(&self) -> bool {
        true
    }

    /// Try to build an expression
    fn try_expr(&mut self, _id: &TermRef) -> Result<Option<Arc<Expr>>, Self::Error> {
        Ok(None)
    }

    /// Build an inductive definition
    fn ind_def(&mut self, def: &IndDef) -> Result<Arc<Expr>, Self::Error>;

    /// Build an inductive constructor
    fn ind_cons(
        &mut self,
        def: &TermId,
        fam_ix: VarIx,
        con_ix: VarIx,
    ) -> Result<Arc<Expr>, Self::Error>;

    /// Build an inductive family
    fn ind_fam(&mut self, def: &TermId, fam_ix: VarIx) -> Result<Arc<Expr>, Self::Error>;

    /// Build a recursive definition
    fn rec_def(&mut self, def: &RecDef) -> Result<Arc<Expr>, Self::Error>;

    /// Build a recursively defined term
    fn rec(&mut self, def: &TermId, ix: VarIx) -> Result<Arc<Expr>, Self::Error>;

    /// Build a universe
    fn universe(&mut self, universe: &Universe) -> Result<Arc<Expr>, Self::Error> {
        Ok(Arc::new(Expr::Universe(ast::Universe(universe.level()))))
    }
}

/// A term which can be converted to an AST
pub trait ToAST {
    /// Convert this term to an AST
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error>;
}

impl ToAST for TermRef<'_> {
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        if let Some(expr) = builder.try_expr(self)? {
            return Ok(expr);
        }
        let expr = {
            let mut arg_asts = SmallVec::new();
            let mut args = self.args();
            if builder.collapse_args() {
                for arg in &mut args {
                    arg_asts.push(arg.to_ast(builder)?)
                }
            }
            if arg_asts.is_empty() {
                (**self).to_ast(builder)?
            } else {
                arg_asts.push(args.0.to_ast(builder)?);
                arg_asts.reverse();
                Arc::new(Expr::App(ast::App(arg_asts)))
            }
        };
        //TODO: optimize
        if let Some(ty) = self.ty() {
            if Some(&ty) != self.base_ty().as_ref() {
                let ty = ty.to_ast(builder)?;
                return Ok(Arc::new(Expr::Annotated(ast::Annotated { expr, ty })));
            }
        }
        Ok(expr)
    }
}

impl ToAST for Term {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        for_term!(self; t => t.to_ast(builder))
    }
}

impl ToAST for Var {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.var(self.ix(), self.ty())
    }
}

impl ToAST for Lambda {
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        let param_ty = self.param_ty().to_ast(builder)?;
        builder.push_param(Some(self.param_ty().borrow_id()))?;
        let param_name = if self.result().has_dep(0) {
            builder.param_name()
        } else {
            Ok(None)
        };
        let result = self.result().to_ast(builder);
        builder.pop_param()?;
        Ok(Arc::new(Expr::Lambda(ast::Lambda {
            param_ty: Some(param_ty),
            param_name: param_name?,
            result: result?,
        })))
    }
}

impl ToAST for App {
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        Ok(Arc::new(Expr::App(ast::App(smallvec![
            self.left().to_ast(builder)?,
            self.right().to_ast(builder)?
        ]))))
    }
}

impl ToAST for Pi {
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        let param_ty = self.param_ty().to_ast(builder)?;
        builder.push_param(Some(self.param_ty().borrow_id()))?;
        let param_name = if self.result().has_dep(0) {
            builder.param_name()
        } else {
            Ok(None)
        };
        let result = self.result().to_ast(builder);
        builder.pop_param()?;
        Ok(Arc::new(Expr::Pi(ast::Pi {
            param_ty,
            param_name: param_name?,
            result: result?,
        })))
    }
}

impl ToAST for Universe {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.universe(self)
    }
}

impl ToAST for IndDef {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.ind_def(self)
    }
}

impl ToAST for IndFamily {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.ind_fam(self.def(), self.ix())
    }
}

impl ToAST for IndCons {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.ind_cons(self.def(), self.fam_ix(), self.ix())
    }
}

impl ToAST for Case {
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        Ok(Arc::new(Expr::Case(ast::Case {
            target: self.target().to_ast(builder)?,
            family: self.family().to_ast(builder)?,
        })))
    }
}

impl ToAST for RecDef {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.rec_def(self)
    }
}

impl ToAST for Rec {
    #[inline]
    fn to_ast<B: ASTBuilder>(&self, builder: &mut B) -> Result<Arc<Expr>, B::Error> {
        builder.rec(self.def(), self.ix())
    }
}

/// A simple AST builder for `isotope` values, which maintains a set of mapping from terms to expressions
#[derive(Clone, Debug, Default)]
pub struct SimpleNameBuilder<V, S = ahash::RandomState> {
    /// The map from *de-Bruijn closed* terms to expressions
    pub closed: HashMap<TermId, Arc<Expr>, S>,
    /// The variable resolver in use
    pub resolver: V,
}

/// A simple variable resolver for `isotope` values, which simply returns *de-Bruijn levels* (rather than de-Bruijn indices!)
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct LevelResolver {
    /// The current de-Bruijn level
    pub level: VarIx,
}

impl LevelResolver {
    /// Get the name of a given variable index
    #[inline]
    pub fn name(&self, ix: VarIx) -> Option<SmolStr> {
        if ix < self.level {
            Some(format!("_b{}", self.level - 1 - ix).into())
        } else {
            Some(format!("_f{}", ix - self.level).into())
        }
    }
}

impl TermClimber for LevelResolver {
    type Error = ();

    #[inline]
    fn push_param(&mut self, _ty: Option<TermRef>) -> Result<(), ()> {
        self.level = self.level.checked_add(1).ok_or(())?;
        Ok(())
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), ()> {
        self.level = self.level.checked_sub(1).ok_or(())?;
        Ok(())
    }
}

impl ASTVarResolver for LevelResolver {
    #[inline]
    fn param_name(&mut self) -> Result<Option<SmolStr>, Self::Error> {
        Ok(self.name(0))
    }

    #[inline]
    fn var(&mut self, ix: VarIx, _ty: Option<TermRef>) -> Result<Arc<Expr>, Self::Error> {
        Ok(Arc::new(Expr::Ident(self.name(ix))))
    }
}

impl ASTBuilder for LevelResolver {
    fn ind_def(&mut self, def: &IndDef) -> Result<Arc<Expr>, Self::Error> {
        Ok(Arc::new(Expr::Ident(Some(
            format!("_ind{}", def.code().0).into(),
        ))))
    }

    fn ind_cons(
        &mut self,
        def: &TermId,
        fam_ix: VarIx,
        con_ix: VarIx,
    ) -> Result<Arc<Expr>, Self::Error> {
        if let Ok(var) = def.as_var() {
            if let Some(name) = self.name(var.ix()) {
                return Ok(Arc::new(Expr::Ident(Some(
                    format!("_ind[{}].{}.{}", name, fam_ix, con_ix).into(),
                ))));
            }
        }
        Ok(Arc::new(Expr::Ident(Some(
            format!("_ind{}.{}.{}", def.code().0, fam_ix, con_ix).into(),
        ))))
    }

    fn ind_fam(&mut self, def: &TermId, fam_ix: VarIx) -> Result<Arc<Expr>, Self::Error> {
        if let Ok(var) = def.as_var() {
            if let Some(name) = self.name(var.ix()) {
                return Ok(Arc::new(Expr::Ident(Some(
                    format!("_ind[{}].{}", name, fam_ix).into(),
                ))));
            }
        }
        Ok(Arc::new(Expr::Ident(Some(
            format!("_ind{}.{}", def.code().0, fam_ix).into(),
        ))))
    }

    fn rec_def(&mut self, def: &RecDef) -> Result<Arc<Expr>, Self::Error> {
        Ok(Arc::new(Expr::Ident(Some(
            format!("_rec{}", def.code().0).into(),
        ))))
    }

    fn rec(&mut self, def: &TermId, ix: VarIx) -> Result<Arc<Expr>, Self::Error> {
        if let Ok(var) = def.as_var() {
            if let Some(name) = self.name(var.ix()) {
                return Ok(Arc::new(Expr::Ident(Some(
                    format!("_rec[{}].{}", name, ix).into(),
                ))));
            }
        }
        Ok(Arc::new(Expr::Ident(Some(
            format!("_rec{}.{}", def.code().0, ix).into(),
        ))))
    }

    fn universe(&mut self, universe: &Universe) -> Result<Arc<Expr>, Self::Error> {
        Ok(Arc::new(Expr::Universe(ast::Universe(universe.level()))))
    }
}

impl<V, S> TermClimber for SimpleNameBuilder<V, S>
where
    V: ASTVarResolver,
{
    type Error = V::Error;

    #[inline]
    fn push_param(&mut self, ty: Option<TermRef>) -> Result<(), Self::Error> {
        self.resolver.push_param(ty)
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Self::Error> {
        self.resolver.pop_param()
    }
}

impl<V, S> ASTVarResolver for SimpleNameBuilder<V, S>
where
    V: ASTVarResolver,
{
    #[inline]
    fn param_name(&mut self) -> Result<Option<SmolStr>, Self::Error> {
        self.resolver.param_name()
    }

    #[inline]
    fn var(&mut self, ix: VarIx, ty: Option<TermRef>) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.var(ix, ty)
    }
}

impl<V, S> ASTBuilder for SimpleNameBuilder<V, S>
where
    V: ASTBuilder,
    S: BuildHasher,
{
    #[inline]
    fn try_expr(&mut self, id: &TermRef) -> Result<Option<Arc<Expr>>, Self::Error> {
        if id.is_closed() {
            return Ok(self.closed.get(id).cloned());
        }
        Ok(None)
    }

    #[inline]
    fn ind_def(&mut self, def: &IndDef) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.ind_def(def)
    }

    #[inline]
    fn ind_cons(
        &mut self,
        def: &TermId,
        fam_ix: VarIx,
        con_ix: VarIx,
    ) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.ind_cons(def, fam_ix, con_ix)
    }

    #[inline]
    fn ind_fam(&mut self, def: &TermId, fam_ix: VarIx) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.ind_fam(def, fam_ix)
    }

    #[inline]
    fn rec_def(&mut self, def: &RecDef) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.rec_def(def)
    }

    #[inline]
    fn rec(&mut self, def: &TermId, ix: VarIx) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.rec(def, ix)
    }

    #[inline]
    fn universe(&mut self, universe: &Universe) -> Result<Arc<Expr>, Self::Error> {
        self.resolver.universe(universe)
    }
}

impl Display for Term {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut builder = SimpleNameBuilder::<LevelResolver>::default();
        match self.to_ast(&mut builder) {
            Ok(ast) => {
                let arena = pretty::Arena::<String>::new();
                write!(f, "{}", ast.pretty(false, &arena).pretty(60))
            }
            Err(()) => write!(f, "#invalid"),
        }
    }
}

impl Display for TermId {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        Display::fmt(&**self, f)
    }
}
