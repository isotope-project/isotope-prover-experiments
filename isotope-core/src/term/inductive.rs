/*!
Inductive and finite types
*/
use super::*;

/// An inductive definition
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct IndDef {
    /// The code of this definition
    code: Code,
    /// The free variable set of this definition
    fv: Filter,
    /// The flags of this definition
    flags: Flags,
    /// The set of mutually inductive families in this definition
    families: SmallVec<[IndFamDef; 1]>,
}

impl Debug for IndDef {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_tuple("IndDef").field(&self.families).finish()
    }
}

impl IndDef {
    /// Attempt to create an inductive definition from a vector of families
    pub fn try_new(families: SmallVec<[IndFamDef; 1]>) -> Result<IndDef, Error> {
        Self::inductive_var_check(&families)?;
        Self::strict_positivity_check(&families)?;
        let fv = Self::compute_fv(&families);
        let flags = Self::compute_flags(&families, fv);
        let code = Self::compute_code(&families);
        Ok(IndDef {
            families,
            fv,
            code,
            flags,
        })
    }

    /// Perform a variable type check on a vector of families
    pub fn inductive_var_check(families: &[IndFamDef]) -> Result<(), Error> {
        //TODO: factor into independent subtypes, to optimize?
        for family in families {
            Self::inductive_var_check_term(families, 0, &*family.ty)?;
            for con in &family.cons {
                Self::inductive_var_check_term(families, 0, &**con)?;
            }
        }
        Ok(())
    }

    /// Perform a variable type check on a term
    pub fn inductive_var_check_term(
        families: &[IndFamDef],
        shift: VarIx,
        term: &Term,
    ) -> Result<(), Error> {
        if term.fv().contains(shift) == Some(false) {
            return Ok(());
        }
        //TODO: shifted comparator
        match term {
            Term::IndCons(con) => {
                if let Ok(var) = con.def().as_var() {
                    if var.ix() == shift {
                        if con.base_ty().ok_or(Error::ExpectedTyped)?.shifted(
                            shift as SVarIx,
                            0,
                            &mut (),
                        )? != families
                            .get(con.fam_ix() as usize)
                            .ok_or(Error::IndFamIxOob)?
                            .cons
                            .get(con.ix() as usize)
                            .ok_or(Error::IndConsIxOob)?
                            .borrow_id()
                            .into_shifted(shift as SVarIx, 0, &mut ())?
                        {
                            return Err(Error::IndVarMismatch);
                        }
                    }
                }
            }
            Term::IndFamily(fam) => {
                if let Ok(var) = fam.def().as_var() {
                    if var.ix() == shift {
                        if fam.base_ty().ok_or(Error::ExpectedTyped)?
                            != families
                                .get(fam.ix() as usize)
                                .ok_or(Error::IndFamIxOob)?
                                .ty
                                .borrow_id()
                                .into_shifted(shift as SVarIx, 0, &mut ())?
                        {
                            return Err(Error::IndVarMismatch);
                        }
                    }
                }
            }
            _ => {}
        }
        term.visit_direct_deps(shift, &mut |_is_ty, shift, dep: TermRef| {
            Self::inductive_var_check_term(families, shift, &*dep)
        })
    }

    /// Perform a strict positivity check on a vector of families
    pub fn strict_positivity_check(families: &[IndFamDef]) -> Result<(), Error> {
        //TODO: return an error on too many families, or just crash?
        //TODO: check family variables have the correct type!
        for (i, family) in families.iter().enumerate() {
            let sort = family.ty.arity_sort_id()?;
            debug_assert!(family.ty.is_arity(Some(sort.borrow_id())));
            for con in family.cons.iter() {
                if !con.is_type_of_constructor(
                    0,
                    i as VarIx,
                    family.ty.borrow_id(),
                    sort.borrow_id(),
                )? {
                    return Err(Error::ExpectedTypeOfConstructor);
                }
                if !con.satisfies_positivity_condition(0)? {
                    return Err(Error::FailedPositivityCondition);
                }
            }
        }
        Ok(())
    }

    /// Compute the free variable set for an inductive definition having a vector of families
    pub fn compute_fv(families: &[IndFamDef]) -> Filter {
        let mut fv = Filter::EMPTY;
        for family in families {
            fv = fv.union(family.compute_fv());
        }
        fv
    }

    /// Compute the flags for an inductive definition having a vector of families
    pub fn compute_flags(families: &[IndFamDef], fv: Filter) -> Flags {
        let mut flags = Flags::default();
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            families
                .iter()
                .all(|family| family.ty.nil_tyck() && family.cons.iter().all(|con| con.nil_tyck())),
        );
        flags
    }

    /// Compute the code for an inductive definition having a vector of families
    pub fn compute_code(families: &[IndFamDef]) -> Code {
        let mut hasher = AHasher::new_with_keys(445, 684577);
        families.hash(&mut hasher);
        Code(hasher.finish())
    }

    /// Create the empty type in a given context
    #[inline]
    pub fn empty(ctx: &mut impl ConsCtx) -> IndDef {
        IndFamDef::empty(ctx.cons_ctx()).into_def().unwrap()
    }

    /// Create the unit type in a given context
    #[inline]
    pub fn unit(ctx: &mut impl ConsCtx) -> IndDef {
        IndFamDef::unit(ctx.cons_ctx()).into_def().unwrap()
    }

    /// Create the booleans in a given context
    #[inline]
    pub fn bools(ctx: &mut impl ConsCtx) -> IndDef {
        IndFamDef::bools(ctx.cons_ctx()).into_def().unwrap()
    }

    /// Create the natural numbers in a given context
    #[inline]
    pub fn nats(ctx: &mut impl ConsCtx) -> IndDef {
        IndFamDef::nats(ctx.cons_ctx()).into_def().unwrap()
    }
}

impl Deref for IndDef {
    type Target = [IndFamDef];

    #[inline]
    fn deref(&self) -> &Self::Target {
        &*self.families
    }
}

/// An inductive family definition
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct IndFamDef {
    /// The type of this family
    pub ty: TermId,
    /// The constructor types for this family
    pub cons: SmallVec<[TermId; 2]>,
}

impl IndFamDef {
    /// Convert this inductive family definition into a (non mutually recursive) inductive type
    #[inline]
    pub fn into_def(self) -> Result<IndDef, Error> {
        IndDef::try_new(smallvec![self])
    }

    /// Convert this inductive family definition into a (non mutually recursive) inductive type ID
    #[inline]
    pub fn into_def_id(self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(self.into_def()?.into_id_in(ctx))
    }

    /// Convert this inductive family definition into a (non mutually recursive) inductive family
    #[inline]
    pub fn into_fam(self, ctx: &mut impl ConsCtx) -> Result<IndFamily, Error> {
        let base_ty = self.ty.consed(ctx);
        IndFamily::try_new(self.into_def_id(ctx)?, 0, Some(base_ty), None)
    }

    /// Convert this inductive family definition into a (non mutually recursive) inductive family ID
    #[inline]
    pub fn into_fam_id(self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(self.into_fam(ctx)?.into_id_in(ctx))
    }

    /// Create the empty type in a given context
    #[inline]
    pub fn empty(ctx: &mut impl ConsCtx) -> IndFamDef {
        //TODO: type as Prop
        let set = Universe::set().into_id_in(ctx);
        IndFamDef {
            ty: set,
            cons: smallvec![],
        }
    }

    /// Create the unit type in a given context
    #[inline]
    pub fn unit(ctx: &mut impl ConsCtx) -> IndFamDef {
        //TODO: type as Prop
        let set = Universe::set().into_id_in(ctx);
        let unit = set
            .var_fam_in(0, 0, ctx)
            .expect("Families of an untyped variable are always valid");
        IndFamDef {
            ty: set,
            cons: smallvec![unit],
        }
    }

    /// Create the booleans in a given context
    #[inline]
    pub fn bools(ctx: &mut impl ConsCtx) -> IndFamDef {
        let set = Universe::set().into_id_in(ctx);
        let family = set
            .var_fam_in(0, 0, ctx)
            .expect("Families of an untyped variable are always valid");
        IndFamDef {
            ty: set,
            cons: smallvec![family.clone(), family],
        }
    }

    /// Create the natural numbers in a given context
    #[inline]
    pub fn nats(ctx: &mut impl ConsCtx) -> IndFamDef {
        let set = Universe::set().into_id_in(ctx);
        let family = set
            .var_fam_in(0, 0, ctx)
            .expect("Families of an untyped variable are always valid");
        let succ = family
            .simple_in(family.clone(), ctx)
            .expect("Function from a family to family");
        IndFamDef {
            ty: set,
            cons: smallvec![family, succ],
        }
    }
}

impl Deref for IndFamDef {
    type Target = [TermId];

    #[inline]
    fn deref(&self) -> &Self::Target {
        &*self.cons
    }
}

impl IndFamDef {
    /// Get whether this family depends on index within a given equivalence class between `max` and `min`
    pub fn var_within(&self, def_shift: VarIx, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if self.ty.var_within(equiv, min, max) {
            true
        } else {
            let sh_equiv = equiv.wrapping_add(def_shift);
            let sh_min = if let Some(min) = min.checked_add(def_shift as VarIx) {
                min
            } else {
                return false;
            };
            let sh_max = max.saturating_add(def_shift as VarIx);
            for con in &self.cons {
                if con.var_within(sh_equiv, sh_min, sh_max) {
                    return true;
                }
            }
            false
        }
    }
    /// Compute the free variable set of this inductive family definition, given a shift
    pub fn compute_fv(&self) -> Filter {
        let mut fv = self.ty.fv();
        for con in &self.cons {
            fv = fv.union(con.shift_down_fv(1));
        }
        fv
    }
}

/// A family of inductive types
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct IndFamily {
    /// This family's code
    code: Code,
    /// This family's free variable set
    fv: Filter,
    /// The surface type of this family
    ty: TermId,
    /// This family's base type. Cached to improve substitution.
    base_ty: TermId,
    /// The underlying inductive definition
    def: TermId,
    /// The index of this family
    ix: VarIx,
    /// The flags of this family
    flags: Flags,
}

impl Debug for IndFamily {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Ok(var) = self.def.as_var() {
            let mut dbg = f.debug_tuple("IndFamilyVar");
            dbg.field(&var.ix()).field(&self.base_ty);
            if self.ty != self.base_ty {
                dbg.field(&self.ty);
            }
            return dbg.finish();
        }
        let mut dbg = f.debug_struct("IndFamily");
        let code = self.def.code();
        dbg.field(
            "def",
            if let Term::Var(def) = &*self.def {
                def
            } else {
                &code
            },
        )
        .field("ix", &self.ix);
        if self.ty != self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl IndFamily {
    /// Create a new inductive family from a given inductive definition *or variable* and index
    ///
    /// Note that a *base type* must be given if a variable is specified
    #[inline]
    pub fn try_new(
        def: TermId,
        ix: VarIx,
        base_ty: Option<TermId>,
        ty: Option<TermId>,
    ) -> Result<IndFamily, Error> {
        let base_ty = Self::compute_base_ty(&def, ix, base_ty.as_ref())?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&def, ix, &ty);
        let flags = Self::compute_flags(&def, ix, &base_ty, &ty, fv);
        let code = Self::compute_code(&def, ix, &ty);
        Ok(IndFamily {
            ty,
            base_ty,
            def,
            ix,
            fv,
            code,
            flags,
        })
    }

    /// Compute the base type for an inductive family with a given definition, index, and surface type
    #[inline]
    pub fn compute_base_ty(
        def_id: &TermId,
        ix: VarIx,
        base_ty: Option<&TermId>,
    ) -> Result<TermId, Error> {
        match &**def_id {
            Term::IndDef(def) => {
                let fam = def.get(ix as usize).ok_or(Error::IndFamIxOob)?;
                //NOTE: we assume this is already consed, since we assume `def_id` is consed as desired.
                Ok(fam.ty.clone())
            }
            Term::Var(v) if v.ty().is_none() => {
                if let Some(base_ty) = base_ty {
                    Ok(base_ty.clone())
                } else {
                    Err(Error::CannotInfer)
                }
            }
            _ => Err(Error::ExpectedIndDef),
        }
    }

    /// Compute the code for an inductive constructor with a given definition, family, index, and type annotation
    #[inline]
    pub fn compute_code(def: &TermId, ix: VarIx, ty: &TermId) -> Code {
        let mut state = AHasher::new_with_keys(6672653, 8434778143);
        def.code().hash(&mut state);
        ix.hash(&mut state);
        ty.hash(&mut state);
        Code(state.finish())
    }

    /// Compute the free variable set for an inductive constructor with a given definition, family, index, and type annotation
    #[inline]
    pub fn compute_fv(def: &TermId, _ix: VarIx, ty: &TermId) -> Filter {
        def.fv().union(ty.fv())
    }

    /// Compute the flags for an inductive constructor with a given definition, family, index, and type annotation
    #[inline]
    pub fn compute_flags(
        def: &TermId,
        _ix: VarIx,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        flags.set(Flags::IS_TYPE, true);
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            def.nil_tyck() && ty.nil_tyck() && ty == base_ty,
        );
        flags
    }

    /// Get this family's associated inductive definition, if any
    #[inline]
    pub fn get_def(&self) -> Result<&IndDef, Error> {
        self.def.as_ind_def()
    }

    /// Get this family's associated inductive definition
    #[inline]
    pub fn def(&self) -> &TermId {
        &self.def
    }

    /// Get the definition of this family
    #[inline]
    pub fn get_fam_def(&self) -> Result<&IndFamDef, Error> {
        Ok(&self.get_def()?[self.ix as usize])
    }

    /// Get the base type of this family
    #[inline]
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this family
    #[inline]
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    /// Get the index of this family
    #[inline]
    pub fn ix(&self) -> VarIx {
        self.ix
    }

    /// Get the number of constructors for this family
    #[inline]
    pub fn no_cons(&self) -> Result<VarIx, Error> {
        Ok(self.get_fam_def()?.len() as VarIx)
    }

    /// Get the number of parameters for a constructor for this family
    #[inline]
    pub fn no_params(&self, ix: VarIx) -> Result<VarIx, Error> {
        self.get_fam_def()?
            .get(ix as usize)
            .ok_or(Error::IndConsIxOob)?
            .params()
            .count()
            .try_into()
            .map_err(|_| Error::VarIxOverflow)
    }

    /// Get the `n`th constructor for this family
    #[inline]
    pub fn make_con(&self, n: VarIx, ctx: &mut impl ConsCtx) -> Result<IndCons, Error> {
        IndCons::try_new(self.def.clone(), self.ix, n, None, None, ctx)
    }

    /// Get the `n`th constructor for this family as an ID
    #[inline]
    pub fn con_id(&self, n: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(self.make_con(n, ctx)?.into_id_in(ctx))
    }

    /// Get the `n`th branch type for this inductive family, given a target family
    #[inline]
    pub fn branch_ty(
        &self,
        n: VarIx,
        target_family: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let mut ctx = SubstStack::new(ctx.cons_ctx());
        let con = self.con_id(n, &mut ctx.cons)?;
        let mut con_ty_iter = con.as_ind_cons().unwrap().get_base_ty().params();
        let mut params: SmallVec<[TermId; 32]> = (&mut con_ty_iter)
            .enumerate()
            .map(|(i, param)| param.shifted(i as SVarIx, 0, ctx.cons_ctx()))
            .collect::<Result<_, _>>()?;
        let mut vars = Vec::with_capacity(params.len());
        //TODO: check if this should actually be reversed...
        for (i, param) in params.iter().enumerate().rev() {
            vars.push(
                Var::with_ty(
                    (params.len() - 1 - i) as VarIx,
                    param.shifted((params.len() - i) as SVarIx, 0, ctx.cons_ctx())?,
                )?
                .into_id_in(ctx.cons_ctx()),
            );
        }
        debug_assert_eq!(params.len(), vars.len());
        let variant = con.applied(&mut vars, &mut ctx)?;
        if vars.len() != 0 {
            return Err(Error::IncompleteApplication);
        }
        debug_assert!(ctx.is_null());
        vars.push(variant);
        for arg in con_ty_iter.0.args() {
            vars.push(arg.shifted(params.len() as SVarIx, 0, ctx.cons_ctx())?)
        }
        let target = target_family
            .shifted(params.len() as SVarIx, 0, &mut ctx.cons)?
            .applied(&mut vars, &mut ctx)?;
        if vars.len() != 0 {
            return Err(Error::IncompleteApplication);
        }
        debug_assert!(ctx.is_null());
        let mut result = target;
        while let Some(param) = params.pop() {
            result =
                Pi::try_new(param, result, None, ctx.cons.cons_ctx())?.into_id_in(ctx.cons_ctx());
        }
        Ok(result)
    }

    /// Create the booleans in a given context
    #[inline]
    pub fn bools(ctx: &mut impl ConsCtx) -> IndFamily {
        IndFamDef::bools(ctx.cons_ctx())
            .into_fam(ctx.cons_ctx())
            .unwrap()
    }

    /// Create the natural numbers in a given context
    #[inline]
    pub fn nats(ctx: &mut impl ConsCtx) -> IndFamily {
        IndFamDef::nats(ctx.cons_ctx())
            .into_fam(ctx.cons_ctx())
            .unwrap()
    }
}

/// An inductive constructor
#[derive(Clone, PartialEq, Eq, Hash)]

pub struct IndCons {
    /// This family's code
    code: Code,
    /// This family's free variable set
    fv: Filter,
    /// This constructor's base type
    base_ty: TermId,
    /// The surface type of this constructor
    ty: TermId,
    /// The underlying inductive definition
    def: TermId,
    /// The family of this constructor
    fam: VarIx,
    /// The index of this constructor
    ix: VarIx,
    /// The flags for this constructor
    flags: Flags,
}

impl Debug for IndCons {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Ok(var) = self.def.as_var() {
            return f
                .debug_tuple("IndConsVar")
                .field(&var.ix())
                .field(&self.base_ty)
                .finish();
        }
        let mut dbg = f.debug_struct("IndCons");
        let code = self.def.code();
        dbg.field(
            "def",
            if let Term::Var(def) = &*self.def {
                def
            } else {
                &code
            },
        )
        .field("fam", &self.fam)
        .field("ix", &self.ix);
        if self.ty != self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl IndCons {
    /// Create a new inductive constructor from a given inductive definition *or variable*, family index, and constructor index
    ///
    /// Note that, if constructing an inductive constructor using a variable, supplying a type is *mandatory*.
    #[inline]
    pub fn try_new(
        def: TermId,
        fam: VarIx,
        ix: VarIx,
        base_ty: Option<TermId>,
        ty: Option<TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<IndCons, Error> {
        let base_ty = Self::compute_base_ty(&def, fam, ix, base_ty.as_ref(), ctx)?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&def, fam, ix, &ty);
        let flags = Self::compute_flags(&def, fam, ix, &base_ty, &ty, fv);
        let code = Self::compute_code(&def, fam, ix, &ty);
        Ok(IndCons {
            code,
            fv,
            base_ty,
            ty,
            def,
            fam,
            ix,
            flags,
        })
    }

    /// Compute the base type for an inductive constructor with a given definition, family, and index
    #[inline]
    pub fn compute_base_ty(
        def_id: &TermId,
        fam: VarIx,
        ix: VarIx,
        base_ty: Option<&TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        match &**def_id {
            Term::IndDef(def) => {
                let fam = def.get(fam as usize).ok_or(Error::IndFamIxOob)?;
                let cons = fam.get(ix as usize).ok_or(Error::IndConsIxOob)?;
                let result = cons.subst_slice_consed(std::slice::from_ref(def_id), 0, ctx)?;
                Ok(result)
            }
            Term::Var(_) => {
                if let Some(base_ty) = base_ty {
                    Ok(base_ty.clone())
                } else {
                    Err(Error::CannotInfer)
                }
            }
            _ => Err(Error::ExpectedIndDef),
        }
    }

    /// Compute the free variable set for an inductive constructor with a given definition, family, index, and type annotation
    #[inline]
    pub fn compute_fv(def: &TermId, _fam: VarIx, _ix: VarIx, ty: &TermId) -> Filter {
        def.fv().union(ty.fv())
    }

    /// Compute the free variable set for an inductive constructor with a given definition, family, index, and type annotation
    #[inline]
    pub fn compute_flags(
        def: &TermId,
        _fam: VarIx,
        _ix: VarIx,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        flags.set(Flags::IS_IRR, base_ty.is_prop() || ty.is_prop());
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            def.nil_tyck() && ty.nil_tyck() && ty == base_ty,
        );
        flags
    }

    /// Compute the code for an inductive constructor with a given definition, family, index, and type annotation
    #[inline]
    pub fn compute_code(def: &TermId, fam: VarIx, ix: VarIx, ty: &TermId) -> Code {
        let mut state = AHasher::new_with_keys(632653, 87864543);
        def.code().hash(&mut state);
        fam.hash(&mut state);
        ix.hash(&mut state);
        ty.hash(&mut state);
        Code(state.finish())
    }

    /// Get this constructor's associated inductive definition
    #[inline]
    pub fn def(&self) -> &TermId {
        &self.def
    }

    /// Get this constructor's associated inductive definition
    #[inline]
    pub fn get_def(&self) -> &IndDef {
        &self
            .def
            .as_ind_def()
            .expect("self.def should always be an inductive definition")
    }

    /// Get the definition of this constructor's family
    #[inline]
    pub fn get_fam_def(&self) -> &IndFamDef {
        &self.get_def()[self.fam as usize]
    }

    /// Get the base type of this constructor
    #[inline]
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this constructor
    #[inline]
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    /// Get the index of this constructor
    #[inline]
    pub fn ix(&self) -> VarIx {
        self.ix
    }

    /// Get the family index of this constructor
    #[inline]
    pub fn fam_ix(&self) -> VarIx {
        self.fam
    }
}

impl Equiv<IndDef> for IndDef {
    fn term_eq_mut(
        &self,
        other: &IndDef,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        self.families.term_eq_mut(&other.families, fail_early, ctx)
    }
}

impl Equiv<IndFamDef> for IndFamDef {
    fn term_eq_mut(
        &self,
        other: &IndFamDef,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        opt_and!(
            false;
            self.ty.term_eq_mut(&other.ty, fail_early, ctx),
            self.cons.term_eq_mut(&other.cons, fail_early, ctx)
        )
    }
}

eq_term!(IndDef);

impl Cons<IndDef> for IndDef {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<IndDef> {
        do_cons! {
            in ctx;
            let families = self.families;
            Some(IndDef {
                families,
                ..*self
            })
        }
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> IndDef {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> IndDef {
        self.clone()
    }
}

impl Cons<IndFamDef> for IndFamDef {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<IndFamDef> {
        do_cons! {
            in ctx;
            let ty = self.ty;
            let cons = self.cons;
            Some(IndFamDef { ty, cons, ..*self })
        }
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> IndFamDef {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> IndFamDef {
        self.clone()
    }
}

impl Cast<IndDef> for IndDef {
    fn cast(&self, _ty: TermRef, _ctx: &mut impl ConsCtx) -> Result<Option<IndDef>, Error> {
        Err(Error::UntypedCast)
    }
}

impl Subst<IndFamDef> for IndFamDef {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<IndFamDef>, Error> {
        let ty = self.ty.subst(ctx)?;
        ctx.push_param(None)?;
        let cons = self.cons.subst(ctx);
        ctx.pop_param()?;
        let cons = cons?;
        if ty.is_none() && cons.is_none() {
            Ok(None)
        } else {
            let ty = ty.unwrap_or_else(|| self.ty.clone());
            let cons = cons.unwrap_or_else(|| self.cons.clone());
            Ok(Some(IndFamDef { ty, cons, ..*self }))
        }
    }
}

impl Subst<IndDef> for IndDef {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<IndDef>, Error> {
        do_subst! {
            in ctx;
            let families = self.families;
            IndDef::try_new(families).map(Some)
        }
    }
}

value_impl!(IndDef);

impl Value for IndDef {
    #[inline]
    fn into_term(self) -> Term {
        Term::IndDef(self)
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        let mut failure = false;
        for fam in self.families.iter() {
            match fam.ty.tyck_mut(fail_early, ctx) {
                Some(true) => {}
                Some(false) => return Some(false),
                None => {
                    failure = true;
                    if fail_early {
                        break;
                    }
                }
            }
            for con in fam.cons.iter() {
                match con.tyck_mut(fail_early, ctx) {
                    Some(true) => {}
                    Some(false) => {
                        return Some(false);
                    }
                    None => {
                        failure = true;
                        if fail_early {
                            break;
                        }
                    }
                }
            }
        }
        if failure {
            None
        } else {
            Some(true)
        }
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            for family in &self.families {
                if family.var_within(self.families.len() as VarIx, equiv, min, max) {
                    return true;
                }
            }
            false
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        for family in &self.families {
            family.ty.visit_transitive_deps_rec(shift, visitor)?;
            for con in &family.cons {
                con.visit_transitive_deps_rec(shift + 1, visitor)?;
            }
        }
        Ok(())
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        for fam in &self.families {
            trans(hasher, &fam.ty);
            for con in &fam.cons {
                trans(hasher, con)
            }
        }
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        if self.families.len() != other.families.len() {
            return self.families.len().cmp(&other.families.len());
        }
        let mut ord = Ordering::Equal;
        for (i, f) in self.families.iter().enumerate() {
            ord = ord.then(f.cons.len().cmp(&other.families[i].cons.len()));
            if ord != Ordering::Equal {
                break;
            }
            ord = ord.then(f.ty.cmp_term(&other.families[i].ty));
            if ord != Ordering::Equal {
                break;
            }
            for (j, c) in f.cons.iter().enumerate() {
                ord = ord.then(c.cmp_term(&other.families[i].cons[j]));
                if ord != Ordering::Equal {
                    break;
                }
            }
        }
        ord
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

impl Equiv<IndFamily> for IndFamily {
    fn term_eq_mut(
        &self,
        other: &IndFamily,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        opt_and! {
            fail_early;
            Some(self.ix == other.ix),
            // if ctx.typed() {  self.ty.term_eq(&other.ty, ctx) } else { Some(true) },
            self.def.term_eq_mut(&other.def, fail_early, ctx)
        }
    }
}

impl Cons<IndFamily> for IndFamily {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<IndFamily> {
        do_cons! {
            in ctx;
            let ty = self.ty;
            let base_ty = self.base_ty;
            let def = self.def;
            Some(IndFamily { ty, base_ty, def, ..*self })
        }
    }

    fn consed(&self, ctx: &mut impl ConsCtx) -> IndFamily {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> IndFamily {
        self.clone()
    }
}

impl Cast<IndFamily> for IndFamily {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<IndFamily>, Error> {
        if ty == self.ty {
            Ok(None)
        } else {
            //TODO: avoid re-checking base type
            Self::try_new(
                self.def.consed(ctx),
                self.ix,
                Some(self.base_ty.consed(ctx)),
                Some(self.ty.consed(ctx)),
            )
            .map(Some)
        }
    }
}

impl Subst<IndFamily> for IndFamily {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<IndFamily>, Error> {
        do_subst! {
            in ctx;
            let ty = self.ty;
            let base_ty = self.base_ty;
            let def = self.def;
            IndFamily::try_new(def, self.ix, Some(base_ty), Some(ty)).map(Some)
        }
    }
}

eq_term!(IndFamily);
value_impl!(IndFamily);

impl Value for IndFamily {
    #[inline]
    fn into_term(self) -> Term {
        Term::IndFamily(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.get_ty().borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.get_base_ty().borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.ty, &self.base_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.def.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        //FIXME: semi-untyped comparison?
        self == other
    }

    #[inline]
    fn is_universe(&self) -> bool {
        false
    }

    #[inline]
    fn is_ty(&self) -> bool {
        self.get_ty().is_universe()
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.ty.var_within(equiv, min, max) || self.def.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        true
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.def.visit_transitive_deps_rec(shift, visitor)
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        trans(hasher, &self.ty);
        self.ix.hash(hasher);
        trans(hasher, &self.def);
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then(self.ix.cmp(&other.ix))
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.def.cmp(&other.def))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

impl Equiv<IndCons> for IndCons {
    //NOTE: base_ty is implied
    fn term_eq_mut(
        &self,
        other: &IndCons,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        opt_and! {
            fail_early;
            Some(self.ix == other.ix),
            self.ty.term_eq_mut(&other.ty, fail_early, ctx),
            self.def.term_eq_mut(&other.def, fail_early, ctx)
        }
    }
}

impl Cons<IndCons> for IndCons {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<IndCons> {
        do_cons!(
            in ctx;
            let base_ty= self.base_ty;
            let ty = self.ty;
            let def = self.def;
            Some(IndCons { base_ty, ty, def, ..*self })
        )
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> IndCons {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    #[inline]
    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> IndCons {
        self.clone()
    }
}

impl Cast<IndCons> for IndCons {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<IndCons>, Error> {
        if self.ty == ty {
            Ok(None)
        } else {
            //TODO: avoid recomputing base_ty
            Self::try_new(
                self.def.consed(ctx),
                self.fam,
                self.ix,
                Some(self.base_ty.consed(ctx)),
                Some(self.ty.consed(ctx)),
                ctx,
            )
            .map(Some)
        }
    }
}

impl Subst<IndCons> for IndCons {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<IndCons>, Error> {
        do_subst!(
            in ctx;
            let ty = self.ty;
            let base_ty = self.base_ty;
            let def = self.def;
            IndCons::try_new(def, self.fam, self.ix, Some(base_ty), Some(ty), ctx.cons_ctx()).map(Some)
        )
    }
}

eq_term!(IndCons);
value_impl!(IndCons);

impl Value for IndCons {
    #[inline]
    fn into_term(self) -> Term {
        Term::IndCons(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.get_ty().borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.get_base_ty().borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.ty, &self.base_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.def.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, _other: &Term) -> bool {
        false
    }

    #[inline]
    fn is_universe(&self) -> bool {
        false
    }

    #[inline]
    fn is_ty(&self) -> bool {
        false
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.ty.var_within(equiv, min, max) || self.def.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        true
    }

    #[inline]
    fn apply(
        &self,
        _args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        self.subst(ctx)
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.def.visit_transitive_deps_rec(shift, visitor)
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        self.ix.hash(hasher);
        self.fam.hash(hasher);
        trans(hasher, &self.ty);
        trans(hasher, &self.def);
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then(self.ix.cmp(&other.ix))
            .then(self.fam.cmp(&other.fam))
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.def.cmp(&other.def))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

impl TermId {
    /// Get the case result type for this inductive family, given a target family
    #[inline]
    pub fn case_result_ty(
        &self,
        target_family: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let mut ctx = SubstStack::new(ctx.cons_ctx());
        let mut d = 0;
        let mut fam_ty_iter = self.as_ind_fam()?.get_base_ty().params();
        let mut params: SmallVec<[TermId; 32]> = smallvec![];
        for param in &mut fam_ty_iter {
            params.push(param.shifted(d, 0, ctx.cons_ctx())?);
            d += 1;
        }
        let mut vars = Vec::with_capacity(params.len());
        for (i, param) in params.iter().enumerate().rev() {
            vars.push(
                Var::with_ty(
                    (params.len() - 1 - i) as VarIx,
                    param.shifted((params.len() - i) as SVarIx, 0, ctx.cons_ctx())?,
                )?
                .into_id_in(ctx.cons_ctx()),
            );
        }
        debug_assert_eq!(params.len(), vars.len());
        let family_member = self.applied(&mut vars, &mut ctx)?;
        if vars.len() != 0 {
            return Err(Error::IncompleteApplication);
        }
        debug_assert!(ctx.is_null());
        vars.push(
            family_member
                .shifted(1, 0, ctx.cons_ctx())?
                .var_in(0, ctx.cons_ctx())?,
        );
        for (i, param) in params.iter().enumerate().rev() {
            vars.push(
                Var::with_ty(
                    (params.len() - i) as VarIx,
                    param.shifted((params.len() + 1 - i) as SVarIx, 0, ctx.cons_ctx())?,
                )?
                .into_id_in(ctx.cons_ctx()),
            );
        }
        let target = target_family
            .shifted((params.len() + 1) as SVarIx, 0, &mut ctx.cons)?
            .applied(&mut vars, &mut ctx)?;
        if vars.len() != 0 {
            return Err(Error::IncompleteApplication);
        }
        debug_assert!(ctx.is_null());
        let mut result = target;
        params.push(family_member);
        while let Some(param) = params.pop() {
            result =
                Pi::try_new(param, result, None, ctx.cons.cons_ctx())?.into_id_in(ctx.cons_ctx());
        }
        Ok(result)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_natural_numbers() {
        // === Defining the naturals ===

        let mut ctx = HashCtx::default();
        let set = TermId::set_in(&mut ctx);
        let nats = TermId::nats_in(&mut ctx);

        // === Basic properties of the naturals ===

        assert_eq!(nats.ty().unwrap(), set);
        assert!(nats.is_ty());
        assert!(!nats.is_universe());
        assert!(nats.is_subty(&nats));
        assert!(!nats.is_subty(&set));
        assert_eq!(nats.fv(), Filter::EMPTY);
        let zero = nats.con_in(0, &mut ctx).unwrap();
        assert_eq!(zero.ty().unwrap(), nats);
        let succ = nats.con_in(1, &mut ctx).unwrap();
        let unary_nats = Pi::unary(nats.clone(), &mut ctx)
            .unwrap()
            .into_id_in(&mut ctx);
        assert_eq!(succ.ty().unwrap(), unary_nats);
        assert_eq!(nats.con_in(2, &mut ctx), Err(Error::IndConsIxOob));
        assert_eq!(nats.no_cons(), Ok(2));
        assert_eq!(nats.no_params(0), Ok(0));
        assert_eq!(nats.no_params(1), Ok(1));

        // === Branch types of the naturals ===

        let family_ty = nats.simple_in(set.clone(), &mut ctx).unwrap();
        let family = family_ty.var_in(0, &mut ctx).unwrap();
        let zero_branch_ty = family.app_in(zero.clone(), &mut ctx).unwrap();
        assert_eq!(
            nats.branch_ty(0, &family, &mut ctx).unwrap(),
            zero_branch_ty
        );
        let succ_branch_ty = Pi::try_new(
            nats.clone(),
            family_ty
                .var_in(1, &mut ctx)
                .unwrap()
                .app_in(
                    succ.app_in(nats.var_in(0, &mut ctx).unwrap(), &mut ctx)
                        .unwrap(),
                    &mut ctx,
                )
                .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(
            nats.branch_ty(1, &family, &mut ctx).unwrap(),
            succ_branch_ty
        );
        let nat_result_ty = Pi::try_new(
            nats.clone(),
            family_ty
                .var_in(1, &mut ctx)
                .unwrap()
                .app_in(nats.var_in(0, &mut ctx).unwrap(), &mut ctx)
                .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(
            nats.case_result_ty(&family, &mut ctx).unwrap(),
            nat_result_ty
        );

        // === Case type of the naturals ===

        let nats_case_ty = zero_branch_ty
            .simple_in(
                succ_branch_ty
                    .simple_in(nat_result_ty.clone(), &mut ctx)
                    .unwrap(),
                &mut ctx,
            )
            .unwrap();
        let nats_case = nats.case_in(family.clone(), &mut ctx).unwrap();
        assert_eq!(nats_case.ty().unwrap(), nats_case_ty);
    }

    #[test]
    fn ind_family_subst() {
        let mut ctx = HashCtx::default();
        let set = TermId::set_in(&mut ctx);
        let nats = TermId::nats_in(&mut ctx);
        let def_var = set.var_fam_in(0, 0, &mut ctx).unwrap();

        assert_eq!(
            def_var
                .subst_slice_consed(
                    std::slice::from_ref(nats.as_ind_fam().unwrap().def()),
                    0,
                    &mut ctx
                )
                .unwrap(),
            nats
        );

        let box_nats = IndFamDef {
            ty: set.clone(),
            cons: smallvec![nats.simple_in(def_var, &mut ctx).unwrap()],
        }
        .into_fam_id(&mut ctx)
        .unwrap();

        assert_eq!(box_nats.ty().unwrap(), set);

        assert_eq!(
            box_nats.con_in(0, &mut ctx).unwrap().ty().unwrap(),
            nats.simple_in(box_nats.clone(), &mut ctx).unwrap()
        );

        let nat_fam = nats.simple_in(set.clone(), &mut ctx).unwrap();
        let def_fam_var_1 = nat_fam.var_fam_in(1, 0, &mut ctx).unwrap();
        let nat_0 = nats.var_in(0, &mut ctx).unwrap();

        let index_nats = IndFamDef {
            ty: nat_fam.clone(),
            cons: smallvec![Pi::try_new(
                nats.clone(),
                def_fam_var_1.app_in(nat_0.clone(), &mut ctx).unwrap(),
                None,
                &mut ctx
            )
            .unwrap()
            .into_id_in(&mut ctx)],
        }
        .into_fam_id(&mut ctx)
        .unwrap();

        assert_eq!(index_nats.ty().unwrap(), nat_fam);

        assert_eq!(
            index_nats.con_in(0, &mut ctx).unwrap().ty().unwrap(),
            Pi::try_new(
                nats.clone(),
                index_nats.app_in(nat_0.clone(), &mut ctx).unwrap(),
                None,
                &mut ctx
            )
            .unwrap()
            .into_id_in(&mut ctx)
        );
    }

    //TODO: FIXME!
    /*
    #[test]
    fn basic_even_odd() {
        // === Defining the mutually inductive predicates even/odd ===

        let mut ctx = HashCtx::default();
        let set = TermId::set_in(&mut ctx);
        let nats = TermId::nats_in(&mut ctx);
        let zero = nats.con_id(0, &mut ctx).unwrap();
        let succ = nats.con_id(1, &mut ctx).unwrap();
        let nat_fam = nats.simple_in(set.clone(), &mut ctx).unwrap();
        let nat_0 = nats.var_in(0, &mut ctx).unwrap();
        let nat_1 = nats.var_in(1, &mut ctx).unwrap();
        let succ_nat_0 = succ.app_in(nat_0.clone(), &mut ctx).unwrap();
        let even_def = IndFamDef {
            ty: nat_fam.clone(),
            cons: smallvec![
                nat_fam
                    .var_fam_in(0, 0, &mut ctx)
                    .unwrap()
                    .app_in(zero.clone(), &mut ctx)
                    .unwrap(),
                Pi::try_new(
                    nats.clone(),
                    nat_fam
                        .var_fam_in(1, 1, &mut ctx)
                        .unwrap()
                        .app_in(nat_0.clone(), &mut ctx)
                        .unwrap()
                        .simple_in(
                            nat_fam
                                .var_fam_in(1, 0, &mut ctx)
                                .unwrap()
                                .app_in(succ_nat_0.clone(), &mut ctx)
                                .unwrap(),
                            &mut ctx
                        )
                        .unwrap(),
                    None,
                    &mut ctx
                )
                .unwrap()
                .into_id_in(&mut ctx)
            ],
        };
        let odd_def = IndFamDef {
            ty: nat_fam.clone(),
            cons: smallvec![Pi::try_new(
                nats.clone(),
                nat_fam
                    .var_fam_in(1, 0, &mut ctx)
                    .unwrap()
                    .app_in(nat_0.clone(), &mut ctx)
                    .unwrap()
                    .simple_in(
                        nat_fam
                            .var_fam_in(1, 1, &mut ctx)
                            .unwrap()
                            .app_in(succ_nat_0.clone(), &mut ctx)
                            .unwrap(),
                        &mut ctx
                    )
                    .unwrap(),
                None,
                &mut ctx
            )
            .unwrap()
            .into_id_in(&mut ctx)],
        };
        let even_odd_def = IndDef::try_new(smallvec![even_def, odd_def])
            .unwrap()
            .into_id_in(&mut ctx);
        let even = even_odd_def.fam_in(0, &mut ctx).unwrap();
        let odd = even_odd_def.fam_in(1, &mut ctx).unwrap();
        assert_ne!(even, odd);
        let even_zero = even.con_id(0, &mut ctx).unwrap();
        let even_succ = even.con_id(1, &mut ctx).unwrap();
        let odd_succ = odd.con_id(0, &mut ctx).unwrap();

        // === Check types of mutually inductive predicates ===

        assert_eq!(even.ty().unwrap(), nat_fam);
        assert_eq!(odd.ty().unwrap(), nat_fam);
        assert_eq!(
            even_zero.ty().unwrap(),
            even.app_in(zero.clone(), &mut ctx).unwrap()
        );
        let even_succ_ty = Pi::try_new(
            nats.clone(),
            odd.app_in(nat_0.clone(), &mut ctx)
                .unwrap()
                .simple_in(even.app_in(succ_nat_0.clone(), &mut ctx).unwrap(), &mut ctx)
                .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(even_succ.ty().unwrap(), even_succ_ty);
        let odd_succ_ty = Pi::try_new(
            nats.clone(),
            even.app_in(nat_0.clone(), &mut ctx)
                .unwrap()
                .simple_in(odd.app_in(succ_nat_0.clone(), &mut ctx).unwrap(), &mut ctx)
                .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(odd_succ.ty().unwrap(), odd_succ_ty);

        // === Check branch types of mutually inductive predicates ===

        let even_family_ty = Pi::try_new(
            nats.clone(),
            even.app_in(nat_0.clone(), &mut ctx)
                .unwrap()
                .simple_in(set.clone(), &mut ctx)
                .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        let even_family_0 = even_family_ty.var_in(0, &mut ctx).unwrap();
        let even_zero_branch = even_family_0
            .app_in(zero.clone(), &mut ctx)
            .unwrap()
            .app_in(even_zero.clone(), &mut ctx)
            .unwrap();
        assert_eq!(
            even.branch_ty(0, &even_family_0, &mut ctx).unwrap(),
            even_zero_branch
        );
        let even_succ_branch = Pi::try_new(
            nats.clone(),
            Pi::try_new(
                odd.app_in(nat_0.clone(), &mut ctx).unwrap(),
                even_family_ty
                    .var_in(2, &mut ctx)
                    .unwrap()
                    .app_in(succ.app_in(nat_1.clone(), &mut ctx).unwrap(), &mut ctx)
                    .unwrap()
                    .app_in(
                        even_succ
                            .app_in(nat_1.clone(), &mut ctx)
                            .unwrap()
                            .app_in(
                                odd.app_in(nat_1.clone(), &mut ctx)
                                    .unwrap()
                                    .var_in(0, &mut ctx)
                                    .unwrap(),
                                &mut ctx,
                            )
                            .unwrap(),
                        &mut ctx,
                    )
                    .unwrap(),
                None,
                &mut ctx,
            )
            .unwrap()
            .into_id_in(&mut ctx),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(
            even.branch_ty(1, &even_family_0, &mut ctx).unwrap(),
            even_succ_branch
        );
        let even_result = Pi::try_new(
            nats.clone(),
            Pi::try_new(
                even.app_in(nat_0.clone(), &mut ctx).unwrap(),
                even_family_ty
                    .var_in(2, &mut ctx)
                    .unwrap()
                    .app_in(nat_1.clone(), &mut ctx)
                    .unwrap()
                    .app_in(
                        even.app_in(nat_1.clone(), &mut ctx)
                            .unwrap()
                            .var_in(0, &mut ctx)
                            .unwrap(),
                        &mut ctx,
                    )
                    .unwrap(),
                None,
                &mut ctx,
            )
            .unwrap()
            .into_id_in(&mut ctx),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(
            even.case_result_ty(&even_family_0, &mut ctx).unwrap(),
            even_result
        );

        let odd_family_ty = Pi::try_new(
            nats.clone(),
            odd.app_in(nat_0.clone(), &mut ctx)
                .unwrap()
                .simple_in(set.clone(), &mut ctx)
                .unwrap(),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        let odd_family_0 = odd_family_ty.var_in(0, &mut ctx).unwrap();
        let odd_succ_branch = Pi::try_new(
            nats.clone(),
            Pi::try_new(
                even.app_in(nat_0.clone(), &mut ctx).unwrap(),
                odd_family_ty
                    .var_in(2, &mut ctx)
                    .unwrap()
                    .app_in(succ.app_in(nat_1.clone(), &mut ctx).unwrap(), &mut ctx)
                    .unwrap()
                    .app_in(
                        odd_succ
                            .app_in(nat_1.clone(), &mut ctx)
                            .unwrap()
                            .app_in(
                                even.app_in(nat_1.clone(), &mut ctx)
                                    .unwrap()
                                    .var_in(0, &mut ctx)
                                    .unwrap(),
                                &mut ctx,
                            )
                            .unwrap(),
                        &mut ctx,
                    )
                    .unwrap(),
                None,
                &mut ctx,
            )
            .unwrap()
            .into_id_in(&mut ctx),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(
            odd.branch_ty(0, &odd_family_0, &mut ctx).unwrap(),
            odd_succ_branch
        );
        let odd_result = Pi::try_new(
            nats.clone(),
            Pi::try_new(
                odd.app_in(nat_0.clone(), &mut ctx).unwrap(),
                odd_family_ty
                    .var_in(2, &mut ctx)
                    .unwrap()
                    .app_in(nat_1.clone(), &mut ctx)
                    .unwrap()
                    .app_in(
                        odd.app_in(nat_1.clone(), &mut ctx)
                            .unwrap()
                            .var_in(0, &mut ctx)
                            .unwrap(),
                        &mut ctx,
                    )
                    .unwrap(),
                None,
                &mut ctx,
            )
            .unwrap()
            .into_id_in(&mut ctx),
            None,
            &mut ctx,
        )
        .unwrap()
        .into_id_in(&mut ctx);
        assert_eq!(
            odd.case_result_ty(&odd_family_0, &mut ctx).unwrap(),
            odd_result
        );

        // === Check case types of mutually inductive predicates ===

        let even_case_ty = even_zero_branch
            .simple_in(
                even_succ_branch
                    .simple_in(even_result.clone(), &mut ctx)
                    .unwrap(),
                &mut ctx,
            )
            .unwrap();
        let even_case = even.case_in(even_family_0.clone(), &mut ctx).unwrap();
        assert_eq!(even_case.ty().unwrap(), even_case_ty);

        let odd_case_ty = odd_succ_branch
            .simple_in(odd_result.clone(), &mut ctx)
            .unwrap();
        let odd_case = odd.case_in(odd_family_0.clone(), &mut ctx).unwrap();
        assert_eq!(odd_case.ty().unwrap(), odd_case_ty);
    }
    */
}
