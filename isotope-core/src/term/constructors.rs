/*!
Convenience constructors for `TermRef`
*/
use super::*;

lazy_static::lazy_static! {
    /// The type of simple types
    pub static ref SET: TermId =
        Universe::set().into_id();

    /// The empty type
    pub static ref EMPTY: TermId = IndFamDef::empty(&mut ()).into_fam_id(&mut ()).unwrap();

    /// The unit type
    pub static ref UNIT: TermId = IndFamDef::unit(&mut ()).into_fam_id(&mut ()).unwrap();

    /// The constructor for the unit type
    pub static ref NIL: TermId = UNIT.con(0).unwrap();

    /// The type of booleans
    pub static ref BOOLS: TermId =
        IndFamDef::bools(&mut ())
            .into_fam_id(&mut ())
            .unwrap();

    /// The true boolean
    pub static ref TRUE: TermId = BOOLS.con(1).unwrap();

    /// The false boolean
    pub static ref FALSE: TermId = BOOLS.con(0).unwrap();

    /// The unary type over the booleans
    pub static ref BOOL_UNARY: TermId = BOOLS.unary().unwrap();

    /// The binary type over the booleans
    pub static ref BOOL_BINARY: TermId = BOOLS.binary().unwrap();

    /// The boolean identity
    pub static ref BOOL_ID: TermId = BOOLS.id_fn().unwrap();

    /// The boolean if-not-statement
    pub static ref BOOL_IF_NOT: TermId = BOOLS.if_not().unwrap();

    /// The boolean NOT gate
    pub static ref NOT: TermId = BOOL_IF_NOT.app(TRUE.clone()).unwrap().app(FALSE.clone()).unwrap();

    /// The boolean AND gate
    pub static ref AND: TermId = BOOL_IF_NOT.app(FALSE.clone()).unwrap();

    /// The boolean OR gate
    pub static ref OR: TermId = BOOL_UNARY.if_not().unwrap()
        .app(BOOL_ID.clone()).unwrap().app(TRUE.const_over(BOOLS.clone()).unwrap()).unwrap();

    /// The boolean XOR gate
    pub static ref XOR: TermId = BOOL_UNARY.if_not().unwrap()
        .app(BOOL_ID.clone()).unwrap().app(NOT.clone()).unwrap();

    /// The type of natural numbers
    //TODO: builtin naturals?
    pub static ref NATS: TermId =
        IndFamDef::nats(&mut ())
            .into_fam_id(&mut ())
            .unwrap();

    /// The unary type over the naturals
    pub static ref NATS_UNARY: TermId = NATS.unary().unwrap();

    /// The binary type over the naturals
    pub static ref NATS_BINARY: TermId = NATS.binary().unwrap();

    /// Zero
    pub static ref ZERO: TermId = NATS.con(0).unwrap();

    /// The successor function
    pub static ref SUCC: TermId = NATS.con(1).unwrap();

    /// One
    pub static ref ONE: TermId = SUCC.app(ZERO.clone()).unwrap();

    /// Two
    pub static ref TWO: TermId = SUCC.app(ONE.clone()).unwrap();

    /// Left-addition of natural numbers
    //TODO: builtin addition?
    pub static ref LEFT_ADD: TermId =
        RecDef::left_add(&mut ())
            .into_id()
            .into_rec(0)
            .unwrap();

    /// Left-multiplication of natural numbers
    //TODO: builtin multiplication?
    pub static ref LEFT_MUL: TermId =
        RecDef::left_mul(&mut ())
            .into_id()
            .into_rec(0)
            .unwrap();
}

impl<'a> TermRef<'a> {
    /// Get the `n`th family for this inductive definition
    #[inline]
    pub fn into_fam(self, ix: VarIx) -> Result<TermId, Error> {
        self.into_fam_in(ix, &mut ())
    }

    /// Get the `n`th family for this inductive definition
    #[inline]
    pub fn into_fam_in(self, ix: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(IndFamily::try_new(self.into_owned(), ix, None, None)?.into_id_in(ctx.cons_ctx()))
    }

    /// Get the `n`th family for this inductive definition
    #[inline]
    pub fn fam(&self, ix: VarIx) -> Result<TermId, Error> {
        self.fam_in(ix, &mut ())
    }

    /// Get the `n`th family for this inductive definition
    #[inline]
    pub fn fam_in(&self, ix: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(IndFamily::try_new(self.clone_into_owned(), ix, None, None)?.into_id_in(ctx.cons_ctx()))
    }

    /// Get the `n`th constructor for this family
    #[inline]
    pub fn con(&self, n: VarIx) -> Result<TermId, Error> {
        self.as_ind_fam()?.con_id(n, &mut ())
    }

    /// Get the `n`th constructor for this family
    #[inline]
    pub fn con_in(&self, n: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        self.as_ind_fam()?.con_id(n, ctx)
    }

    /// Get the `n`th family for an inductive definition variable with this type
    #[inline]
    pub fn into_var_fam(self, var_ix: VarIx, fam_ix: VarIx) -> Result<TermId, Error> {
        self.var_fam_in(var_ix, fam_ix, &mut ())
    }

    /// Get the `n`th family for an inductive definition variable with this type
    #[inline]
    pub fn into_var_fam_in(
        self,
        var_ix: VarIx,
        fam_ix: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(IndFamily::try_new(
            Var::with_ix(var_ix).into_id_in(ctx.cons_ctx()),
            fam_ix,
            Some(self.into_owned()),
            None,
        )?
        .into_id_in(ctx.cons_ctx()))
    }

    /// Get the `n`th family for an inductive definition variable with this type
    #[inline]
    pub fn var_fam(&self, var_ix: VarIx, fam_ix: VarIx) -> Result<TermId, Error> {
        self.var_fam_in(var_ix, fam_ix, &mut ())
    }

    /// Get the `n`th family for an inductive definition variable with this type
    #[inline]
    pub fn var_fam_in(
        &self,
        var_ix: VarIx,
        fam_ix: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(IndFamily::try_new(
            Var::with_ix(var_ix).into_id_in(ctx.cons_ctx()),
            fam_ix,
            Some(self.clone_into_owned()),
            None,
        )?
        .into_id_in(ctx.cons_ctx()))
    }

    /// Get the `n`th recursively defined term in this recursive definition
    #[inline]
    pub fn rec(&self, ix: VarIx) -> Result<TermId, Error> {
        self.rec_in(ix, &mut ())
    }

    /// Get the `n`th recursively defined term in this recursive definition
    #[inline]
    pub fn rec_in(&self, ix: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Rec::try_new(self.clone_into_owned(), ix, None, None, ctx)?.into_id_in(ctx))
    }

    /// Get the `n`th recursively defined term in this recursive definition
    #[inline]
    pub fn into_rec(self, ix: VarIx) -> Result<TermId, Error> {
        self.into_rec_in(ix, &mut ())
    }

    /// Get the `n`th recursively defined term in this recursive definition
    #[inline]
    pub fn into_rec_in(self, ix: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Rec::try_new(self.into_owned(), ix, None, None, ctx)?.into_id_in(ctx))
    }

    /// Get the `n`th recursive definition for a recursive definition variable with this type
    #[inline]
    pub fn var_rec(&self, var_ix: VarIx, rec_ix: VarIx) -> Result<TermId, Error> {
        self.var_rec_in(var_ix, rec_ix, &mut ())
    }

    /// Get the `n`th recursive definition for a recursive definition variable with this type
    #[inline]
    pub fn var_rec_in(
        &self,
        var_ix: VarIx,
        rec_ix: VarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(Rec::try_new(
            Var::with_ix(var_ix).into_id_in(ctx.cons_ctx()),
            rec_ix,
            Some(self.clone_into_owned()),
            None,
            ctx,
        )?
        .into_id_in(ctx.cons_ctx()))
    }

    /// Create a variable of a given type
    #[inline]
    pub fn into_var(self, ix: VarIx) -> Result<TermId, Error> {
        Ok(Var::with_ty(ix, self.into_owned())?.into_id())
    }

    /// Create a variable of a given type in the given context
    #[inline]
    pub fn into_var_in(self, ix: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Var::with_ty(ix, self.into_owned())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a variable of a given type
    #[inline]
    pub fn var(&self, ix: VarIx) -> Result<TermId, Error> {
        self.var_in(ix, &mut ())
    }

    /// Create a variable of a given type in the given context
    #[inline]
    pub fn var_in(&self, ix: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Var::with_ty(ix, self.clone_into_owned())?.into_id_in(ctx.cons_ctx()))
    }

    /// Apply this term to a given type, without normalization
    #[inline]
    pub fn into_app(self, right: TermId) -> Result<TermId, Error> {
        self.into_app_in(right, &mut ())
    }

    /// Apply this term to a given type in the given context, without normalization
    #[inline]
    pub fn into_app_in(self, right: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            App::try_new(self.into_owned(), right, None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Apply this term to a given type , without normalization
    #[inline]
    pub fn app(&self, right: TermId) -> Result<TermId, Error> {
        self.app_in(right, &mut ())
    }

    /// Apply this term to a given type in the given context, without normalization
    #[inline]
    pub fn app_in(&self, right: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            App::try_new(self.clone_into_owned(), right, None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Create a new simple function type
    #[inline]
    pub fn into_simple(self, target: TermId) -> Result<TermId, Error> {
        self.into_simple_in(target, &mut ())
    }

    /// Create a new simple function type
    #[inline]
    pub fn into_simple_in(self, target: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Pi::simple(self.into_owned(), target, ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a new simple function type
    #[inline]
    pub fn simple(&self, target: TermId) -> Result<TermId, Error> {
        self.simple_in(target, &mut ())
    }

    /// Create a new simple function type
    #[inline]
    pub fn simple_in(&self, target: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Pi::simple(self.clone_into_owned(), target, ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a unary function type over this type
    #[inline]
    pub fn into_unary(self) -> Result<TermId, Error> {
        self.into_unary_in(&mut ())
    }

    /// Create a unary function type over this type
    #[inline]
    pub fn into_unary_in(self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Pi::unary(self.into_owned(), ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a unary function type over this type
    #[inline]
    pub fn unary(&self) -> Result<TermId, Error> {
        self.unary_in(&mut ())
    }

    /// Create a unary function type over this type
    #[inline]
    pub fn unary_in(&self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Pi::unary(self.clone_into_owned(), ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a binary function type over this type
    #[inline]
    pub fn into_binary(self) -> Result<TermId, Error> {
        self.into_binary_in(&mut ())
    }

    /// Create a binary function type over this type
    #[inline]
    pub fn into_binary_in(self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Pi::binary(self.into_owned(), ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a binary function type over this type
    #[inline]
    pub fn binary(&self) -> Result<TermId, Error> {
        self.binary_in(&mut ())
    }

    /// Create a binary function type over this type
    #[inline]
    pub fn binary_in(&self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Pi::binary(self.clone_into_owned(), ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create the identity function over this type
    #[inline]
    pub fn into_id_fn(self) -> Result<TermId, Error> {
        self.into_id_fn_in(&mut ())
    }

    /// Create the identity function over this type
    #[inline]
    pub fn into_id_fn_in(self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Lambda::id(self.into_owned(), ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create the identity function over this type
    #[inline]
    pub fn id_fn(&self) -> Result<TermId, Error> {
        self.id_fn_in(&mut ())
    }

    /// Create the identity function over this type
    #[inline]
    pub fn id_fn_in(&self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(Lambda::id(self.clone_into_owned(), ctx.cons_ctx())?.into_id_in(ctx.cons_ctx()))
    }

    /// Create a constant function with a given parameter type
    #[inline]
    pub fn into_const_over(self, param_ty: TermId) -> Result<TermId, Error> {
        self.into_const_over_in(param_ty, &mut ())
    }

    /// Create a constant function with a given parameter type
    #[inline]
    pub fn into_const_over_in(
        self,
        param_ty: TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(Lambda::try_new(
            param_ty,
            self.into_shifted(1, 0, ctx)?,
            None,
            ctx.cons_ctx(),
        )?
        .into_id_in(ctx.cons_ctx()))
    }

    /// Create a constant function with a given parameter type
    #[inline]
    pub fn const_over(&self, param_ty: TermId) -> Result<TermId, Error> {
        self.const_over_in(param_ty, &mut ())
    }

    /// Create a constant function with a given parameter type
    #[inline]
    pub fn const_over_in(&self, param_ty: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            Lambda::try_new(param_ty, self.shifted(1, 0, ctx)?, None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Create a function with a given parameter type
    #[inline]
    pub fn into_abs(self, param_ty: TermId) -> Result<TermId, Error> {
        self.into_abs_in(param_ty, &mut ())
    }

    /// Create a function with a given parameter type
    #[inline]
    pub fn into_abs_in(self, param_ty: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            Lambda::try_new(param_ty, self.into_owned(), None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Create a function with a given parameter type
    #[inline]
    pub fn abs(&self, param_ty: TermId) -> Result<TermId, Error> {
        self.abs_in(param_ty, &mut ())
    }

    /// Create a function with a given parameter type
    #[inline]
    pub fn abs_in(&self, param_ty: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            Lambda::try_new(param_ty, self.clone_into_owned(), None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Construct a case statement over this inductive type given a target family
    #[inline]
    pub fn into_case(self, target: TermId) -> Result<TermId, Error> {
        self.into_case_in(target, &mut ())
    }

    /// Construct a case statement over this inductive type given a target family
    #[inline]
    pub fn into_case_in(self, target: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            Case::try_new(self.into_owned(), target, None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Construct a case statement over this inductive type given a target family
    #[inline]
    pub fn case(&self, target: TermId) -> Result<TermId, Error> {
        self.case_in(target, &mut ())
    }

    /// Construct a case statement over this inductive type given a target family
    #[inline]
    pub fn case_in(&self, target: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        Ok(
            Case::try_new(self.clone_into_owned(), target, None, ctx.cons_ctx())?
                .into_id_in(ctx.cons_ctx()),
        )
    }

    /// Construct a case statement over this inductive type given a target type
    #[inline]
    pub fn simple_case(&self, target: TermId) -> Result<TermId, Error> {
        self.case_in(target.const_over_in(target.clone(), &mut ())?, &mut ())
    }

    /// Construct a case statement over this inductive type given a target type
    #[inline]
    pub fn simple_case_in(&self, target: TermId, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        self.case_in(target.const_over_in(target.clone(), ctx)?, ctx)
    }

    /// Construct an "if statement" over this inductive type
    #[inline]
    pub fn if_not(&self) -> Result<TermId, Error> {
        BOOLS.simple_case(self.clone_into_owned())
    }

    /// Construct an "if statement" over this inductive type
    #[inline]
    pub fn if_not_in(&self, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        BOOLS.simple_case_in(self.clone_into_owned(), ctx)
    }

    /// Create the type of small types
    #[inline]
    pub fn set() -> TermId {
        SET.clone()
    }

    /// Create the type of small types in a given context
    #[inline]
    pub fn set_in(ctx: &mut impl ConsCtx) -> TermId {
        SET.consed(ctx)
    }

    /// Create the empty type
    #[inline]
    pub fn empty() -> TermId {
        EMPTY.clone()
    }

    /// Create the empty type in a given context
    #[inline]
    pub fn empty_in(ctx: &mut impl ConsCtx) -> TermId {
        EMPTY.consed(ctx)
    }

    /// Create the unit type
    #[inline]
    pub fn unit() -> TermId {
        UNIT.clone()
    }

    /// Create the unit type in a given context
    #[inline]
    pub fn unit_in(ctx: &mut impl ConsCtx) -> TermId {
        UNIT.consed(ctx)
    }

    /// Create the unit value
    #[inline]
    pub fn nil() -> TermId {
        NIL.clone()
    }

    /// Create the unit value in a given context
    #[inline]
    pub fn nil_in(ctx: &mut impl ConsCtx) -> TermId {
        NIL.consed(ctx)
    }

    /// Create the booleans
    #[inline]
    pub fn bools() -> TermId {
        BOOLS.clone()
    }

    /// Create the booleans in a given context
    #[inline]
    pub fn bools_in(ctx: &mut impl ConsCtx) -> TermId {
        BOOLS.consed(ctx)
    }

    /// Create the true boolean
    #[inline]
    pub fn true_() -> TermId {
        TRUE.clone()
    }

    /// Create the true boolean
    #[inline]
    pub fn true_in(ctx: &mut impl ConsCtx) -> TermId {
        TRUE.consed(ctx)
    }

    /// Create the false boolean
    #[inline]
    pub fn false_() -> TermId {
        FALSE.clone()
    }

    /// Create the false boolean
    #[inline]
    pub fn false_in(ctx: &mut impl ConsCtx) -> TermId {
        FALSE.consed(ctx)
    }

    /// Create the natural numbers
    #[inline]
    pub fn nats() -> TermId {
        NATS.clone()
    }

    /// Create the natural numbers in a given context
    #[inline]
    pub fn nats_in(ctx: &mut impl ConsCtx) -> TermId {
        NATS.consed(ctx)
    }

    /// Create zero
    #[inline]
    pub fn zero() -> TermId {
        ZERO.clone()
    }

    /// Create zero
    #[inline]
    pub fn zero_in(ctx: &mut impl ConsCtx) -> TermId {
        TRUE.consed(ctx)
    }

    /// Create the successor function
    #[inline]
    pub fn succ() -> TermId {
        SUCC.clone()
    }

    /// Create the successor function
    #[inline]
    pub fn succ_in(ctx: &mut impl ConsCtx) -> TermId {
        SUCC.consed(ctx)
    }

    /// Get left-addition
    #[inline]
    pub fn left_add() -> TermId {
        LEFT_ADD.clone()
    }

    /// Get left-addition in a given context
    #[inline]
    pub fn left_add_in(ctx: &mut impl ConsCtx) -> TermId {
        LEFT_ADD.consed(ctx)
    }

    /// Get left-multiplication
    #[inline]
    pub fn left_mul() -> TermId {
        LEFT_MUL.clone()
    }

    /// Get left-multiplication in a given context
    #[inline]
    pub fn left_mul_in(ctx: &mut impl ConsCtx) -> TermId {
        LEFT_MUL.consed(ctx)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn constant_properties() {
        let constants = [
            &*SET,
            &*EMPTY,
            &*UNIT,
            &*NIL,
            &*BOOLS,
            &*BOOL_UNARY,
            &*TRUE,
            &*FALSE,
            &*BOOL_ID,
            &*BOOL_IF_NOT,
            &*NOT,
            &*AND,
            &*OR,
            &*XOR,
            &*NATS,
            &*ZERO,
            &*ONE,
            &*TWO,
            &*LEFT_ADD,
            &*LEFT_MUL,
        ];
        for constant in constants {
            assert!(constant.nil_tyck());
            assert!(constant.is_closed());
            assert_eq!(constant.fv(), Filter::EMPTY);
        }
    }

    #[test]
    fn boolean_operators() {
        let bool_ = BOOLS.clone();
        let bools = [FALSE.clone(), TRUE.clone()];
        for b in &bools {
            assert_eq!(b.ty().unwrap(), bool_);
        }
        let bool_unary = BOOL_UNARY.clone();
        let bool_binary = BOOL_BINARY.clone();
        let not = NOT.clone();
        assert_eq!(not.ty().unwrap(), bool_unary);
        let and = AND.clone();
        assert_eq!(and.ty().unwrap(), bool_binary);
        let or = OR.clone();
        assert_eq!(or.ty().unwrap(), bool_binary);
        let xor = XOR.clone();
        assert_eq!(xor.ty().unwrap(), bool_binary);
        for x in [true, false] {
            assert_eq!(
                not.app(bools[x as usize].clone())
                    .unwrap()
                    .norm(&mut 100)
                    .unwrap(),
                bools[!x as usize]
            );
            for y in [true, false] {
                assert_eq!(
                    and.app(bools[x as usize].clone())
                        .unwrap()
                        .app(bools[y as usize].clone())
                        .unwrap()
                        .norm(&mut 100)
                        .unwrap(),
                    bools[(x & y) as usize]
                );
                assert_eq!(
                    or.app(bools[x as usize].clone())
                        .unwrap()
                        .app(bools[y as usize].clone())
                        .unwrap()
                        .norm(&mut 100)
                        .unwrap(),
                    bools[(x | y) as usize]
                );
                assert_eq!(
                    xor.app(bools[x as usize].clone())
                        .unwrap()
                        .app(bools[y as usize].clone())
                        .unwrap()
                        .norm(&mut 100)
                        .unwrap(),
                    bools[(x ^ y) as usize]
                );
            }
        }
    }

    #[test]
    fn natural_operators() {
        use arrayvec::ArrayVec;
        const TO_CHECK: usize = 3;
        const GAS: u64 = 1000;

        let nats = NATS.clone();
        let mut small_nats: ArrayVec<TermId, TO_CHECK> = ArrayVec::new();
        small_nats.push(ZERO.clone());
        for _ in 1..TO_CHECK {
            let last = small_nats.last().unwrap().clone();
            small_nats.push(SUCC.app(last).unwrap())
        }
        for nat in &small_nats {
            debug_assert_eq!(nat.ty().unwrap(), nats);
        }
        let nats_binary = NATS_BINARY.clone();
        let add = LEFT_ADD.clone();
        assert_eq!(add.ty().unwrap(), nats_binary);
        let mul = LEFT_MUL.clone();
        assert_eq!(mul.ty().unwrap(), nats_binary);

        for (i, ni) in small_nats.iter().enumerate() {
            for (j, nj) in small_nats.iter().enumerate() {
                let aij = add
                    .app(ni.clone())
                    .unwrap()
                    .app(nj.clone())
                    .unwrap()
                    .norm(&mut (GAS as u64))
                    .unwrap();
                if let Some(aijs) = small_nats.get(i + j) {
                    assert_eq!(aij, *aijs);
                } else {
                    // Check at least commutativity if result not cached
                    let aji = add
                        .app(nj.clone())
                        .unwrap()
                        .app(ni.clone())
                        .unwrap()
                        .norm(&mut (GAS as u64))
                        .unwrap();
                    assert_eq!(aij, aji);
                }
                let mij = mul
                    .app(ni.clone())
                    .unwrap()
                    .app(nj.clone())
                    .unwrap()
                    .norm(&mut (GAS as u64))
                    .unwrap();
                if let Some(mijs) = small_nats.get(i * j) {
                    assert_eq!(mij, *mijs);
                } else {
                    // Check at least commutativity if result not cached
                    let mji = mul
                        .app(nj.clone())
                        .unwrap()
                        .app(ni.clone())
                        .unwrap()
                        .norm(&mut (GAS as u64))
                        .unwrap();
                    assert_eq!(mij, mji);
                }
            }
        }
    }
}
