/*!
Keywords and special characters
*/

/// The set of special characters (including whitespace), represented as a string
pub const SPECIAL_CHARACTERS: &str = "\n\r\t :[](){}#\",;.λ";

/// An assignment
pub const ASSIGN: &str = "=";

/// A function type arrow
pub const ARROW: &str = "->";

/// A mapping arrow
pub const MAPSTO: &str = "=>";

/// A let statement
pub const LET: &str = "#let";

/// An equality check
pub const ASSERT_EQ: &str = "#eq";

/// A lambda function
pub const LAMBDA: &str = "λ";

/// A simple lambda function
pub const SIMPLE_LAMBDA: &str = "#lam";

/// The keyword for typing universes
pub const UNIVERSE: &str = "#U";

/// The keyword for a "hole"
pub const HOLE: &str = "_";

/// Check whether a string which does not contain [special characters](`SPECIAL_CHARACTERS`) is a keyword or number
///
/// Current keywords include the tokens
/// - [`ASSIGN == "="`](`ASSIGN`)
/// - [`MAPSTO == "=>"`](`MAPSTO`)
/// - [`ARROW == "->"`](`ARROW`)
/// - [`HOLE == "_"`](`HOLE`)
/// - Any string starting with a digit `0..9`, which is treated as a natural number
#[inline]
pub fn is_keyword(input: &str) -> bool {
    input == ASSIGN
        || input == MAPSTO
        || input == ARROW
        || input == HOLE
        || input
            .chars()
            .next()
            .map(|c| char::is_digit(c, 10))
            .unwrap_or(false)
}
