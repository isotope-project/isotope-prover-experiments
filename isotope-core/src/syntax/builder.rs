/*!
A builder for `isotope-core` terms which consumes `isotope-core` ASTs
*/
use crate::Error as CoreError;
use hayami::{SymbolMap, SymbolTable};
use smallvec::{smallvec, SmallVec};
use smol_str::SmolStr;
use thiserror::Error;

use super::*;

use ast::{Annotated, Expr, Inductive, Let, Scope, Stmt};

/// A builder for `isotope-core` terms which consumes `isotope-core` ASTs
#[derive(Debug, Clone, Default)]
pub struct Builder {
    /// The current symbol table
    symbols: SymbolTable<SmolStr, TableEntry, ahash::RandomState>,
    /// The current builder level
    level: VarIx,
}

/// `isotope` syntax errors
#[derive(Error, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Error {
    /// A term construction error
    #[error("Term construction error: {0}")]
    CoreError(CoreError),

    // === Builder errors ===
    /// An undefined symbol
    #[error("Undefined symbol")]
    UndefinedSymbol,
    /// Scope stack overflow
    #[error("Scope stack overflow")]
    SymbolOverflow,
    /// Symbol table underflow
    #[error("Attempted to pop scope from empty scope stack")]
    SymbolUnderflow,

    // === External errors ===
    /// A parse error
    #[error("Parse error")]
    ParseError,

    // === Development errors ===
    /// Not implemented
    #[error("Not implemented")]
    NotImplemented,
}

impl From<CoreError> for Error {
    #[inline]
    fn from(err: CoreError) -> Self {
        Error::CoreError(err)
    }
}

// === AST-based builder API ===

impl Builder {
    /// Build an expression, with an optional target type for inference
    #[inline]
    pub fn expr(
        &mut self,
        expr: &Expr,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let result = match expr {
            Expr::Ident(Some(expr)) => self.ident(expr, target, cons)?,
            Expr::Ident(None) => self.infer(target, cons)?,
            Expr::Lambda(expr) => self.lambda(expr, target, cons)?,
            Expr::App(expr) => self.app(expr, target, cons)?,
            Expr::Pi(expr) => self.pi(expr, target, cons)?,
            Expr::Universe(expr) => self.universe(expr, target, cons)?,
            Expr::Case(expr) => self.case(expr, target, cons)?,
            Expr::Annotated(expr) => self.annotated(expr, target, cons)?,
            Expr::Scope(scope) => self.scope(scope, target, cons)?,
            _ => return Err(Error::NotImplemented),
        };
        Ok(result)
    }

    /// Handle a statement, visiting all top-level names defined
    #[inline]
    pub fn stmt(
        &mut self,
        stmt: &Stmt,
        visitor: impl FnMut(SmolStr, TermRef),
        cons: &mut impl ConsCtx,
    ) -> Result<(), Error> {
        match stmt {
            Stmt::Let(stmt) => self.let_(stmt, visitor, cons),
            Stmt::Inductive(stmt) => self.inductive(stmt, visitor, cons),
            Stmt::Recursive(stmt) => self.recursive(stmt, visitor, cons),
        }
    }

    /// Build an identifier
    #[inline]
    pub fn ident(
        &mut self,
        ident: &str,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        if let Some(entry) = self.symbols.get(ident) {
            let result = entry.get(self.level, cons)?;
            if let Some(ty) = target {
                Ok(result.cast_consed(ty, cons)?)
            } else {
                Ok(result)
            }
        } else {
            Err(Error::UndefinedSymbol)
        }
    }

    /// Infer a hole
    #[inline]
    pub fn infer(
        &mut self,
        _target: Option<TermId>,
        _cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Err(Error::CoreError(CoreError::CannotInfer))
    }

    /// Build a lambda function
    #[inline]
    pub fn lambda(
        &mut self,
        lambda: &ast::Lambda,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let param_ty = if let Some(ty) = &lambda.param_ty {
            self.expr(ty, None, cons)?
        } else if let Some(Term::Pi(pi)) = target.as_deref() {
            pi.param_ty().clone()
        } else {
            return Err(Error::CoreError(CoreError::CannotInfer));
        };
        let result_ty = if let Some(Term::Pi(pi)) = target.as_deref() {
            Some(pi.result())
        } else {
            None
        };
        self.push_scope(1)?;
        let result = self.parametrized(
            lambda.param_name.as_ref().into_iter().collect(),
            &param_ty,
            smallvec![(&*lambda.result, result_ty.cloned())],
            cons,
        );
        self.pop_scope(1)?;
        Ok(Lambda::try_new(param_ty, result?.pop().unwrap(), target, cons)?.into_id_in(cons))
    }

    /// Build a function application
    fn app(
        &mut self,
        app: &ast::App,
        mut target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let mut result: Option<TermId> = None;
        for (i, arg) in app.0.iter().enumerate() {
            let mut curr_target = None;
            if i + 1 == app.0.len() {
                std::mem::swap(&mut target, &mut curr_target)
            }
            if let Some(left) = &result {
                let right_target = if let Some(ty) = left.ty() {
                    if let Ok(pi) = ty.as_pi() {
                        Some(pi.param_ty().clone())
                    } else {
                        None
                    }
                } else {
                    None
                };
                let right = self.expr(arg, right_target, cons)?;
                let mut left = None;
                std::mem::swap(&mut result, &mut left);
                result = Some(App::try_new(left.unwrap(), right, curr_target, cons)?.into_id_in(cons));
            } else {
                result = Some(self.expr(arg, curr_target, cons)?);
            }
        }
        if let Some(result) = result {
            Ok(result)
        } else {
            //TODO: unit construction
            Err(Error::NotImplemented)
        }
    }

    /// Build a pi type
    #[inline]
    pub fn pi(
        &mut self,
        pi: &ast::Pi,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let param_ty = self.expr(&pi.param_ty, target.clone(), cons)?;
        let result_target = if let Some(target) = &target {
            Some(target.shifted(1, 0, cons)?)
        } else {
            None
        };
        self.push_scope(1)?;
        let result = self.parametrized(
            pi.param_name.as_ref().into_iter().collect(),
            &param_ty,
            smallvec![(&*pi.result, result_target)],
            cons,
        );
        self.pop_scope(1)?;
        Ok(Pi::try_new(param_ty, result?.pop().unwrap(), target, cons)?.into_id_in(cons))
    }

    /// Build a typing universe
    fn universe(
        &mut self,
        universe: &ast::Universe,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(if let Some(ty) = target {
            Universe::with_level_ty(universe.0, ty)?
        } else {
            Universe::with_level(universe.0)
        }
        .into_id_in(cons))
    }

    /// Build a case statement
    fn case(
        &mut self,
        case: &ast::Case,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let ty = target;
        let family = self.expr(&case.family, None, cons)?;
        //TODO: family type can be inferred from target and/or ty (originally target...), but this is expensive?
        let target = self.expr(&case.target, None, cons)?;
        Ok(Case::try_new(family, target, ty, cons)?.into_id_in(cons))
    }

    /// Build an annotated expression
    fn annotated(
        &mut self,
        expr: &Annotated,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        //TODO: think about this
        //TODO: check equality?
        let ty = self.expr(&expr.ty, None, cons)?;
        let mut expr = self.expr(&expr.expr, Some(ty), cons)?;
        if let Some(target) = target {
            expr = expr.cast_consed(target, cons)?;
        }
        Ok(expr)
    }

    /// Build a scope
    fn scope(
        &mut self,
        scope: &Scope,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        self.push_scope(0)?;
        let result = self.scope_inner(scope, target, cons);
        self.pop_scope(0)?;
        result
    }

    /// Internally handle a scope
    fn scope_inner(
        &mut self,
        scope: &Scope,
        target: Option<TermId>,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        for stmt in &scope.stmts {
            self.stmt(stmt, |_, _| (), cons)?;
        }
        self.expr(&scope.retv, target, cons)
    }

    /// Handle a let-statement
    #[inline]
    pub fn let_(
        &mut self,
        let_: &Let,
        mut visitor: impl FnMut(SmolStr, TermRef),
        cons: &mut impl ConsCtx,
    ) -> Result<(), Error> {
        let ty = let_
            .ty
            .as_deref()
            .map(|ty| self.expr(ty, None, cons))
            .transpose()?;
        let value = self.expr(&let_.value, ty, cons)?;
        if let Some(ident) = &let_.ident {
            visitor(ident.clone(), value.clone());
            self.register_subst(ident.clone(), value)
        }
        Ok(())
    }

    /// Handle an inductive definition
    #[inline]
    pub fn inductive(
        &mut self,
        inductive: &Inductive,
        mut visitor: impl FnMut(SmolStr, TermRef),
        cons: &mut impl ConsCtx,
    ) -> Result<(), Error> {
        let def = self.inductive_def(inductive, cons)?;
        for (fam_ix, family) in inductive.families.iter().enumerate() {
            let fam = def.fam_in(fam_ix as VarIx, cons)?;
            for (con_ix, (name, _)) in family.cons.iter().enumerate() {
                let con = fam.con_in(con_ix as VarIx, cons)?;
                visitor(name.clone(), con.borrow_id());
                self.register_subst(name.clone(), con);
            }
            visitor(family.name.clone(), fam.borrow_id());
            self.register_subst(family.name.clone(), fam);
        }
        Ok(())
    }

    /// Handle a block of recursive definitions
    #[inline]
    pub fn recursive(
        &mut self,
        recursive: &ast::Recursive,
        mut visitor: impl FnMut(SmolStr, TermRef),
        cons: &mut impl ConsCtx,
    ) -> Result<(), Error> {
        self.push_scope(1)?;
        let def = self.recursive_inner(recursive, cons);
        self.pop_scope(1)?;
        let defs = def?.into_id_in(cons);
        for (ix, ident) in recursive
            .defs
            .iter()
            .filter_map(|def| def.ident.as_ref())
            .enumerate()
        {
            let def = defs.rec_in(ix as VarIx, cons)?;
            visitor(ident.clone(), def.borrow_id());
            self.register_subst(ident.clone(), def);
        }
        Ok(())
    }

    /// Internally handle a block of recursive definitions
    fn recursive_inner(
        &mut self,
        recursive: &ast::Recursive,
        cons: &mut impl ConsCtx,
    ) -> Result<RecDef, Error> {
        let mut defs: SmallVec<[TermId; 2]> = SmallVec::with_capacity(recursive.defs.len());
        for def in recursive.defs.iter() {
            if let Some(ident) = &def.ident {
                let ty = def
                    .ty
                    .as_deref()
                    .ok_or(Error::CoreError(CoreError::CannotInfer))?;
                let ty = self.expr(ty, None, cons)?;
                let def = ty.var_rec_in(0, defs.len() as VarIx, cons)?;
                self.register_subst(ident.clone(), def);
                defs.push(ty);
            }
        }
        let mut name_count = 0;
        for def in recursive.defs.iter() {
            if let Some(_ident) = &def.ident {
                let def = self.expr(&def.value, Some(defs[name_count].clone()), cons)?;
                defs[name_count] = def;
                name_count += 1;
            } else {
                let ty = def
                    .ty
                    .as_deref()
                    .map(|ty| self.expr(ty, None, cons))
                    .transpose()?;
                self.expr(&def.value, ty, cons)?;
            }
        }
        Ok(RecDef::try_new(defs)?)
    }

    /// Register a substitution
    #[inline]
    pub fn register_subst(&mut self, ident: SmolStr, value: TermId) {
        self.symbols.insert(
            ident,
            TableEntry {
                term: value,
                level: self.level,
            },
        )
    }

    /// Push a scope
    #[inline]
    pub fn push_scope(&mut self, levels: VarIx) -> Result<(), Error> {
        self.level = self
            .level
            .checked_add(levels)
            .ok_or(Error::SymbolOverflow)?;
        self.symbols.push();
        Ok(())
    }

    /// Pop a scope
    #[inline]
    pub fn pop_scope(&mut self, levels: VarIx) -> Result<(), Error> {
        self.level = self
            .level
            .checked_sub(levels)
            .ok_or(Error::SymbolUnderflow)?;
        self.symbols.pop();
        Ok(())
    }

    /// Build a set of parametrized values
    fn parametrized(
        &mut self,
        param_names: SmallVec<[&SmolStr; 2]>,
        param_ty: &TermId,
        results: SmallVec<[(&Expr, Option<TermId>); 1]>,
        cons: &mut impl ConsCtx,
    ) -> Result<SmallVec<[TermId; 2]>, Error> {
        for &ident in &param_names {
            //TODO: avoid clone for var call here
            //TODO: handle overflow properly
            let value = param_ty
                .shifted(param_names.len() as SVarIx, 0, cons)?
                .var_in(0, cons)?;
            self.register_subst(ident.clone(), value);
        }
        results
            .into_iter()
            .map(|(result, result_ty)| self.expr(result, result_ty, cons))
            .collect()
    }

    /// Build an inductive definition
    fn inductive_def(
        &mut self,
        inductive: &Inductive,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        //TODO: proper overflow handling
        self.push_scope(1)?;
        let def = self.inductive_def_inner(inductive, cons);
        self.pop_scope(1)?;
        def
    }

    /// Internally build an inductive definition
    fn inductive_def_inner(
        &mut self,
        inductive: &Inductive,
        cons: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let def_var = Var::with_ix(0).into_id_in(cons);
        let mut families: SmallVec<[IndFamDef; 1]> =
            SmallVec::with_capacity(inductive.families.len());
        for (fam_ix, family) in inductive.families.iter().enumerate() {
            let ty = self.expr(
                family
                    .ty
                    .as_ref()
                    .ok_or(Error::CoreError(CoreError::CannotInfer))?,
                None,
                cons,
            )?;
            let fam = IndFamily::try_new(def_var.clone(), fam_ix as VarIx, Some(ty.clone()), None)?
                .into_id_in(cons);
            self.register_subst(family.name.clone(), fam);
            families.push(IndFamDef {
                ty,
                cons: SmallVec::new(),
            })
        }
        for (fam_ix, family) in inductive.families.iter().enumerate() {
            families[fam_ix].cons = SmallVec::with_capacity(family.cons.len());
            for (_name, con) in family.cons.iter() {
                families[fam_ix].cons.push(self.expr(con, None, cons)?);
            }
        }
        Ok(IndDef::try_new(families)?.into_id_in(cons))
    }
}

// === Parsing based builder API ===
#[cfg(feature = "parser")]
pub use super::parser::IResult;

#[cfg(feature = "parser")]
impl Builder {
    /// Parse an expression
    pub fn parse_expr<'a>(
        &mut self,
        input: &'a str,
        cons: &mut impl ConsCtx,
    ) -> IResult<&'a str, TermId> {
        nom::combinator::map_res(super::parser::expr, |ast| self.expr(&ast, None, cons))(input)
    }

    /// Parse a statement
    pub fn parse_stmt<'a>(
        &mut self,
        input: &'a str,
        cons: &mut impl ConsCtx,
    ) -> IResult<&'a str, ()> {
        nom::combinator::map_res(super::parser::stmt, |ast| self.stmt(&ast, |_, _| {}, cons))(input)
    }
}

/// An entry in a symbol table
#[derive(Debug, Clone, PartialEq, Eq)]
struct TableEntry {
    /// The substitution
    term: TermId,
    /// The level of this substitution
    level: VarIx,
}

impl TableEntry {
    /// Get an entry at a given level
    fn get(&self, level: VarIx, ctx: &mut impl ConsCtx) -> Result<TermId, Error> {
        let shift = level - self.level;
        self.term
            .shifted(shift as i32, 0, ctx)
            .map_err(Error::CoreError)
    }
}
