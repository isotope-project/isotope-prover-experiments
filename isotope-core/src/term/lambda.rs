/*!
Lambda functions and dependent function types
*/
use super::*;

// Type definitions

/// A lambda function
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Lambda {
    /// This lambda function's code
    code: Code,
    /// The parameter type of this lambda function
    param_ty: TermId,
    /// The result of this lambda function
    result: TermId,
    /// The base type of this lambda function
    base_ty: TermId,
    /// The surface type of this lambda function
    ty: TermId,
    /// This lambda function's free variable set
    fv: Filter,
    /// This lambda function's flags
    flags: Flags,
}

impl Debug for Lambda {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut dbg = f.debug_struct("Lambda");
        dbg.field("param_ty", &self.param_ty)
            .field("result", &self.result);
        if self.ty != self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl Lambda {
    // === Constructors ===

    /// Create a new lambda function with an optional type annotation
    pub fn try_new(
        param_ty: TermId,
        result: TermId,
        ty: Option<TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<Lambda, Error> {
        let (base_ty, is_const) = Self::compute_ty(&param_ty, &result, ctx)?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&param_ty, &result, &ty);
        let flags = Self::compute_flags(&param_ty, &result, is_const, &base_ty, &ty, fv);
        let code = Self::compute_code(&param_ty, &result, &ty);
        Ok(Lambda {
            code,
            param_ty,
            result,
            base_ty,
            ty,
            fv,
            flags,
        })
    }

    // === Getters ===

    /// Get the parameter type of this lambda function
    pub fn param_ty(&self) -> &TermId {
        &self.param_ty
    }

    /// Get the result of this lambda function
    pub fn result(&self) -> &TermId {
        &self.result
    }

    /// Get the base type of this lambda function
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this lambda function
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    // === Property computations ===

    /// Compute the base pi type of a lambda function, also returning whether the result is constant
    pub fn compute_pi(
        param_ty: &TermId,
        result: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<(Pi, bool), Error> {
        //TODO: shifted comparator...
        let shifted_param_ty = param_ty.shifted(1, 0, &mut ())?;
        let mut is_const = true;
        result.visit_transitive_deps_rec(0, &mut |_is_ty, shift, dep| {
            if let Ok(var) = dep.as_var() {
                if var.ix() == shift {
                    is_const = false;
                    if var.base_ty().ok_or(Error::ExpectedTyped)? != *shifted_param_ty {
                        return Err(Error::VarMismatch);
                    }
                }
            }
            Ok(dep.fv().contains(shift).unwrap_or(true))
        })?;
        let pi = Pi::try_new(
            param_ty.clone(),
            result.ty().ok_or(Error::CannotInfer)?.into_id_in(ctx),
            None,
            ctx,
        )?;
        debug_assert_eq!(is_const, !result.has_dep(0));
        Ok((pi, is_const))
    }

    /// Compute the base type of a lambda function, also returning whether the result is constant
    pub fn compute_ty(
        param_ty: &TermId,
        result: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<(TermId, bool), Error> {
        let (pi, is_const) = Self::compute_pi(param_ty, result, ctx)?;
        Ok((pi.into_id_in(ctx), is_const))
    }

    /// Compute the flags of a lambda function
    pub fn compute_flags(
        param_ty: &TermId,
        result: &TermId,
        is_const: bool,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        debug_assert_eq!(is_const, !result.has_dep(0));
        let mut flags = Flags::default();
        flags.set(Flags::IS_IRR, result.is_irr());
        flags.set(Flags::IS_CONST, is_const);
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            result.nil_tyck() && param_ty.nil_tyck() && ty.nil_tyck() && ty == base_ty,
        );
        flags
    }

    /// Compute the free variable set of a lambda function with the given type annotation
    pub fn compute_fv(param_ty: &TermId, result: &TermId, ty: &TermId) -> Filter {
        result.shift_down_fv(1).union(param_ty.fv()).union(ty.fv())
    }

    /// Compute the code of a lambda function with the given optional type annotation
    pub fn compute_code(param_ty: &TermId, result: &TermId, ty: &TermId) -> Code {
        let mut hasher = AHasher::new_with_keys(48457863, 8794393);
        param_ty.code().hash(&mut hasher);
        result.code().hash(&mut hasher);
        ty.code().hash(&mut hasher);
        Code(hasher.finish())
    }

    // === Utility constructors ===

    /// Construct the identity function over a given type
    pub fn id(ty: TermId, ctx: &mut impl ConsCtx) -> Result<Lambda, Error> {
        let result_ty = ty.shifted(1, 0, ctx)?;
        let result = Lambda::try_new(ty, Var::with_ty(0, result_ty)?.into_id_in(ctx), None, ctx)?;
        Ok(result)
    }

    // === Private utility methods ===

    /// Create a new lambda function with the given type and result
    fn new_unchecked(param_ty: TermId, result: TermId, base_ty: TermId, ty: TermId) -> Lambda {
        let fv = Self::compute_fv(&param_ty, &result, &ty);
        let code = Self::compute_code(&param_ty, &result, &ty);
        let flags = Self::compute_flags(&param_ty, &result, !result.has_dep(0), &base_ty, &ty, fv);
        Lambda {
            code,
            param_ty,
            result,
            base_ty,
            ty,
            fv,
            flags,
        }
    }
}

/// A dependent function type
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Pi {
    /// This lambda function's code
    code: Code,
    /// The parameter type of this pi type
    param_ty: TermId,
    /// The result type of this pi type
    result: TermId,
    /// The base type annotation of this pi type
    base_ty: TermId,
    /// The surface type of this pi type, if different from the base type
    ty: TermId,
    /// This lambda function's free variable set
    fv: Filter,
    /// This pi type's flags
    flags: Flags,
}

impl Debug for Pi {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut dbg = f.debug_struct("Pi");
        dbg.field("param_ty", &self.param_ty)
            .field("result", &self.result);
        if self.ty != self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl Pi {
    // === Constructors ===

    /// Create a new pi type with an optional type annotation
    pub fn try_new(
        param_ty: TermId,
        result: TermId,
        ty: Option<TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<Pi, Error> {
        let base_ty = Self::compute_ty(&param_ty, &result, ctx)?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&param_ty, &result, &ty);
        //TODO: optimize
        let flags = Self::compute_flags(&param_ty, &result, &base_ty, &ty, fv);
        let code = Self::compute_code(&param_ty, &result, &ty);
        Ok(Pi {
            code,
            param_ty,
            result,
            base_ty,
            ty,
            fv,
            flags,
        })
    }

    // === Getters ===

    /// Get the parameter type of this pi type
    pub fn param_ty(&self) -> &TermId {
        &self.param_ty
    }

    /// Get the result type of this pi type
    pub fn result(&self) -> &TermId {
        &self.result
    }

    /// Get the base type of this pi type
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this pi type
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    // === Property computations ===

    /// Compute the base type of a pi type
    pub fn compute_ty(
        param_ty: &TermId,
        result: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let shifted_param_ty = param_ty.shifted(1, 0, &mut ())?;
        result.visit_transitive_deps_rec(0, &mut |_is_ty, shift, dep| {
            if let Ok(var) = dep.as_var() {
                if var.ix() == shift {
                    if var.base_ty().ok_or(Error::ExpectedTyped)? != shifted_param_ty {
                        return Err(Error::VarMismatch);
                    }
                }
            }
            Ok(dep.fv().contains(shift).unwrap_or(true))
        })?;
        let param_ty_ty = param_ty.ty().ok_or(Error::CannotInfer)?;
        let result_ty = result.ty().ok_or(Error::CannotInfer)?;
        let result = match param_ty_ty
            .ty()
            .ok_or(Error::CannotInfer)?
            .join_ty(result_ty.borrow_id(), ctx)?
        {
            Binary::This(this) => Ok(this),
            Binary::Left => Ok(param_ty_ty.clone_into_id_in(ctx)),
            Binary::Right => Ok(result_ty.into_id_in(ctx)),
        };
        result
    }

    /// Compute the free variable set of a pi type with the given type annotation
    pub fn compute_fv(param_ty: &TermId, result: &TermId, ty: &TermId) -> Filter {
        result.shift_down_fv(1).union(param_ty.fv()).union(ty.fv())
    }

    /// Compute the code of a pi type with the given optional type annotation
    pub fn compute_code(param_ty: &TermId, result: &TermId, ty: &TermId) -> Code {
        let mut hasher = AHasher::new_with_keys(6325763, 68784928687);
        param_ty.code().hash(&mut hasher);
        result.code().hash(&mut hasher);
        ty.code().hash(&mut hasher);
        Code(hasher.finish())
    }

    /// Compute the flags of a pi type
    pub fn compute_flags(
        param_ty: &TermId,
        result: &TermId,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        let result_flags = result.flags();
        flags.set(Flags::IS_TYPE, true);
        flags.set(Flags::IS_PROP, result_flags.contains(Flags::IS_PROP));
        flags.set(Flags::IS_SET, result_flags.contains(Flags::IS_SET));
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            result_flags.contains(Flags::TYCK_NIL)
                && param_ty.nil_tyck()
                && ty.nil_tyck()
                && ty == base_ty,
        );
        flags
    }

    /// Get whether this pi type is an arity of a given sort
    pub fn is_arity(&self, sort: Option<TermRef>) -> bool {
        self.result.is_arity(sort)
    }

    /// Get the sort of this arity. If this pi type is not an arity, return an error. If this term is it's own arity, return `None`.
    ///
    /// Note this does *not* perform any reduction, and in particular, may return an error for a term but a valid sort for it's redex.
    /// The sort of a redex may also be a subtype of the sort of the unreduced term, if both are arities.
    ///
    /// If `t.arity_sort() == Ok(sort)`, then `t.is_arity(Some(sort))` should return `true`.
    pub fn arity_sort(&self) -> Result<TermRef, Error> {
        self.result.arity_sort_id()
    }

    // === Utility constructors ===

    /// Construct a unary function type over a given type
    #[inline]
    pub fn unary(ty: TermId, ctx: &mut impl ConsCtx) -> Result<Pi, Error> {
        let ty_fv = ty.fv();
        let target = ty.shifted(1, 0, ctx)?;
        let result = Pi::try_new(ty.clone(), target, None, ctx)?;
        debug_assert_eq!(result.fv(), ty_fv);
        Ok(result)
    }

    /// Construct a binary function type over a given type
    #[inline]
    pub fn binary(ty: TermId, ctx: &mut impl ConsCtx) -> Result<Pi, Error> {
        let ty_fv = ty.fv();
        let unary = ty.shifted(1, 0, ctx)?.into_unary_in(ctx)?;
        let result = Pi::try_new(ty, unary, None, ctx)?;
        debug_assert_eq!(result.fv(), ty_fv);
        Ok(result)
    }

    /// Construct a simple function type
    #[inline]
    pub fn simple(src: TermId, target: TermId, ctx: &mut impl ConsCtx) -> Result<Pi, Error> {
        let fv = src.fv().union(target.fv());
        let result = Pi::try_new(src, target.into_shifted(1, 0, ctx)?, None, ctx)?;
        debug_assert_eq!(result.fv(), fv);
        Ok(result)
    }

    /// Swap the root target of this dependent function type with another type, optionally combining this with a cast
    #[inline]
    pub fn swap_root_pi(
        &self,
        ty: TermId,
        shift: SVarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<Pi, Error> {
        Pi::try_new(
            self.param_ty.clone(),
            self.result.swap_root_in(ty, shift + 1, ctx)?,
            None,
            ctx,
        )
    }

    /// Swap the root target of this dependent function type with another type
    #[inline]
    pub fn swap_root(
        &self,
        ty: TermId,
        shift: SVarIx,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        Ok(self.swap_root_pi(ty, shift, ctx)?.into_id_in(ctx))
    }

    // === Private utility methods ===

    /// Create a new pi type function with the given type and result
    fn new_unchecked(param_ty: TermId, result: TermId, base_ty: TermId, ty: TermId) -> Pi {
        let fv = Self::compute_fv(&param_ty, &result, &ty);
        let code = Self::compute_code(&param_ty, &result, &ty);
        let flags = Self::compute_flags(&param_ty, &result, &base_ty, &ty, fv);
        Pi {
            code,
            param_ty,
            result,
            base_ty,
            ty,
            fv,
            flags,
        }
    }
}

// Trait implementations

impl Equiv<Lambda> for Lambda {
    fn term_eq_mut(
        &self,
        other: &Lambda,
        fail_early: bool,
        ctx: &mut impl EqCtxMut,
    ) -> Option<bool> {
        opt_and!(
            fail_early;
            self.ty().term_eq_mut(&other.ty(), fail_early, ctx),
            self.param_ty.term_eq_mut(&other.param_ty, fail_early, ctx),
            self.result.term_eq_mut(&other.result, fail_early, ctx)
        )
    }
}

eq_term!(Lambda);

self_cons! {
    Lambda;
    this, ctx => {
        let param_ty = this.param_ty.cons(ctx);
        let result = this.result.cons(ctx);
        let base_ty = this.base_ty.cons(ctx);
        let ty = this.ty.cons(ctx);
        if param_ty.is_none() && result.is_none() && base_ty.is_none() && ty.is_none() {
            None
        } else {
            Some(Lambda {
                param_ty: param_ty.unwrap_or_else(|| this.param_ty.clone()),
                result: result.unwrap_or_else(|| this.result.clone()),
                base_ty: base_ty.unwrap_or_else(|| this.base_ty.clone()),
                ty: ty.unwrap_or_else(|| this.ty.clone()),
                ..*this
            })
        }
    }
}

impl Cast<Lambda> for Lambda {
    fn cast(&self, cast: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<Lambda>, Error> {
        if !cast.is_ty() {
            return Err(Error::ExpectedType);
        }
        if cast == self.ty {
            Ok(None)
        } else {
            //TODO: avoid recomputing result free variable set?
            Ok(Some(Lambda::new_unchecked(
                self.param_ty.consed(ctx),
                self.result.consed(ctx),
                self.base_ty.consed(ctx),
                cast.into_owned(),
            )))
        }
    }
}

impl Subst<Lambda> for Lambda {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Lambda>, Error> {
        let param_ty = self.param_ty.subst(ctx)?;
        ctx.push_param(Some(&self.param_ty))?;
        let result = self.result.subst(ctx);
        ctx.pop_param()?;
        let result = result?;
        let ty = self.ty.subst(ctx)?;
        if param_ty.is_none() && result.is_none() && ty.is_none() {
            Ok(None)
        } else {
            Self::try_new(
                param_ty.unwrap_or_else(|| self.param_ty.consed(ctx.cons_ctx())),
                result.unwrap_or_else(|| self.result.consed(ctx.cons_ctx())),
                Some(ty.unwrap_or_else(|| self.ty.consed(ctx.cons_ctx()))),
                ctx.cons_ctx(),
            )
            .map(Some)
        }
    }
}

value_impl!(Lambda);

impl Value for Lambda {
    #[inline]
    fn into_term(self) -> Term {
        Term::Lambda(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.ty.borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.base_ty.borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.ty, &self.base_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.result.tyck_mut(fail_early, ctx),
            self.param_ty.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, _other: &Term) -> bool {
        false
    }

    #[inline]
    fn is_universe(&self) -> bool {
        false
    }

    #[inline]
    fn is_ty(&self) -> bool {
        false
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.param_ty.var_within(equiv, min, max) ||
            self.result.var_within(equiv, min, max) ||
            //NOTE: base_ty is implied by param_ty and result
            self.ty.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        // Lambda functions are not roots due to eta-reduction!
        false
    }

    #[inline]
    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        if let Some(arg) = args.pop_arg()? {
            ctx.push_subst(arg, Some(&self.param_ty), false)?;
            let result = self.result.apply(args, ctx);
            ctx.pop_subst(false)?;
            if let Some(result) = result? {
                Ok(Some(result))
            } else {
                self.result.shifted(-1, 0, ctx.cons_ctx()).map(Some)
            }
        } else {
            //TODO: think about this...
            self.subst(ctx)
        }
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.param_ty.visit_transitive_deps_rec(shift, visitor)?;
        self.result.visit_transitive_deps_rec(shift + 1, visitor)?;
        Ok(())
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        trans(hasher, &self.ty);
        trans(hasher, &self.param_ty);
        trans(hasher, &self.result)
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.param_ty.cmp(&other.param_ty))
            .then_with(|| self.result.cmp(&other.result))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

impl Equiv<Pi> for Pi {
    fn term_eq_mut(&self, other: &Pi, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            self.ty().term_eq_mut(&other.ty(), fail_early, ctx),
            self.param_ty.term_eq_mut(&other.param_ty, fail_early, ctx),
            self.result.term_eq_mut(&other.result, fail_early, ctx)
        )
    }
}

eq_term!(Pi);

self_cons! {
    Pi;
    this, ctx => {
        let param_ty = this.param_ty.cons(ctx);
        let result = this.result.cons(ctx);
        let base_ty = this.base_ty.cons(ctx);
        let ty = this.ty.cons(ctx);
        if param_ty.is_none() && result.is_none() && base_ty.is_none() && ty.is_none() {
            None
        } else {
            Some(Pi {
                param_ty: param_ty.unwrap_or_else(|| this.param_ty.clone()),
                result: result.unwrap_or_else(|| this.result.clone()),
                base_ty: base_ty.unwrap_or_else(|| this.base_ty.clone()),
                ty: ty.unwrap_or_else(|| this.ty.clone()),
                ..*this
            })
        }
    }
}

impl Cast<Pi> for Pi {
    fn cast(&self, cast: TermRef, _ctx: &mut impl ConsCtx) -> Result<Option<Pi>, Error> {
        if !cast.is_ty() {
            return Err(Error::ExpectedType);
        }
        if cast == self.ty {
            Ok(None)
        } else {
            Ok(Some(Pi::new_unchecked(
                self.param_ty.clone(),
                self.result.clone(),
                self.base_ty.clone(),
                cast.into_owned(),
            )))
        }
    }
}

impl Subst<Pi> for Pi {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Pi>, Error> {
        let param_ty = self.param_ty.subst(ctx)?;
        ctx.push_param(Some(&self.param_ty))?;
        let result = self.result.subst(ctx);
        ctx.pop_param()?;
        let result = result?;
        let ty = self.ty.subst(ctx)?;
        if param_ty.is_none() && result.is_none() && ty.is_none() {
            Ok(None)
        } else {
            Self::try_new(
                param_ty.unwrap_or_else(|| self.param_ty.consed(ctx.cons_ctx())),
                result.unwrap_or_else(|| self.result.consed(ctx.cons_ctx())),
                Some(ty.unwrap_or_else(|| self.ty.consed(ctx.cons_ctx()))),
                ctx.cons_ctx(),
            )
            .map(Some)
        }
    }
}

value_impl!(Pi);

impl Value for Pi {
    #[inline]
    fn into_term(self) -> Term {
        Term::Pi(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.ty.borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.base_ty.borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.ty, &self.base_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.result.tyck_mut(fail_early, ctx),
            self.param_ty.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        //TODO: more advanced subtyping?
        self.term_eq_mut(other, true, &mut Structural(true))
            .unwrap_or(false)
    }

    #[inline]
    fn is_universe(&self) -> bool {
        false
    }

    #[inline]
    fn is_ty(&self) -> bool {
        true
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.param_ty.var_within(equiv, min, max) ||
            self.result.var_within(equiv, min, max) ||
            //NOTE: base_ty is implied by param_ty and result
            self.ty.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        true
    }

    fn apply_ty(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        if let Some(arg) = args.pop_arg()? {
            ctx.push_subst(arg, Some(&self.param_ty), false)?;
            let result = self.result.apply_ty(args, ctx);
            ctx.pop_subst(false)?;
            if let Some(result) = result? {
                Ok(Some(result))
            } else {
                self.result.shifted(-1, 0, ctx.cons_ctx()).map(Some)
            }
        } else {
            self.subst(ctx)
        }
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.param_ty.visit_transitive_deps_rec(shift, visitor)?;
        self.result.visit_transitive_deps_rec(shift + 1, visitor)
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        if let Some(ty) = &self.ty() {
            trans(hasher, ty)
        } else {
            0x546435.hash(hasher)
        }
        trans(hasher, &self.param_ty);
        trans(hasher, &self.result)
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.param_ty.cmp(&other.param_ty))
            .then_with(|| self.result.cmp(&other.result))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

/// Iterate over the parameter types of a pi type or lambda function, along with whether the given parameters are constant
///
/// On termination, contains the result type of the pi type/lambda function.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Params<'a>(pub TermRef<'a>);

impl<'a> Iterator for Params<'a> {
    type Item = TermRef<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let (param, result) = match self.0.try_borrow() {
            Either::Left(this) => match this {
                Term::Pi(curr) => (curr.param_ty.borrow_id(), curr.result.borrow_id()),
                Term::Lambda(curr) => (curr.param_ty.borrow_id(), curr.result.borrow_id()),
                _ => return None,
            },
            Either::Right(this) => match this {
                Term::Pi(curr) => (curr.param_ty.clone(), curr.result.clone()),
                Term::Lambda(curr) => (curr.param_ty.clone(), curr.result.clone()),
                _ => return None,
            },
        };
        self.0 = result;
        return Some(param);
    }
}

#[cfg(test)]
mod test {
    use crate::subst::stack::SubstStack;

    use super::*;

    #[test]
    fn set_id() {
        let mut cons = HashCtx::default();
        let set = Universe::set().into_id_in(&mut cons);
        let a = Var::with_ty(35, set.clone()).unwrap().into_id_in(&mut cons);
        let set_id = Lambda::id(set.clone(), &mut cons)
            .unwrap()
            .into_id_in(&mut cons);
        assert_eq!(set_id.arity(), 1);
        let mut ctx = SubstStack::new(&mut cons);
        let mut argv = vec![a.clone()];
        assert_eq!(set_id.apply(&mut argv, &mut ctx).unwrap().unwrap(), a);
        assert_eq!(argv.len(), 0);
    }

    #[test]
    fn polymorphic_set_id() {
        let mut cons = HashCtx::default();
        let set = Universe::set();
        let typ = set.succ();
        let set = set.into_id_in(&mut cons);
        let typ = typ.into_id_in(&mut cons);
        let a = Var::with_ty(0, set.clone()).unwrap().into_id_in(&mut cons);
        assert_eq!(a.arity(), 0);
        let id_a = Lambda::id(a.clone(), &mut cons)
            .unwrap()
            .into_id_in(&mut cons);
        assert!(!id_a.is_closed());
        assert!(!id_a.is_irr());
        assert!(!id_a.is_const());
        assert!(id_a.nil_tyck());
        assert_eq!(id_a.arity(), 1);
        let poly_id = Lambda::try_new(set.clone(), id_a.clone(), None, &mut cons)
            .unwrap()
            .into_id_in(&mut cons);
        assert!(poly_id.is_closed());
        assert!(poly_id.nil_tyck());
        assert!(!poly_id.is_irr());
        //TODO: change this, if the only dependencies on the zero dep are in types?
        assert!(!poly_id.is_const());
        assert_eq!(poly_id.arity(), 2);
        assert_eq!(poly_id.ty().unwrap().ty().unwrap(), typ);
        let mut argv = vec![a.clone()];
        let mut ctx = SubstStack::new(&mut cons);
        assert_eq!(poly_id.apply(&mut argv, &mut ctx).unwrap().unwrap(), id_a);
        assert_eq!(argv.len(), 0);
        let b = Var::with_ty(1, set.clone())
            .unwrap()
            .into_id_in(ctx.cons_ctx());
        let x = Var::with_ty(0, b.clone())
            .unwrap()
            .into_id_in(ctx.cons_ctx());
        argv.push(x.clone());
        argv.push(b.clone());
        assert_eq!(poly_id.apply(&mut argv, &mut ctx).unwrap().unwrap(), x);
        assert_eq!(argv.len(), 0);
    }
}
