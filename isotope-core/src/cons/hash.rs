/*!
Hash-consing via `hashbrown`
*/
use super::*;
use hashbrown::raw::RawTable;

/// A consing context based off a `hashbrown` table
#[derive(Clone, Default)]
pub struct HashCtx {
    /// The underlying raw table of consed terms
    table: RawTable<TermId>,
}

impl HashCtx {
    /// Create a new hash context with a given capacity
    #[inline]
    pub fn with_capacity(capacity: usize) -> HashCtx {
        HashCtx {
            table: RawTable::with_capacity(capacity),
        }
    }
    /// Get the length of this context
    #[inline]
    pub fn len(&self) -> usize {
        self.table.len()
    }
    /// Get the capacity of this context
    #[inline]
    pub fn capacity(&self) -> usize {
        self.table.capacity()
    }
    /// Get the number of buckets in this context
    #[inline]
    pub fn buckets(&self) -> usize {
        self.table.buckets()
    }
}

impl Debug for HashCtx {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("HashCtx")
            .field("len", &self.table.len())
            .field("buckets", &self.table.buckets())
            .field("capacity", &self.table.capacity())
            .finish()
    }
}

impl ConsCtx for HashCtx {
    type ConsCtx = HashCtx;

    #[inline]
    fn is_consed(&self, term: &TermRef<'_>) -> bool {
        if let Some(cons) = self.try_cons(&**term) {
            cons.ptr_eq(term)
        } else {
            false
        }
    }

    #[inline]
    fn try_cons(&self, term: &Term) -> Option<&TermId> {
        self.table.get(term.code().0, |id| id == term)
    }

    #[inline]
    fn shallow_cons(&mut self, id: TermRef<'_>) -> Option<TermId> {
        if let Some(old) = self.try_cons(&*id) {
            Some(old.clone())
        } else {
            self.table
                .insert((*id).code().0, id.clone_into_owned(), |id| id.code().0);
            None
        }
    }

    #[inline]
    fn pointer_exact(&self) -> bool {
        false
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self
    }

    #[inline]
    fn uncons(&self, code: Code) -> Option<TermId> {
        self.table.get(code.0, |id| id.code() == code).cloned()
    }
}
