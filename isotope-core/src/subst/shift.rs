/*!
Simple shift-based substitution context
*/
use crate::*;

/// A simple, shift-based context
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Shift<C> {
    /// The shift to apply
    pub shift: SVarIx,
    /// The base for the shift
    pub base: VarIx,
    /// The underlying cons context
    pub cons: C,
}

//TODO: fixme

impl<C> Shift<C> {
    /// Get a shifted index
    pub fn shift_ix(shift: SVarIx, base: VarIx, ix: VarIx) -> Result<VarIx, Error> {
        if ix < base {
            Ok(ix)
        } else if shift < 0 {
            let result = ix
                .checked_sub(shift.checked_neg().ok_or(Error::VarIxOverflow)? as VarIx)
                .ok_or(Error::ParamUnderflow)?;
            if result < base {
                Err(Error::ParamUnderflow)
            } else {
                Ok(result)
            }
        } else {
            ix.checked_add(shift as VarIx).ok_or(Error::VarIxOverflow)
        }
    }
}

impl<C> SubstCtx for Shift<C>
where
    C: ConsCtx,
{
    type ConsCtx = C::ConsCtx;

    #[inline]
    fn is_null(&self) -> bool {
        self.shift == 0
    }

    #[inline]
    fn try_subst(&mut self, term: &Term) -> Result<Option<Option<TermId>>, Error> {
        if term.fv().max() < self.base {
            Ok(Some(None))
        } else {
            Ok(None)
        }
    }

    #[inline]
    fn subst_ix(
        &mut self,
        ix: VarIx,
        base_ty: Option<&TermId>,
        ty: Option<&TermId>,
    ) -> Result<Option<TermId>, Error> {
        let six = Self::shift_ix(self.shift, self.base, ix)?;
        if six == ix {
            Ok(None)
        } else {
            Ok(Some(
                Var::try_new(six, base_ty.subst_cons(self)?, ty.subst_cons(self)?)?
                    .into_id_in(self.cons.cons_ctx()),
            ))
        }
    }

    #[inline]
    fn push_param(&mut self, _ty: Option<&TermId>) -> Result<(), Error> {
        self.push_params(1)
    }

    #[inline]
    fn pop_param(&mut self) -> Result<(), Error> {
        self.pop_params(1)
    }

    #[inline]
    fn push_params(&mut self, n: VarIx) -> Result<(), Error> {
        self.base = self.base.checked_add(n).ok_or(Error::VarIxOverflow)?;
        Ok(())
    }

    #[inline]
    fn pop_params(&mut self, n: VarIx) -> Result<(), Error> {
        self.base = self.base.checked_sub(n).ok_or(Error::VarIxOverflow)?;
        Ok(())
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self.cons.cons_ctx()
    }
}
