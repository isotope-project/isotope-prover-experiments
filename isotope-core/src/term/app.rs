/*!
Applications of `isotope` terms
*/

use crate::*;

/// A function application
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct App {
    /// The code of this application
    code: Code,
    /// The function being applied
    left: TermId,
    /// The value it is being applied to
    right: TermId,
    /// The base type of this application
    base_ty: TermId,
    /// The surface type of this application
    ty: TermId,
    /// The free variable set of this application
    fv: Filter,
    /// This application's flags
    flags: Flags,
}

impl Debug for App {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut dbg = f.debug_struct("App");
        dbg.field("left", &self.left).field("right", &self.right);
        if &self.ty != &self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl App {
    // === Constructors ===

    /// Create a new application with an optional type annotation
    pub fn try_new(
        left: TermId,
        right: TermId,
        ty: Option<TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<App, Error> {
        let base_ty = Self::compute_ty(&left, &right, ctx)?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&left, &right, &base_ty, &ty);
        let flags = Self::compute_flags(&left, &right, &base_ty, &ty, fv);
        let code = Self::compute_code(&left, &right, &base_ty, &ty);
        Ok(App {
            code,
            left,
            right,
            base_ty,
            ty,
            fv,
            flags,
        })
    }

    // === Getters ===

    /// Get the left-hand-side of this application
    #[inline]
    pub fn left(&self) -> &TermId {
        &self.left
    }

    /// Get the right-hand-side of this application
    #[inline]
    pub fn right(&self) -> &TermId {
        &self.right
    }

    /// Get the base type of this pi type
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this pi type
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    // === Property computations ===

    /// Compute the base type of an application
    pub fn compute_ty(
        left: &TermId,
        right: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let left_ty = left.ty().ok_or(Error::CannotInfer)?;
        let result = left_ty
            .apply_ty(&mut vec![right.clone()], &mut SubstStack::new(&mut *ctx))?
            .unwrap_or_else(|| left_ty.clone_into_owned());
        Ok(result)
    }

    /// Compute the free variable set of an application with the given optional type annotation
    pub fn compute_fv(left: &TermId, right: &TermId, base_ty: &TermId, ty: &TermId) -> Filter {
        left.fv()
            .union(right.fv())
            .union(base_ty.fv())
            .union(ty.fv())
    }

    /// Compute the code of a lambda function with the given optional type annotation
    pub fn compute_code(left: &TermId, right: &TermId, base_ty: &TermId, ty: &TermId) -> Code {
        let mut hasher = AHasher::new_with_keys(55642356, 576436757);
        left.code().hash(&mut hasher);
        right.code().hash(&mut hasher);
        base_ty.code().hash(&mut hasher);
        ty.code().hash(&mut hasher);
        Code(hasher.finish())
    }

    /// Compute the flags of an application
    pub fn compute_flags(
        left: &TermId,
        right: &TermId,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        let is_irr = base_ty.is_prop() || ty.is_prop();
        flags.set(Flags::IS_IRR, is_irr);
        flags.set(Flags::IS_TYPE, base_ty.is_universe() || ty.is_universe());
        flags.set(
            Flags::IS_PROP,
            base_ty.is_prop_universe() || ty.is_prop_universe(),
        );
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            left.nil_tyck() && right.nil_tyck() && ty.nil_tyck() && ty == base_ty,
        );
        flags
    }
}

impl PartialEq<Term> for App {
    fn eq(&self, other: &Term) -> bool {
        match other {
            Term::App(other) => self.eq(other),
            _ => false,
        }
    }
}

impl Equiv<App> for App {
    fn term_eq_mut(&self, other: &App, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            self.ty().term_eq_mut(&other.ty(), fail_early, ctx),
            if let Some(true) = opt_and!(
                false;
                self.left.term_eq_mut(&other.left, fail_early, ctx),
                self.right.term_eq_mut(&other.right, fail_early, ctx)
            ) {
                Some(true)
            } else {
                None
            }
        )
    }
}

impl Equiv<Term> for App {
    fn term_eq_mut(&self, other: &Term, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        match other {
            Term::App(other) => self.term_eq_mut(other, fail_early, ctx),
            _ => None,
        }
    }
}

self_cons! {
    App;
    this, ctx => {
        let left = this.left.cons(ctx);
        let right = this.right.cons(ctx);
        let base_ty = this.base_ty.cons(ctx);
        let ty = this.ty.cons(ctx);
        if left.is_none() && right.is_none() && base_ty.is_none() && ty.is_none() {
            None
        } else {
            Some(App {
                left: left.unwrap_or_else(|| this.left.clone()),
                right: right.unwrap_or_else(|| this.right.clone()),
                base_ty: base_ty.unwrap_or_else(|| this.base_ty.clone()),
                ty: ty.unwrap_or_else(|| this.ty.clone()),
                ..*this
            })
        }
    }
}

impl Cast<App> for App {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<App>, Error> {
        if self.ty == ty {
            Ok(self.cons(ctx))
        } else {
            //TODO: avoid recomputing base type
            App::try_new(
                self.left.consed(ctx),
                self.right.consed(ctx),
                Some(ty.into_owned()),
                ctx,
            )
            .map(Some)
        }
    }
}

impl Subst<App> for App {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<App>, Error> {
        let left = self.left.subst(ctx)?;
        let right = self.right.subst(ctx)?;
        let ty = self.ty.subst(ctx)?;
        if left.is_none() && right.is_none() && ty.is_none() {
            Ok(None)
        } else {
            Self::try_new(
                left.unwrap_or_else(|| self.left.consed(ctx.cons_ctx())),
                right.unwrap_or_else(|| self.right.consed(ctx.cons_ctx())),
                Some(ty.unwrap_or_else(|| self.ty.consed(ctx.cons_ctx()))),
                ctx.cons_ctx(),
            )
            .map(Some)
        }
    }
}

value_impl!(App);

impl Value for App {
    #[inline]
    fn into_term(self) -> Term {
        Term::App(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.ty.borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.base_ty.borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        let left_ty = self.left.ty()
            .expect("Application of untyped values is not allowed; this should have failed at construction time!");
        let param_ty = left_ty
        .as_pi()
        .expect("The surface type of an applied function must be a pi type; this should have failed at construction time!")
        .param_ty();
        let right_ty = self.right.ty().expect("Application to untyped values is not allowed; this should have failed at construction time!");
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.base_ty, &self.ty, fail_early),
            //TODO: allow subtyping?
            ctx.constrain_mut(&param_ty, &right_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.left.tyck_mut(fail_early, ctx),
            self.right.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn is_subty(&self, other: &Term) -> bool {
        //FIXME: semi-untyped?
        //FIXME: check this is a type?
        self == other
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        false
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.left.var_within(equiv, min, max) ||
            self.right.var_within(equiv, min, max) ||
            //NOTE: base_ty is implied by left and right
            self.ty.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        args.push_arg(self.right.subst_cons(ctx)?)?;
        match self.left.apply(args, ctx) {
            Ok(Some(result)) => Ok(Some(result)),
            Ok(None) => Ok(Some(self.left.consed(ctx.cons_ctx()))),
            Err(err) => {
                args.pop_arg()?;
                Err(err)
            }
        }
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        self.left.app_root().or(Some(&self.left))
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.right.visit_transitive_deps_rec(shift, visitor)?;
        self.left.visit_transitive_deps_rec(shift, visitor)
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        if let Some(ty) = &self.ty() {
            trans(hasher, ty)
        } else {
            0x56352335.hash(hasher)
        }
        trans(hasher, &self.left);
        trans(hasher, &self.right)
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.left.cmp(&other.left))
            .then_with(|| self.right.cmp(&other.right))
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }
}

/// Iterate over the arguments of an application
///
/// On termination, contains the root function being applied
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Args<'a>(pub TermRef<'a>);

impl<'a> Iterator for Args<'a> {
    type Item = TermRef<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let (left, right) = match self.0.try_borrow() {
            Either::Left(term) => match term {
                Term::App(app) => (app.left.borrow_id(), app.right.borrow_id()),
                _ => return None,
            },
            Either::Right(term) => match term {
                Term::App(app) => (app.left.clone(), app.right.clone()),
                _ => return None,
            },
        };
        self.0 = left;
        Some(right)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn set_id() {
        let mut cons = HashCtx::default();
        let set = Universe::set().into_id_in(&mut cons);
        let a = Var::with_ty(35, set.clone()).unwrap().into_id_in(&mut cons);
        let set_id = Lambda::id(set.clone(), &mut cons)
            .unwrap()
            .into_id_in(&mut cons);
        let set_id_a = App::try_new(set_id, a.clone(), None, &mut cons)
            .unwrap()
            .into_id_in(&mut cons);
        let mut ctx = SubstStack::new(&mut cons);
        assert_ne!(set_id_a, a);
        assert_eq!(a.ty().unwrap(), set);
        assert_eq!(set_id_a.ty().unwrap(), set);
        assert_eq!(set_id_a.norm_cons_in(&mut 10, &mut ctx).unwrap(), a)
    }
}
