/*!
Error codes and associated utilities
*/
use thiserror::Error;

/// An error code
#[derive(Error, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Error {
    /// Expected a type
    #[error("Expected a type")]
    ExpectedType,
    /// Expected a variable
    #[error("Expected a variable")]
    ExpectedVar,
    /// Expected a function
    #[error("Expected a function")]
    ExpectedFn,
    /// Expected a function type
    #[error("Expected a function type")]
    ExpectedPi,
    /// Expected a typing universe
    #[error("Expected a typing universe")]
    ExpectedUni,
    /// Expected a typed term
    #[error("Expected a typed term")]
    ExpectedTyped,
    /// Expected a fully applied root
    #[error("Expected a fully applied root")]
    ExpectedBaseRoot,
    /// Invalid cyclic dependency
    #[error("Invalid cyclic dependency")]
    CyclicDep,
    /// A term cannot be inferred
    #[error("A term cannot be inferred")]
    CannotInfer,
    /// Two terms cannot be joined
    #[error("Two terms cannot be joined")]
    CannotJoin,
    /// A type cannot be fully applied
    #[error("A type cannot be fully applied")]
    CannotApplyTy,
    /// Variable index overflow
    #[error("Variable index overflow")]
    VarIxOverflow,
    /// Parameter underflow
    #[error("Parameter underflow")]
    ParamUnderflow,
    /// Variable base type mismatch
    #[error("Variable base type mismatch")]
    VarMismatch,
    /// Recursive definition variable type mismatch
    #[error("Recursive definition variable type mismatch")]
    RecVarMismatch,
    /// Inductive definition variable type mismatch
    #[error("Inductive definition variable type mismatch")]
    IndVarMismatch,
    /// Inductive type mismatch in case statement
    #[error("Inductive type mismatch in case statement")]
    IndCaseMismatch,
    /// Attempted to cast an untyped value
    #[error("Attempted to cast an untyped value")]
    UntypedCast,
    /// Expected an inductive definition
    #[error("Expected an inductive definition")]
    ExpectedIndDef,
    /// Expected an inductive family
    #[error("Expected an inductive family")]
    ExpectedIndFam,
    /// Expected an inductive constructor
    #[error("Expected an inductive constructor")]
    ExpectedIndCons,
    /// Expected a recursive definition
    #[error("Expected a recursive definition")]
    ExpectedRecDef,
    /// Expected a recursively defined term
    #[error("Expected a recursively defined term")]
    ExpectedRec,
    /// Inductive family index out of bounds
    #[error("Inductive family index out of bounds")]
    IndFamIxOob,
    /// Inductive constructor index out of bounds
    #[error("Inductive family index out of bounds")]
    IndConsIxOob,
    /// Internal invariant failure
    #[error("Internal invariant failure")]
    Invariant,
    /// An incomplete application
    #[error("An incomplete application")]
    IncompleteApplication,
    /// Recursive definition index out of bounds
    #[error("Recursive definition index out of bounds")]
    RecIxOob,
    /// Expected an arity
    #[error("Expected an arity")]
    ExpectedArity,
    /// Expected a type of constructor
    #[error("Expected a type of constructor")]
    ExpectedTypeOfConstructor,
    /// Constructor failed positivity condition
    #[error("Constructor failed positivity condition")]
    FailedPositivityCondition,
    /// Could not find a termination order
    #[error("Could not find a termination order")]
    Nonterminating,
    /// Expected a null substitution
    #[error("Expected a null substitution")]
    ExpectedNullSubst,
    /// Failed to normalize a term in the requested number of steps
    #[error("Failed to normalize a term in the requested number of steps")]
    OutOfGas,
    /// An invalid term index was provided
    #[error("An invalid term index was provided")]
    InvalidTermIx,
}
