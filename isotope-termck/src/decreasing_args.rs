/*!
Mappings from definitions to their decreasing arguments
*/
use smallvec::SmallVec;

/// A map of definitions to their decreasing arguments
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DecreasingArgs {
    no_defs: usize,
    mapping: DecreasingArgsInner,
}

impl DecreasingArgs {
    /// Construct a mapping from `no_defs` definitions to slices of decreasing arguments, starting out empty
    #[inline]
    pub fn new(no_defs: usize) -> DecreasingArgs {
        DecreasingArgs {
            no_defs,
            mapping: DecreasingArgsInner::new(no_defs),
        }
    }

    /// Construct a mapping from `no_defs` definitions to slices of decreasing arguments, starting out empty, with `capacity` additional capacity
    #[inline]
    pub fn with_capacity(no_defs: usize, capacity: usize) -> DecreasingArgs {
        DecreasingArgs {
            no_defs,
            mapping: DecreasingArgsInner::with_capacity(no_defs, capacity),
        }
    }

    /// Extend the definition `def` with decreasing argument `arg`.
    ///
    /// Unspecified (but safe) behaviour if there are any arguments in any of the definitions after `def` or if `self.no_defs() <= def`
    #[inline]
    pub fn push(&mut self, def: usize, arg: usize) {
        self.mapping.push(def, arg);
    }

    /// Iterate over each component's decreasing argument set
    #[inline]
    pub fn iter(&self) -> impl Iterator<Item=&[usize]> + Clone {
        (0..self.no_defs).map(|def| self.mapping.get(def, self.no_defs))
    }

    /// Get the decreasing arguments for a component, given the maximum component
    #[inline]
    pub fn get(&self, def: usize) -> Option<&[usize]> {
        if def < self.no_defs {
            Some(self.mapping.get(def, self.no_defs))
        } else {
            None
        }
    }

    /// Mutably get the decreasing arguments for a component, given the maximum component
    #[inline]
    pub fn get_mut(&mut self, def: usize) -> Option<&mut [usize]> {
        if def < self.no_defs {
            Some(self.mapping.get_mut(def, self.no_defs))
        } else {
            None
        }
    }

    /// Get the number of definitions in this map
    #[inline]
    pub fn no_defs(&self) -> usize {
        self.no_defs
    }
}

/// A map of components to their decreasing arguments
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DecreasingArgsInner {
    mapping: SmallVec<[usize; 2]>,
}

impl DecreasingArgsInner {
    /// Construct a mapping from `no_defs` definitions to slices of decreasing arguments, starting out empty
    #[inline]
    pub fn new(no_defs: usize) -> DecreasingArgsInner {
        Self::with_capacity(no_defs, no_defs)
    }

    /// Construct a mapping from `no_defs` definitions to slices of decreasing arguments, starting out empty, with `capacity` additional capacity
    #[inline]
    pub fn with_capacity(no_defs: usize, capacity: usize) -> DecreasingArgsInner {
        let mut mapping = SmallVec::with_capacity(no_defs.saturating_sub(1) + capacity);
        for _ in 0..no_defs.saturating_sub(1) {
            mapping.push(0)
        }
        DecreasingArgsInner { mapping }
    }

    /// Extend the definition `def` with decreasing argument `arg`.
    ///
    /// Unspecified (but safe) behaviour if there are any arguments in any of the definitions after `def` or if `self.no_defs() <= def`
    #[inline]
    pub fn push(&mut self, def: usize, arg: usize) {
        for ix in (0..def).rev() {
            if self.mapping[ix] != 0 {
                break;
            }
            self.mapping[ix] = self.mapping.len();
        }
        self.mapping.push(arg);
    }

    /// Get the beginning and end of the decreasing arguments for a definition
    ///
    /// Unspecified (but safe) behaviour if `no_defs <= def`, or if `no_defs` is not the current number of definitions in this map.
    #[inline]
    fn bounds(&self, def: usize, no_defs: usize) -> (usize, usize) {
        if self.mapping.is_empty() {
            return (0, 0)
        }
        let begin = if def < no_defs {
            self.mapping[def]
        } else {
            no_defs
        };
        let end = if def < no_defs {
            if self.mapping[def] == 0 {
                self.mapping.len()
            } else {
                self.mapping[def]
            }
        } else {
            self.mapping[def]
        };
        (begin, end)
    }

    /// Get the decreasing arguments for a component, given the maximum component
    ///
    /// Unspecified (but safe) behaviour if `no_defs <= def`, or if `no_defs` is not the current number of definitions in this map.
    #[inline]
    fn get(&self, def: usize, no_defs: usize) -> &[usize] {
        let (begin, end) = self.bounds(def, no_defs);
        &self.mapping[begin..end]
    }

    /// Mutably get the decreasing arguments for a component, given the maximum component
    ///
    /// Unspecified (but safe) behaviour if `no_defs <= def`, or if `no_defs` is not the current number of definitions in this map.
    #[inline]
    fn get_mut(&mut self, def: usize, no_defs: usize) -> &mut [usize] {
        let (begin, end) = self.bounds(def, no_defs);
        &mut self.mapping[begin..end]
    }
}
