/*!
A disjoint-set forest based equality context which may also be used as a consing context
*/
use super::*;
use congruence::{DisjointSetForest, EqMod, Equation, HashMod, Language, UnionFind};
use cons::IndexCtx;

/// A disjoint-set forest based equality context which may also be used as a consing context
// TODO: cache tyck, and fix typing...
#[derive(Clone, Default)]
pub struct ForestCtx {
    /// The underlying hash-table for consing terms
    table: IndexCtx,
    /// A disjoint-set forest representing the current set of equality relations between terms
    dsf: DisjointSetForest,
}

impl ForestCtx {
    /// Get the number of elements consed in this context
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.table.len()
    }

    /// Truncate this context to (at least) the given length
    ///
    /// Will lead to an invalid context if *any* indices above `len` have been unioned with each other!
    #[inline(always)]
    pub fn truncate_unchecked(&mut self, len: usize) {
        self.table.truncate(len);
    }

    /// Truncate this context to (at least) the given length, but *preserving all equality relations*
    #[inline]
    pub fn truncate(&mut self, len: usize) {
        //TODO: optimize, by giving dsf a len method
        self.table.truncate(len.max(self.dsf.capacity()))
    }

    /// Get the index of a term in this context
    #[inline(always)]
    pub fn ix(&self, term: &Term) -> Option<usize> {
        self.table.ix(term)
    }

    /// Get the term corresponding to a given index
    #[inline(always)]
    pub fn id(&self, ix: usize) -> Option<&TermId> {
        self.table.id(ix)
    }

    /// Get a term's index, or insert it
    ///
    /// Return the index along with whether the term is a new insertion
    #[inline(always)]
    pub fn insert(&mut self, term: TermRef<'_>) -> (usize, bool) {
        self.table.insert(term)
    }
}

impl EqCtxMut for ForestCtx {
    type EqCtxMut = Self;

    #[inline(always)]
    fn ignore_irr(&self) -> bool {
        true
    }

    #[inline]
    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        _fail_early: bool,
    ) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            return Ok(Some(true));
        }
        if let (Some(left), Some(right)) = (self.ix(left), self.ix(right)) {
            if self.dsf.find_mut(left) == self.dsf.find_mut(right) {
                return Ok(Some(true));
            }
        }
        //TODO: false cases
        Err(())
    }

    #[inline(always)]
    fn eq_ctx_mut(&mut self) -> &mut Self::EqCtxMut {
        self
    }
}

impl EqCtx for ForestCtx {
    type EqCtx = Self;

    #[inline]
    fn try_eq(&self, left: &Term, right: &Term, _fail_early: bool) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            return Ok(Some(true));
        }
        if let (Some(left), Some(right)) = (self.ix(left), self.ix(right)) {
            if self.dsf.find(left) == self.dsf.find(right) {
                return Ok(Some(true));
            }
        }
        Err(())
    }

    #[inline]
    fn eq_ctx(&self) -> &Self::EqCtx {
        self
    }
}

impl ConsCtx for ForestCtx {
    type ConsCtx = IndexCtx;

    #[inline(always)]
    fn is_consed(&self, term: &TermRef<'_>) -> bool {
        self.table.is_consed(term)
    }

    #[inline(always)]
    fn try_cons(&self, term: &Term) -> Option<&TermId> {
        self.table.try_cons(term)
    }

    #[inline(always)]
    fn shallow_cons(&mut self, id: TermRef<'_>) -> Option<TermId> {
        self.table.shallow_cons(id)
    }

    #[inline(always)]
    fn pointer_exact(&self) -> bool {
        true
    }

    #[inline(always)]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        &mut self.table
    }

    #[inline(always)]
    fn uncons(&self, code: Code) -> Option<TermId> {
        self.table.uncons(code)
    }
}

impl Debug for ForestCtx {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("ForestCtx").finish()
    }
}

impl UnionFind<usize> for ForestCtx {
    #[inline(always)]
    fn union_find(&mut self, left: usize, right: usize) -> usize {
        self.dsf.union_find(left, right)
    }

    #[inline(always)]
    fn pre_union_find(&mut self, left: usize, right: usize) -> usize {
        self.dsf.pre_union_find(left, right)
    }

    #[inline(always)]
    fn union(&mut self, left: usize, right: usize) {
        self.dsf.union(left, right)
    }

    #[inline(always)]
    fn find(&self, node: usize) -> usize {
        self.dsf.find(node)
    }

    #[inline(always)]
    fn node_eq(&self, left: usize, right: usize) -> bool {
        self.dsf.node_eq(left, right)
    }

    #[inline(always)]
    fn find_mut(&mut self, node: usize) -> usize {
        self.dsf.find_mut(node)
    }

    #[inline(always)]
    fn node_eq_mut(&mut self, left: usize, right: usize) -> bool {
        self.dsf.node_eq_mut(left, right)
    }
}

impl Equation<ForestCtx, usize> for TermRef<'_> {
    fn find(ix: usize, ctx: &ForestCtx) -> usize {
        ctx.find(ix)
    }

    fn find_mut(ix: usize, ctx: &mut ForestCtx) -> usize {
        ctx.find_mut(ix)
    }

    fn node_eq(left: usize, right: usize, ctx: &ForestCtx) -> bool {
        ctx.node_eq(left, right)
    }

    fn node_eq_mut(left: usize, right: usize, ctx: &mut ForestCtx) -> bool {
        ctx.node_eq_mut(left, right)
    }

    fn ix(&self, ctx: &ForestCtx) -> Option<usize> {
        ctx.ix(self)
    }

    fn ix_mut(&self, ctx: &mut ForestCtx) -> Option<usize> {
        ctx.ix(self)
    }

    fn merge_ix(
        left: usize,
        right: usize,
        ctx: &mut ForestCtx,
        _pending: &mut congruence::Pending<usize>,
    ) -> (usize, bool) {
        let left_repr = ctx.find_mut(left);
        let right_repr = ctx.find_mut(right);
        let left_flags = ctx.dsf.flags(left_repr);
        let right_flags = ctx.dsf.flags(right_repr);
        let repr = ctx.union_find(left_repr, right_repr);
        let changed_flags = if repr == left_repr {
            right_flags & !left_flags
        } else {
            left_flags & !right_flags
        };
        (repr, Flags::EQ_FLAGS.bits() & changed_flags != 0)
    }

    fn merge_pending(
        &mut self,
        other: Self,
        ctx: &mut ForestCtx,
        _pending: &mut congruence::Pending<usize>,
    ) -> Option<(usize, usize)> {
        Some((self.ix_mut(ctx)?, other.ix_mut(ctx)?))
    }
}

impl Language<ForestCtx, usize> for TermRef<'_> {
    #[inline(always)]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&ForestCtx, usize) -> Result<(), E>,
        ctx: &ForestCtx,
    ) -> Result<(), E> {
        (**self).visit_deps(visitor, ctx)
    }

    #[inline(always)]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut ForestCtx, usize) -> Result<(), E>,
        ctx: &mut ForestCtx,
    ) -> Result<(), E> {
        (**self).visit_deps_mut(visitor, ctx)
    }
}

impl EqMod<ForestCtx, usize> for TermRef<'_> {
    #[inline(always)]
    fn eq_mod(
        &self,
        other: &Self,
        map: &mut impl FnMut(&ForestCtx, usize, usize) -> bool,
        ctx: &ForestCtx,
    ) -> bool {
        (**self).eq_mod(other, map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &Self,
        map: &mut impl FnMut(&mut ForestCtx, usize, usize) -> bool,
        ctx: &mut ForestCtx,
    ) -> bool {
        (**self).eq_mod_mut(other, map, ctx)
    }
}

impl EqMod<ForestCtx, usize, Term> for TermRef<'_> {
    #[inline(always)]
    fn eq_mod(
        &self,
        other: &Term,
        map: &mut impl FnMut(&ForestCtx, usize, usize) -> bool,
        ctx: &ForestCtx,
    ) -> bool {
        (**self).eq_mod(other, map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &Term,
        map: &mut impl FnMut(&mut ForestCtx, usize, usize) -> bool,
        ctx: &mut ForestCtx,
    ) -> bool {
        (**self).eq_mod_mut(other, map, ctx)
    }
}

impl EqMod<ForestCtx, usize, TermRef<'_>> for Term {
    #[inline(always)]
    fn eq_mod(
        &self,
        other: &TermRef,
        map: &mut impl FnMut(&ForestCtx, usize, usize) -> bool,
        ctx: &ForestCtx,
    ) -> bool {
        self.eq_mod(&**other, map, ctx)
    }

    #[inline(always)]
    fn eq_mod_mut(
        &self,
        other: &TermRef,
        map: &mut impl FnMut(&mut ForestCtx, usize, usize) -> bool,
        ctx: &mut ForestCtx,
    ) -> bool {
        self.eq_mod_mut(&**other, map, ctx)
    }
}

impl HashMod<ForestCtx, usize> for TermRef<'_> {
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &ForestCtx, usize),
        ctx: &ForestCtx,
    ) {
        (**self).hash_mod(hasher, hash_mod, ctx)
    }

    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &mut ForestCtx, usize),
        ctx: &mut ForestCtx,
    ) {
        (**self).hash_mod_mut(hasher, hash_mod, ctx)
    }
}

impl Language<ForestCtx, usize> for Term {
    #[inline]
    fn visit_deps<E>(
        &self,
        visitor: &mut impl FnMut(&ForestCtx, usize) -> Result<(), E>,
        ctx: &ForestCtx,
    ) -> Result<(), E> {
        self.visit_direct_deps(0, |_is_ty, _shift, dep| {
            if let Some(ix) = ctx.ix(&*dep) {
                visitor(ctx, ix)?
            }
            Ok(())
        })
    }

    #[inline]
    fn visit_deps_mut<E>(
        &self,
        visitor: &mut impl FnMut(&mut ForestCtx, usize) -> Result<(), E>,
        ctx: &mut ForestCtx,
    ) -> Result<(), E> {
        self.visit_direct_deps(0, |_is_ty, _shift, dep| {
            if let Some(ix) = ctx.ix(&*dep) {
                visitor(ctx, ix)?
            }
            Ok(())
        })
    }
}

impl EqMod<ForestCtx, usize> for Term {
    fn eq_mod(
        &self,
        other: &Self,
        map: &mut impl FnMut(&ForestCtx, usize, usize) -> bool,
        ctx: &ForestCtx,
    ) -> bool {
        self.term_eq_mut(
            other,
            true,
            &mut EqMod(
                |left: &Term, right: &Term| {
                    if left as *const _ == right as *const _ || left == right {
                        return Some(true);
                    }
                    match (ctx.ix(left), ctx.ix(right)) {
                        (Some(left), Some(right)) => Some(map(ctx, left, right)),
                        _ => None,
                    }
                },
                true,
            ),
        )
        .unwrap_or(false)
    }

    fn eq_mod_mut(
        &self,
        other: &Self,
        map: &mut impl FnMut(&mut ForestCtx, usize, usize) -> bool,
        ctx: &mut ForestCtx,
    ) -> bool {
        self.term_eq_mut(
            other,
            true,
            &mut EqMod(
                |left: &Term, right: &Term| {
                    if left as *const _ == right as *const _ || left == right {
                        return Some(true);
                    }
                    match (ctx.ix(left), ctx.ix(right)) {
                        (Some(left), Some(right)) => Some(map(ctx, left, right)),
                        _ => None,
                    }
                },
                true,
            ),
        )
        .unwrap_or(false)
    }
}

impl HashMod<ForestCtx, usize> for Term {
    fn hash_mod<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &ForestCtx, usize),
        ctx: &ForestCtx,
    ) {
        self.hash_deps(hasher, &mut |hasher, dep| {
            if let Some(ix) = ctx.ix(dep) {
                hash_mod(hasher, ctx, ix)
            } else {
                //TODO: generalize for deep match?
                dep.hash(hasher)
            }
        })
    }

    fn hash_mod_mut<H: Hasher>(
        &self,
        hasher: &mut H,
        hash_mod: &mut impl FnMut(&mut H, &mut ForestCtx, usize),
        ctx: &mut ForestCtx,
    ) {
        self.hash_deps(hasher, &mut |hasher, dep| {
            if let Some(ix) = ctx.ix(dep) {
                hash_mod(hasher, ctx, ix)
            } else {
                //TODO: generalize for deep match?
                dep.hash(hasher)
            }
        })
    }
}
