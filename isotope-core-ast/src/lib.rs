/*!
An abstract syntax tree for the core `isotope` language
*/
#![forbid(missing_debug_implementations, missing_docs, unsafe_code)]
use pretty::{DocAllocator, DocBuilder};
use std::borrow::Cow;

pub use num_bigint::BigInt;
pub use smallvec::SmallVec;
pub use smol_str::SmolStr;

/// The `Arc` implementation used by this library
pub type Arc<T> = elysees::Arc<T>;

/// An `isotope-core` expression
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Expr {
    // === Basic language ===
    /// An identifier or hole
    Ident(Option<SmolStr>),
    /// A lambda abstraction
    Lambda(Lambda),
    /// A function application
    App(App),
    /// A dependent function type
    Pi(Pi),
    /// A leveled typing universe
    Universe(Universe),
    /// A case expression
    Case(Case),
    /// An annotated expression
    Annotated(Annotated),

    // === Optimized concepts ===
    /// An integer literal
    Int(BigInt),

    // === Syntax sugar ===
    /// A scope, which may also be a block of mutually recursive definitions
    Scope(Scope),
}

impl Expr {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        match self {
            Expr::Ident(Some(id)) => allocator.text(Cow::Owned(id.to_string())),
            Expr::Ident(None) => allocator.text("_"),
            Expr::Lambda(this) => this.pretty(atom, allocator),
            Expr::App(this) => this.pretty(atom, allocator),
            Expr::Pi(this) => this.pretty(atom, allocator),
            Expr::Universe(this) => this.pretty(atom, allocator),
            Expr::Case(this) => this.pretty(atom, allocator),
            Expr::Annotated(this) => this.pretty(atom, allocator),
            Expr::Int(this) => allocator.text(Cow::Owned(this.to_string())),
            Expr::Scope(this) => this.pretty(atom, allocator),
        }
    }
}

/// An `isotope-core` statement
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Stmt {
    /// A `let` statement
    Let(Let),
    /// An inductive definition
    Inductive(Inductive),
    /// A recursive definition
    Recursive(Recursive),
}

impl Stmt {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        match self {
            Stmt::Let(this) => this.pretty(allocator),
            Stmt::Inductive(this) => this.pretty(allocator),
            Stmt::Recursive(this) => this.pretty(allocator),
        }
    }
}

/// A lambda abstraction
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Lambda {
    /// The variable name being parametrized, or `None` if the result is constant.
    pub param_name: Option<SmolStr>,
    /// The parameter type of this value, if any
    pub param_ty: Option<Arc<Expr>>,
    /// The result value
    pub result: Arc<Expr>,
}

impl Lambda {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        if atom {
            allocator.text("(").append(allocator.line())
        } else {
            allocator.nil()
        }
        .append(allocator.text("λ").append(allocator.space()))
        .append(
            allocator
                .text(
                    self.param_name
                        .as_ref()
                        .map(ToString::to_string)
                        .map(Cow::Owned)
                        .unwrap_or(Cow::Borrowed("_")),
                )
                .append(if let Some(param_ty) = &self.param_ty {
                    allocator
                        .space()
                        .append(allocator.text(":"))
                        .append(allocator.space())
                        .append(param_ty.pretty(false, allocator))
                } else {
                    allocator.nil()
                })
                .group()
                .append(allocator.space()),
        )
        .append(allocator.text("=>"))
        .append(allocator.line())
        .append(self.result.pretty(false, allocator))
        .append(if atom {
            allocator.line().append(allocator.text(")"))
        } else {
            allocator.nil()
        })
        .group()
    }
}

/// An n-ary function application
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct App(pub SmallVec<[Arc<Expr>; 5]>);

impl App {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        if self.0.is_empty() {
            allocator.text("()")
        } else {
            if atom {
                allocator.text("(").append(allocator.line())
            } else {
                allocator.nil()
            }
            .append(allocator.intersperse(
                self.0.iter().map(|expr| expr.pretty(true, allocator)),
                allocator.line(),
            ))
            .append(if atom {
                allocator.line().append(")")
            } else {
                allocator.nil()
            })
            .group()
        }
    }
}

/// A dependent function type
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Pi {
    /// The variable name being parametrized, or `None` if the result is constant.
    pub param_name: Option<SmolStr>,
    /// The parameter type of this value, if any
    pub param_ty: Arc<Expr>,
    /// The result value
    pub result: Arc<Expr>,
}

impl Pi {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        if atom {
            allocator.text("(").append(allocator.line())
        } else {
            allocator.nil()
        }
        .append(
            if self.param_name.is_some() || matches!(&*self.param_ty, Expr::Annotated(_)) {
                allocator
                    .text("(")
                    .append(allocator.space())
                    .append(
                        allocator.text(
                            self.param_name
                                .as_ref()
                                .map(ToString::to_string)
                                .map(Cow::Owned)
                                .unwrap_or(Cow::Borrowed("_")),
                        ),
                    )
                    .append(allocator.space())
                    .append(":")
                    .append(self.param_ty.pretty(false, allocator))
                    .append(allocator.space())
                    .append(")")
            } else {
                self.param_ty.pretty(false, allocator)
            },
        )
        .append(allocator.line())
        .append("->")
        .append(allocator.line())
        .append(self.result.pretty(false, allocator))
        .append(if atom {
            allocator.line().append(allocator.text(")"))
        } else {
            allocator.nil()
        })
        .group()
    }
}

/// A typing universe
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Universe(pub u64);

impl Universe {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, _atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator.text(format!("#U{}", self.0))
    }
}

/// A case expression
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Case {
    /// The inductive family being matched over
    pub family: Arc<Expr>,
    /// The type family being targeted
    pub target: Arc<Expr>,
}

impl Case {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        if atom {
            allocator.text("(").append(allocator.line())
        } else {
            allocator.nil()
        }
        .append(allocator.text("#case"))
        .append(allocator.line())
        .append(self.family.pretty(true, allocator))
        .append(allocator.line())
        .append(self.target.pretty(true, allocator))
        .append(if atom {
            allocator.line().append(allocator.text(")"))
        } else {
            allocator.nil()
        })
        .group()
    }
}

/// An annotated expression
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Annotated {
    /// The expression being annotated
    pub expr: Arc<Expr>,
    /// The type it is being annotated with
    pub ty: Arc<Expr>,
}

impl Annotated {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, _atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("(")
            .append(allocator.line())
            .append(self.expr.pretty(false, allocator))
            .append(allocator.space())
            .append(allocator.text(":"))
            .append(allocator.line())
            .append(self.ty.pretty(false, allocator))
            .append(allocator.line())
            .append(allocator.text(")"))
            .group()
    }
}

/// A scope
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Scope {
    /// The statements in this scope
    pub stmts: SmallVec<[Arc<Stmt>; 4]>,
    /// This scope's return value
    pub retv: Arc<Expr>,
}

impl Scope {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, _atom: bool, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("{")
            .append(allocator.line())
            .append(allocator.intersperse(
                self.stmts.iter().map(|stmt| stmt.pretty(allocator)),
                allocator.line(),
            ))
            .append(allocator.line())
            .append(self.retv.pretty(false, allocator))
            .append(allocator.line())
            .append(allocator.text("}"))
            .group()
    }
}

/// A block of mutually recursive definitions
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Recursive {
    /// The definitions in this block
    pub defs: SmallVec<[Let; 1]>,
}

impl Recursive {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("#rec {")
            .append(allocator.line())
            .append(allocator.intersperse(
                self.defs.iter().map(|stmt| stmt.pretty(allocator)),
                allocator.line(),
            ))
            .append(allocator.line())
            .group()
    }
}

/// A `let` statement
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Let {
    /// The identifier being bound, or `None` if only type-checking is to be performed
    pub ident: Option<SmolStr>,
    /// The type the identifier is being assigned, if any
    pub ty: Option<Arc<Expr>>,
    /// The value being bound to the identifier
    pub value: Arc<Expr>,
}

impl Let {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator.text("#let").append(allocator.line()).append(
            allocator
                .text(
                    self.ident
                        .as_ref()
                        .map(ToString::to_string)
                        .map(Cow::Owned)
                        .unwrap_or(Cow::Borrowed("_")),
                )
                .append(if let Some(ty) = &self.ty {
                    allocator
                        .space()
                        .append(ty.pretty(false, allocator))
                        .append(allocator.space())
                } else {
                    allocator.space()
                })
                .append(allocator.text("="))
                .append(allocator.line())
                .append(self.value.pretty(false, allocator))
                .append(";")
                .group(),
        )
    }
}

/// An inductive definition
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Inductive {
    /// The set of inductive families being defined
    pub families: SmallVec<[InductiveFamily; 1]>,
}

impl Inductive {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text("#data")
            .append(allocator.line())
            .append(allocator.intersperse(
                self.families.iter().map(|family| family.pretty(allocator)),
                allocator.line(),
            ))
            .append(allocator.text(";"))
    }
}

/// An inductive family definition
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct InductiveFamily {
    /// The name of this inductive family
    pub name: SmolStr,
    /// The type of this inductive family, if given
    pub ty: Option<Arc<Expr>>,
    /// The constructor types of this inductive family
    pub cons: Vec<(SmolStr, Arc<Expr>)>,
}

impl InductiveFamily {
    /// Return a pretty printed format of self
    pub fn pretty<'b, D, A>(&self, allocator: &'b D) -> DocBuilder<'b, D, A>
    where
        D: DocAllocator<'b, A>,
        D::Doc: Clone,
        A: Clone,
    {
        allocator
            .text(self.name.to_string())
            .append(allocator.space())
            .append(allocator.text("{"))
            .append(allocator.line())
            .append(allocator.intersperse(
                self.cons.iter().map(|(name, ty)| {
                    allocator
                        .text(name.to_string())
                        .append(ty.pretty(false, allocator))
                }),
                allocator.line(),
            ))
            .append(allocator.line())
            .append(allocator.text("}"))
            .group()
    }
}
