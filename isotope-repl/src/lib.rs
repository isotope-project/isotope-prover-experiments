use isotope_core::eq::CongruenceCtx;
use isotope_core::eq::EqCtxMut;
use isotope_core::subst::SubstStack;
use isotope_core::syntax::Builder;
use isotope_core::syntax::Error as SyntaxError;
use isotope_core::syntax::{LevelResolver, SimpleNameBuilder, ToAST};
use isotope_core::util::sparse_stack::SparseStack;
use isotope_core::{TermId, Value};
use isotope_core_parser::ast::{Expr, Stmt};
use isotope_core_parser::utils::ws;
use isotope_core_parser::{expr, stmt, Arc, SmallVec, SmolStr};
use nom::branch::*;
use nom::bytes::complete::*;
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::IResult;
use rustyline::error::ReadlineError;
use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::validate::{ValidationContext, ValidationResult, Validator};
use rustyline::Context;
use rustyline_derive::{Completer, Helper, Highlighter};
use std::fmt::{self, Debug, Formatter};
use thiserror::Error;
use isotope_core::error::Error as CoreError;

mod command;
mod helper;
pub use command::*;
pub use helper::*;

/// An `isotope` repl
pub struct Repl {
    /// The number of commands handled
    handled: usize,
    /// The builder for this repl
    builder: Builder,
    /// The prettyprinter for this repl
    pretty: SimpleNameBuilder<LevelResolver>,
    /// The congruence context for this repl
    ctx: CongruenceCtx,
    /// The shared substitution stack for this repl
    subst_stack: SparseStack<Option<TermId>>,
    /// The shared application buffer for this repl
    app_buf: Vec<TermId>,
}

impl Debug for Repl {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("Repl")
            .field("handled", &self.handled)
            .finish()
    }
}

impl Repl {
    /// Construct a new repl with the given configuration
    pub fn new(_cfg: ReplConfig) -> Repl {
        Repl {
            handled: 0,
            builder: Builder::default(),
            pretty: SimpleNameBuilder::default(),
            ctx: CongruenceCtx::default(),
            subst_stack: SparseStack::default(),
            app_buf: Vec::default(),
        }
    }

    /// Handle a command
    pub fn handle(&mut self, command: &Command) -> Result<(), ReplError> {
        match command {
            Command::Stmt(s) => self.handle_stmt(s)?,
            Command::Expr(e) => self.handle_expr(e)?,
            Command::Code(e) => println!("{:?}", self.parse_expr(e)?.code()),
            Command::Addr(e) => println!("{:?}", self.parse_expr(e)?.as_ptr()),
            Command::Base(e) => {
                if let Some(ty) = self.parse_expr(e)?.base_ty() {
                    println!("{:?}", ty)
                } else {
                    println!("#untyped")
                }
            }
            Command::Check(e) => {
                let e = self.parse_expr(e)?;
                match e.tyck_mut(false, self.ctx.ty_ctx_mut()) {
                Some(true) => {
                    if let Some(ty) = e.ty() {
                        let arena = pretty::Arena::<String>::new();
                        let pretty = ty
                            .to_ast(&mut self.pretty)
                            .map_err(|_| ReplError::PrettyError)?
                            .pretty(false, &arena);
                        let mut buf = Vec::new();
                        pretty.render(80, &mut buf).unwrap();
                        println!("{}", String::from_utf8_lossy(&buf[..]));
                    } else {
                        println!("#untyped")
                    }
                }
                other => return Err(ReplError::TypeError(other == Some(false))),
            }
        },
            Command::Join((l, r)) => {
                let l = self.parse_expr(l)?;
                let r = self.parse_expr(r)?;
                let tyck = isotope_core::opt_and!(true; 
                    l.tyck_mut(true, self.ctx.ty_ctx_mut()), 
                    r.tyck_mut(true, self.ctx.ty_ctx_mut()));
                if tyck != Some(true) {
                    return Err(ReplError::TypeError(tyck == Some(false)))
                }

                let ln = self.norm_in(l)?;
                let rn = self.norm_in(r)?;

                if ln != rn {
                    return Err(ReplError::TermMismatch)
                }
                let li = self.ctx.insert(ln);
                let ri = self.ctx.insert(rn);
                self.ctx.merge(li.0, ri.0);
            }
        }
        self.handled += 1;
        Ok(())
    }

    /// Handle an expression
    ///
    /// Does not increment the handled count
    pub fn handle_expr(&mut self, expr: &Expr) -> Result<(), ReplError> {
        //TODO: normalize
        let expr = self
            .builder
            .expr(expr, None, &mut self.ctx)
            .map_err(ReplError::SyntaxError)?;

        let norm = self.norm_in(expr)?;

        let ast = norm
            .to_ast(&mut self.pretty)
            .map_err(|_| ReplError::PrettyError)?;
        let arena = pretty::Arena::<String>::new();
        let pretty = ast.pretty(false, &arena);
        let mut buf = Vec::new();
        pretty.render(80, &mut buf).unwrap();
        println!("{}", String::from_utf8_lossy(&buf[..]));
        Ok(())
    }

    /// Handle a statement
    ///
    /// Does not increment the handled count
    pub fn handle_stmt(&mut self, stmt: &Stmt) -> Result<(), ReplError> {
        if matches!(stmt, Stmt::Inductive(_) | Stmt::Recursive(_)) {
            let mut defs: SmallVec<[(SmolStr, TermId); 6]> = SmallVec::new();
            self.builder
                .stmt(
                    stmt,
                    |ident, def| defs.push((ident, def.into_owned())),
                    &mut self.ctx,
                )
                .map_err(ReplError::SyntaxError)?;
            for (ident, def) in defs {
                //TODO: improve this... a lot... metadata?
                self.pretty
                    .closed
                    .insert(def, Arc::new(Expr::Ident(Some(ident))));
            }
        } else {
            self.builder.stmt(stmt, |_, _| (), &mut self.ctx)?;
        }
        Ok(())
    }

    /// Parse an expression
    ///
    /// Does not increment the handled count
    pub fn parse_expr(&mut self, expr: &Expr) -> Result<TermId, ReplError> {
        Ok(self.builder.expr(expr, None, &mut self.ctx)?)
    }

    /// Normalize an expression
    pub fn norm_in(&mut self, expr: TermId) -> Result<TermId, ReplError> {
        self.app_buf.clear();
        let mut stack = SubstStack {
            stack: self.cached_stack(),
            cons: &mut self.ctx,
        };
        let ln = expr
                    .norm_applied_cons_in(&mut self.app_buf, &mut 100000, &mut stack)
                    .map_err(ReplError::NormError);
        if ln.is_ok() {
            debug_assert!(self.app_buf.is_empty());
            debug_assert!(stack.stack.is_empty());
        }
        self.app_buf.clear();
        let mut stack = stack.stack;
        self.cache_stack(&mut stack);
        ln
    }

    /// Get a cached substitution stack
    pub fn cached_stack(&mut self) -> SparseStack<Option<TermId>> {
        debug_assert!(self.subst_stack.is_empty());
        let mut stack = SparseStack::default();
        std::mem::swap(&mut self.subst_stack, &mut stack);
        stack
    }

    /// Cache a substitution stack 
    pub fn cache_stack(&mut self, stack: &mut SparseStack<Option<TermId>>) {
        debug_assert!(self.subst_stack.is_empty());
        stack.clear();
        if self.subst_stack.capacity() < stack.capacity() {
            std::mem::swap(&mut self.subst_stack, stack);
        }
    }
}

/// Configuration for constructing an `isotope` repl
#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct ReplConfig {}

/// An `isotope` repl error
#[derive(Debug, Clone, Error)]
pub enum ReplError {
    /// An error prettyprinting an `isotope` value
    #[error("Error prettyprinting value")]
    PrettyError,
    /// Syntax error
    #[error("Error building value: {0}")]
    SyntaxError(SyntaxError),
    /// An `isotope` value did not typecheck
    #[error("Type error (sure = {0})")]
    TypeError(bool),
    /// Terms did not normalize to the same value
    #[error("Terms did not normalize to the same value")]
    TermMismatch,
    /// A normalization error
    #[error("Normalization error: {0}")]
    NormError(CoreError),
    /// Not implemented
    #[error("Not implemented")]
    NotImplemented,
}

impl From<SyntaxError> for ReplError {
    fn from(err: SyntaxError) -> ReplError {
        ReplError::SyntaxError(err)
    }
}
