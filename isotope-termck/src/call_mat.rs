/*!
Call matrices and associated utilities
*/

use std::ops::Mul;

use sprs::CsMat;

use crate::Relation;

/// A call matrix
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct CallMat(CsMat<Relation>);

impl CallMat {
    /// Create an empty call matrix with a given shape
    #[inline]
    pub fn unknown(callee_args: usize, caller_args: usize) -> CallMat {
        CallMat(CsMat::zero((callee_args, caller_args)))
    }

    /// Check whether a call matrix is empty
    #[inline]
    pub fn is_unknown(&self) -> bool {
        self.0.data().iter().all(|rel| *rel == Relation::Unknown)
    }

    /// Create a call matrix from a *sorted* iterator of callee-argument, caller-argument, relation triples, summing duplicates
    #[inline]
    pub fn new_sorted(
        callee_args: usize,
        caller_args: usize,
        iter: impl IntoIterator<Item = (usize, usize, Relation)>,
    ) -> CallMat {
        let mut result = CallMat::unknown(callee_args, caller_args);
        for (callee_arg, caller_arg, rel) in iter {
            result.insert(callee_arg, caller_arg, rel)
        }
        result
    }

    /// Insert a relation into a call matrix
    ///
    /// Note that this is only fast if relations are inserted in row-major order
    #[inline]
    pub fn insert(&mut self, callee_arg: usize, caller_arg: usize, rel: Relation) {
        if rel != Relation::Unknown {
            self.0.insert(callee_arg, caller_arg, rel)
        }
    }

    /// Get the shape of this call matrix
    #[inline]
    pub fn shape(&self) -> (usize, usize) {
        self.0.shape()
    }

    /// Iterate over the diagonal of this call matrix
    #[inline]
    pub fn diag_iter(
        &self,
    ) -> impl Iterator<Item = Relation> + ExactSizeIterator + DoubleEndedIterator + '_ {
        self.0
            .diag_iter()
            .map(|rel| rel.copied().unwrap_or_default())
    }
}

impl Mul<&'_ CallMat> for &'_ CallMat {
    type Output = CallMat;

    #[inline]
    fn mul(self, rhs: &'_ CallMat) -> Self::Output {
        CallMat(&self.0 * &rhs.0)
    }
}
