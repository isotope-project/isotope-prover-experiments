/*!
`case` statements for inductive types
*/
use super::*;

/// A `case` statement
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Case {
    /// This case statement's code
    code: Code,
    /// This case statement's free variable set
    fv: Filter,
    /// The surface type of this `case` statement
    ty: TermId,
    /// The base type of this `case` statement
    base_ty: TermId,
    /// The inductive family being matched
    family: TermId,
    /// The type family being targeted
    target: TermId,
    /// The flags for this `case` statement
    flags: Flags,
}

impl Debug for Case {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut dbg = f.debug_struct("Case");
        dbg.field("family", &self.family)
            .field("target", &self.target);
        if self.ty != self.base_ty {
            dbg.field("ty", &self.ty);
        }
        dbg.finish()
    }
}

impl Case {
    // === Constructors ===

    /// Attempt to construct a new `case` statement given a type family and optional surface type
    #[inline]
    pub fn try_new(
        family: TermId,
        target: TermId,
        ty: Option<TermId>,
        ctx: &mut impl ConsCtx,
    ) -> Result<Case, Error> {
        let base_ty = Self::compute_ty(&family, &target, ctx)?;
        let ty = ty.unwrap_or_else(|| base_ty.clone());
        let fv = Self::compute_fv(&family, &target, &ty);
        let flags = Self::compute_flags(&family, &target, &base_ty, &ty, fv);
        let code = Self::compute_code(&family, &target, &ty);
        Ok(Case {
            code,
            fv,
            ty,
            base_ty: base_ty.into_id_in(ctx),
            family,
            target,
            flags,
        })
    }

    // === Getters ===

    /// Get the type this case statement matches over
    #[inline]
    pub fn family(&self) -> &TermId {
        &self.family
    }

    /// Get the target of this case statement
    #[inline]
    pub fn target(&self) -> &TermId {
        &self.target
    }

    /// Get the base type of this case statement
    #[inline]
    pub fn get_base_ty(&self) -> &TermId {
        &self.base_ty
    }

    /// Get the type of this case statement
    #[inline]
    pub fn get_ty(&self) -> &TermId {
        &self.ty
    }

    // === Property computations ===

    /// Compute the base type of a `case` statement
    pub fn compute_ty(
        family: &TermId,
        target: &TermId,
        ctx: &mut impl ConsCtx,
    ) -> Result<TermId, Error> {
        let mut result = Some(family.case_result_ty(target, ctx)?);
        for n in (0..family.as_ind_fam()?.no_cons()?).rev() {
            let curr = result.take().unwrap();
            result = Some(
                family
                    .branch_ty(n as VarIx, target, ctx)?
                    .simple_in(curr, ctx)?,
            )
        }
        Ok(result.unwrap())
    }

    /// Compute the free variable set of a `case` statement
    pub fn compute_fv(family: &TermId, target: &TermId, ty: &TermId) -> Filter {
        family.fv().union(target.fv()).union(ty.fv())
    }

    /// Compute the code of a `case` statement
    pub fn compute_code(family: &TermId, target: &TermId, ty: &TermId) -> Code {
        let mut state = AHasher::new_with_keys(5986837856, 43452);
        family.code().hash(&mut state);
        target.code().hash(&mut state);
        ty.hash(&mut state);
        Code(state.finish())
    }

    /// Compute the flags of a `case` statement
    pub fn compute_flags(
        family: &TermId,
        target: &TermId,
        base_ty: &TermId,
        ty: &TermId,
        fv: Filter,
    ) -> Flags {
        let mut flags = Flags::default();
        let is_irr = ty.is_prop() || base_ty.is_prop();
        flags.set(Flags::IS_IRR, is_irr);
        flags.set(Flags::IS_CLOSED, fv.is_empty());
        flags.set(
            Flags::TYCK_NIL,
            family.nil_tyck() && target.nil_tyck() && ty.nil_tyck() && base_ty == ty,
        );
        flags
    }
}

impl Equiv<Case> for Case {
    fn term_eq_mut(&self, other: &Case, fail_early: bool, ctx: &mut impl EqCtxMut) -> Option<bool> {
        //NOTE: base_ty is implied
        opt_and! {
            fail_early;
            self.ty.term_eq_mut(&other.ty, fail_early, ctx),
            self.family.term_eq_mut(&other.family, fail_early, ctx)
        }
    }
}

impl Cons<Case> for Case {
    fn cons(&self, ctx: &mut impl ConsCtx) -> Option<Case> {
        do_cons! {
            in ctx;
            let ty = self.ty;
            let base_ty = self.base_ty;
            let family = self.family;
            let target = self.target;
            Some(Case { ty, base_ty, family, target, ..*self })
        }
    }

    #[inline]
    fn consed(&self, ctx: &mut impl ConsCtx) -> Case {
        if let Some(cons) = self.cons(ctx) {
            cons
        } else {
            self.clone()
        }
    }

    #[inline]
    fn shallow_consed(&self, _ctx: &mut impl ConsCtx) -> Case {
        self.clone()
    }
}

impl Cast<Case> for Case {
    fn cast(&self, ty: TermRef, ctx: &mut impl ConsCtx) -> Result<Option<Case>, Error> {
        if self.ty == ty {
            Ok(None)
        } else {
            //TODO: avoid recomputing base type
            Self::try_new(
                self.family.consed(ctx),
                self.target.consed(ctx),
                Some(ty.into_owned()),
                ctx,
            )
            .map(Some)
        }
    }
}

impl Subst<Case> for Case {
    fn subst(&self, ctx: &mut impl SubstCtx) -> Result<Option<Case>, Error> {
        do_subst!(
            in ctx;
            let ty = self.ty;
            let family = self.family;
            let target = self.target;
            Self::try_new(family, target, Some(ty), ctx.cons_ctx()).map(Some)
        )
    }
}

eq_term!(Case);
value_impl!(Case);

impl Value for Case {
    #[inline]
    fn into_term(self) -> Term {
        Term::Case(self)
    }

    #[inline]
    fn ty(&self) -> Option<TermRef> {
        Some(self.get_ty().borrow_id())
    }

    #[inline]
    fn base_ty(&self) -> Option<TermRef> {
        Some(self.base_ty.borrow_id())
    }

    #[inline]
    fn tyck_mut(&self, fail_early: bool, ctx: &mut impl TyCtxMut) -> Option<bool> {
        opt_and!(
            fail_early;
            ctx.constrain_mut(&self.ty, &self.base_ty, fail_early),
            self.ty.tyck_mut(fail_early, ctx),
            self.family.tyck_mut(fail_early, ctx),
            self.target.tyck_mut(fail_early, ctx)
        )
    }

    #[inline]
    fn app_root(&self) -> Option<&TermId> {
        None
    }

    #[inline]
    fn apply(
        &self,
        args: &mut impl ArgVec,
        ctx: &mut impl EvalCtx,
    ) -> Result<Option<TermId>, Error> {
        let matched = self.family().as_ind_fam()?;
        let matched_no_cons = matched.no_cons()? as usize;
        if args.arg_len() <= matched_no_cons {
            return self.subst(ctx);
        }
        let cons = if let Ok(cons) = args
            .arg(matched_no_cons)
            .ok_or(Error::Invariant)?
            .get_base_root()
        {
            //TODO: better compatibility check?
            match cons.as_ind_cons() {
                Ok(cons)
                    if cons.fam_ix() == matched.ix() && (cons.ix() as usize) < matched_no_cons =>
                {
                    cons
                }
                Ok(_) => return Err(Error::IndCaseMismatch),
                Err(_) => return self.subst(ctx),
            }
        } else {
            return self.subst(ctx);
        };
        let ix = cons.ix();
        let mut pattern = None;
        for i in 0..matched_no_cons {
            let arg = args.pop_arg()?.ok_or(Error::Invariant)?;
            if i == ix as usize {
                pattern = Some(arg)
            }
        }
        let pattern = pattern.unwrap();
        let match_ = args.pop_arg()?.ok_or(Error::Invariant)?;
        match_.push_root_args(args)?;
        Ok(Some(pattern))
    }

    #[inline]
    fn flags(&self) -> Flags {
        self.flags
    }

    #[inline]
    fn is_subty(&self, _other: &Term) -> bool {
        false
    }

    #[inline]
    fn is_unique_head(&self) -> bool {
        //TODO: this...
        true
    }

    #[inline]
    fn var_within(&self, equiv: VarIx, min: VarIx, max: VarIx) -> bool {
        if let Some(result) = self.fv.contains_within(equiv, min, max) {
            result
        } else {
            self.ty.var_within(equiv, min, max) || self.family.var_within(equiv, min, max)
        }
    }

    #[inline]
    fn fv(&self) -> Filter {
        self.fv
    }

    #[inline]
    fn code(&self) -> Code {
        self.code
    }

    #[inline]
    fn visit_transitive_deps<E>(
        &self,
        shift: VarIx,
        visitor: &mut impl FnMut(bool, VarIx, TermRef) -> Result<bool, E>,
    ) -> Result<(), E> {
        visitor(true, shift, self.ty.borrow_id())?;
        self.family.visit_transitive_deps_rec(shift, visitor)?;
        self.target.visit_transitive_deps_rec(shift, visitor)
    }

    #[inline]
    fn cmp_term(&self, other: &Self) -> Ordering {
        self.code
            .cmp(&other.code)
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| self.target.cmp(&other.target))
            .then_with(|| self.family.cmp(&other.family))
    }

    #[inline]
    fn hash_deps<H: Hasher>(&self, hasher: &mut H, trans: &mut impl FnMut(&mut H, &TermRef)) {
        trans(hasher, &self.ty);
        trans(hasher, &self.target);
        trans(hasher, &self.family)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    //TODO: test projections
    
    #[test]
    fn logical_operations() {
        let mut ctx = SubstStack::new(HashCtx::default());
        let b = TermId::bools_in(ctx.cons_ctx());
        let bs = [
            b.con_in(0, ctx.cons_ctx()).unwrap(),
            b.con_in(1, ctx.cons_ctx()).unwrap(),
        ];
        let ib = b.id_fn_in(ctx.cons_ctx()).unwrap();
        let af = bs[0].const_over_in(b.clone(), ctx.cons_ctx()).unwrap();
        let at = bs[1].const_over_in(b.clone(), ctx.cons_ctx()).unwrap();
        let case_b = b
            .case_in(
                b.const_over_in(b.clone(), ctx.cons_ctx()).unwrap(),
                ctx.cons_ctx(),
            )
            .unwrap();
        let not = case_b
            .app_in(bs[1].clone(), ctx.cons_ctx())
            .unwrap()
            .app_in(bs[0].clone(), ctx.cons_ctx())
            .unwrap();
        for x in [false, true] {
            assert_eq!(
                ib.norm_applied_cons_in(&mut vec![bs[x as usize].clone()], &mut 10, &mut ctx)
                    .unwrap(),
                bs[x as usize]
            );
            assert_eq!(
                af.norm_applied_cons_in(&mut vec![bs[x as usize].clone()], &mut 10, &mut ctx)
                    .unwrap(),
                bs[0]
            );
            assert_eq!(
                at.norm_applied_cons_in(&mut vec![bs[x as usize].clone()], &mut 10, &mut ctx)
                    .unwrap(),
                bs[1]
            );
            assert_eq!(
                not.norm_applied_cons_in(&mut vec![bs[x as usize].clone()], &mut 10, &mut ctx)
                    .unwrap(),
                bs[!x as usize]
            );
        }
        let ub = b.unary_in(ctx.cons_ctx()).unwrap();
        let bb = ub.const_over_in(b.clone(), ctx.cons_ctx()).unwrap();
        let case_ub = b.case_in(bb, ctx.cons_ctx()).unwrap();
        let and = case_ub
            .app_in(af.clone(), ctx.cons_ctx())
            .unwrap()
            .app_in(ib.clone(), ctx.cons_ctx())
            .unwrap();
        let or = case_ub
            .app_in(ib.clone(), ctx.cons_ctx())
            .unwrap()
            .app_in(at.clone(), ctx.cons_ctx())
            .unwrap();
        let xor = case_ub
            .app_in(ib.clone(), ctx.cons_ctx())
            .unwrap()
            .app_in(not.clone(), ctx.cons_ctx())
            .unwrap();
        let eq = case_ub
            .app_in(not.clone(), ctx.cons_ctx())
            .unwrap()
            .app_in(ib.clone(), ctx.cons_ctx())
            .unwrap();
        let imp = case_ub
            .app_in(at.clone(), ctx.cons_ctx())
            .unwrap()
            .app_in(ib.clone(), ctx.cons_ctx())
            .unwrap();
        for x in [true, false] {
            for y in [true, false] {
                assert_eq!(
                    and.norm_applied_cons_in(
                        &mut vec![bs[x as usize].clone(), bs[y as usize].clone()],
                        &mut 10,
                        &mut ctx
                    )
                    .unwrap(),
                    bs[(x && y) as usize]
                );
                assert_eq!(
                    or.norm_applied_cons_in(
                        &mut vec![bs[x as usize].clone(), bs[y as usize].clone()],
                        &mut 10,
                        &mut ctx
                    )
                    .unwrap(),
                    bs[(x || y) as usize]
                );
                assert_eq!(
                    xor.norm_applied_cons_in(
                        &mut vec![bs[x as usize].clone(), bs[y as usize].clone()],
                        &mut 10,
                        &mut ctx
                    )
                    .unwrap(),
                    bs[(x ^ y) as usize]
                );
                assert_eq!(
                    eq.norm_applied_cons_in(
                        &mut vec![bs[x as usize].clone(), bs[y as usize].clone()],
                        &mut 10,
                        &mut ctx
                    )
                    .unwrap(),
                    bs[(x == y) as usize]
                );
                assert_eq!(
                    imp.norm_applied_cons_in(
                        &mut vec![bs[x as usize].clone(), bs[y as usize].clone()],
                        &mut 10,
                        &mut ctx
                    )
                    .unwrap(),
                    bs[(x || !y) as usize]
                );
            }
        }
    }
}
