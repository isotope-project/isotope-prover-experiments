use super::*;
use indexmap::{map::Entry, IndexMap};

/// A consing context which maps terms to dense indices
#[derive(Debug, Clone, Default)]
pub struct IndexCtx {
    /// The underlying table of consed terms
    table: IndexMap<TermId, (), ahash::RandomState>,
}

impl IndexCtx {
    /// Get the number of elements consed in this context
    #[inline]
    pub fn len(&self) -> usize {
        self.table.len()
    }

    /// Truncate this context to the given length
    #[inline]
    pub fn truncate(&mut self, len: usize) {
        self.table.truncate(len)
    }

    /// Get the index of a term in this context
    #[inline]
    pub fn ix(&self, term: &Term) -> Option<usize> {
        self.table.get_index_of(term)
    }

    /// Get the term corresponding to a given index
    #[inline]
    pub fn id(&self, ix: usize) -> Option<&TermId> {
        Some(self.table.get_index(ix)?.0)
    }

    /// Get a term's index, or insert it
    ///
    /// Return the index along with whether the term is a new insertion
    #[inline]
    pub fn insert(&mut self, term: TermRef<'_>) -> (usize, bool) {
        match self.table.entry(term.into_owned()) {
            Entry::Occupied(o) => (o.index(), false),
            Entry::Vacant(v) => {
                let ix = v.index();
                v.insert(());
                (ix, true)
            }
        }
    }
}

impl ConsCtx for IndexCtx {
    type ConsCtx = IndexCtx;

    #[inline]
    fn is_consed(&self, term: &TermRef<'_>) -> bool {
        if let Some(cons) = self.try_cons(&**term) {
            cons.ptr_eq(term)
        } else {
            false
        }
    }

    #[inline]
    fn try_cons(&self, term: &Term) -> Option<&TermId> {
        self.id(self.ix(term)?)
    }

    #[inline]
    fn shallow_cons(&mut self, id: TermRef<'_>) -> Option<TermId> {
        let ptr = id.as_ptr();
        let ix = self.insert(id);
        let result = self.id(ix.0).unwrap();
        if result.as_ptr() == ptr {
            None
        } else {
            Some(result.clone())
        }
    }

    #[inline]
    fn pointer_exact(&self) -> bool {
        true
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self
    }

    #[inline]
    fn uncons(&self, code: Code) -> Option<TermId> {
        Some(self.table.get_key_value(&code)?.0.clone())
    }
}
