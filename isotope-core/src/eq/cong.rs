/*!
A congruence-closure based equality context, which also serves as a consing context
*/
use congruence::{CongruenceClosure, Pending, UnionFind, UseSet};

use super::*;

//TODO: implement congruence for things which are not applications

/// A congruence-closure based equality context, which also serves as a consing context
#[derive(Debug, Clone, Default)]
pub struct CongruenceCtx {
    /// The underlying hash-consing context for terms, which also maps them to dense numerical IDs
    dsf: ForestCtx,
    /// The congruence structure on terms
    cc: CongruenceClosure<TermId>,
    /// The usage set for the congruence structure on terms
    us: UseSet<usize>,
    /// The pending queue, cached to avoid re-allocation
    cs: Pending<usize>,
}

impl CongruenceCtx {
    /// Get the number of elements consed in this context
    #[inline(always)]
    pub fn len(&self) -> usize {
        self.dsf.len()
    }

    /// Temporarily access this congruence context's underlying consing context
    #[inline(always)]
    pub fn borrow_cons(&mut self) -> CongruenceCons {
        CongruenceCons {
            len: self.dsf.len(),
            ctx: &mut self.dsf,
        }
    }

    /// Get the index of a term in this context
    #[inline(always)]
    pub fn ix(&self, term: &Term) -> Option<usize> {
        self.dsf.ix(term)
    }

    /// Get the term corresponding to a given index
    #[inline(always)]
    pub fn id(&self, ix: usize) -> Option<&TermId> {
        self.dsf.id(ix)
    }

    /// Get a term's index, or insert it
    ///
    /// Return the index along with whether the term is a new insertion
    #[inline]
    pub fn insert(&mut self, term: TermRef<'_>) -> (usize, bool) {
        let (ix, new) = self.dsf.insert(term.clone());
        if new {
            // New insertion, so have to define it as an equation
            //TODO: fixme!
            self.cc
                .equation(term.into_owned(), &mut self.dsf, &mut self.us, &mut self.cs)
        }
        (ix, new)
    }

    /// Mark two terms in this context as congruent
    #[inline]
    pub fn merge(&mut self, left: usize, right: usize) {
        self.cc
            .merge(left, right, &mut self.dsf, &mut self.us, &mut self.cs);
    }
}

/// Temporarily access a congruence context's underlying consing context
#[derive(Debug)]
pub struct CongruenceCons<'a> {
    ctx: &'a mut ForestCtx,
    len: usize,
}

impl CongruenceCons<'_> {
    /// Get the underlying consing context
    #[inline(always)]
    pub fn as_cons(&mut self) -> &mut impl ConsCtx {
        &mut *self.ctx
    }
}

impl Drop for CongruenceCons<'_> {
    #[inline]
    fn drop(&mut self) {
        self.ctx.truncate_unchecked(self.len)
    }
}

impl ConsCtx for CongruenceCtx {
    type ConsCtx = Self;

    #[inline(always)]
    fn is_consed(&self, term: &TermRef<'_>) -> bool {
        self.dsf.is_consed(term)
    }

    #[inline(always)]
    fn try_cons(&self, term: &Term) -> Option<&TermId> {
        self.dsf.try_cons(term)
    }

    #[inline]
    fn shallow_cons(&mut self, id: TermRef<'_>) -> Option<TermId> {
        match self.insert(id) {
            (_, true) => None,
            (ix, false) => Some(self.dsf.id(ix).unwrap().clone()),
        }
    }

    #[inline(always)]
    fn pointer_exact(&self) -> bool {
        self.dsf.pointer_exact()
    }

    #[inline]
    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self
    }
}

impl EqCtxMut for CongruenceCtx {
    type EqCtxMut = Self;

    #[inline(always)]
    fn ignore_irr(&self) -> bool {
        true
    }

    #[inline]
    fn try_eq_mut(
        &mut self,
        left: &Term,
        right: &Term,
        _fail_early: bool,
    ) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            return Ok(Some(true));
        }
        //TODO: think about this...
        let cc_eq = match (self.ix(left), self.ix(right)) {
            (Some(left_ix), Some(right_ix)) => self.dsf.node_eq(left_ix, right_ix), //TODO: optimize this case to quickly deduce `None`
            (Some(left_ix), None) => self.cc.expr_cong(right, left_ix, &self.dsf),
            (None, Some(right_ix)) => self.cc.expr_cong(left, right_ix, &self.dsf),
            (None, None) => false,
        };
        //TODO: false case
        if cc_eq {
            Ok(Some(true))
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn eq_ctx_mut(&mut self) -> &mut Self::EqCtxMut {
        self
    }
}

impl EqCtx for CongruenceCtx {
    type EqCtx = Self;

    #[inline]
    fn try_eq(&self, left: &Term, right: &Term, fail_early: bool) -> Result<Option<bool>, ()> {
        if left as *const _ == right as *const _ {
            return Ok(Some(true));
        }
        let cc_eq = match (self.ix(left), self.ix(right)) {
            (Some(left_ix), Some(right_ix)) => {
                //TODO: optimize this case to quickly deduce `None`
                if self.dsf.node_eq(left_ix, right_ix) {
                    return Ok(Some(true));
                } else if fail_early {
                    return Ok(None);
                } else {
                    false
                }
            }
            (Some(left_ix), None) => self.cc.expr_cong(right, left_ix, &self.dsf),
            (None, Some(right_ix)) => self.cc.expr_cong(left, right_ix, &self.dsf),
            (None, None) => false,
        };
        //TODO: false case
        if cc_eq {
            Ok(Some(true))
        } else {
            Err(())
        }
    }

    #[inline(always)]
    fn eq_ctx(&self) -> &Self::EqCtx {
        self
    }
}
