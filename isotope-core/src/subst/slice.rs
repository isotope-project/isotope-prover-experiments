/*!
Substitute a slice of variables, with a given base
*/
use super::*;

/// Substitute a slice of variables, with a given base
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct SubstSlice<'a, C> {
    /// The slice of variables to substitute, with variable 0 *last*
    pub substs: &'a [TermId],
    /// The substitution base
    pub base: VarIx,
    /// The underlying consing context
    pub cons: C,
}

//TODO: fixme

impl<C> SubstCtx for SubstSlice<'_, C>
where
    C: ConsCtx,
{
    type ConsCtx = C::ConsCtx;

    #[inline]
    fn is_null(&self) -> bool {
        self.substs.is_empty()
    }

    fn try_subst(&mut self, term: &Term) -> Result<Option<Option<TermId>>, Error> {
        if term.fv().max() < self.base {
            Ok(Some(None))
        } else {
            Ok(None)
        }
    }

    fn subst_ix(
        &mut self,
        ix: VarIx,
        base_ty: Option<&TermId>,
        ty: Option<&TermId>,
    ) -> Result<Option<TermId>, Error> {
        if ix < self.base {
            Ok(None)
        } else if (ix - self.base) as usize >= self.substs.len() {
            //TODO: optimize case where ty == base_ty?
            let shift = self.substs.len() as VarIx;
            Ok(Some(
                Var::try_new(
                    ix - shift,
                    base_ty.subst_cons(self)?,
                    ty.subst_cons(self)?,
                )?
                .into_id_in(self.cons.cons_ctx()),
            ))
        } else {
            Ok(Some(
                self.substs[self.substs.len() - (ix - self.base) as usize - 1].shifted(
                    self.base as SVarIx,
                    0,
                    self.cons.cons_ctx(),
                )?,
            ))
        }
    }

    fn push_param(&mut self, _ty: Option<&TermId>) -> Result<(), Error> {
        self.push_params(1)
    }

    fn pop_param(&mut self) -> Result<(), Error> {
        self.pop_params(1)
    }

    fn push_params(&mut self, n: VarIx) -> Result<(), Error> {
        self.base = self.base.checked_add(n).ok_or(Error::VarIxOverflow)?;
        Ok(())
    }

    fn pop_params(&mut self, n: VarIx) -> Result<(), Error> {
        self.base = self.base.checked_sub(n).ok_or(Error::ParamUnderflow)?;
        Ok(())
    }

    fn cons_ctx(&mut self) -> &mut Self::ConsCtx {
        self.cons.cons_ctx()
    }
}
