/*!
# isotope-core
An implementation of the core `isotope` language
*/
#![forbid(missing_debug_implementations, missing_docs, unsafe_code)]

pub mod cast;
pub mod cons;
pub mod eq;
pub mod error;
pub mod eval;
pub mod subst;
pub mod term;
pub mod termination;
pub mod typing;
pub mod util;

#[cfg(feature="syntax")]
pub mod syntax;

pub use error::Error;
pub use term::{Term, TermId, Value};

//TODO: prelude...
use cast::*;
use cons::*;
use eq::*;
use eval::*;
use subst::*;
use term::*;
use typing::*;
use util::argv::*;
use util::bloom::*;
use util::sparse_stack::*;
use util::*;

pub use util::bloom::SVarIx;
pub use util::bloom::VarIx;

// Imports
use ahash::AHasher;
use arrayvec::ArrayVec;
use either::Either;
use log::*;
use smallvec::{smallvec, SmallVec};
use std::cmp::Ordering;
use std::fmt::{self, Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::*;
