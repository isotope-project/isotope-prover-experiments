/*!
Prettyprint `isotope` terms to an AST
*/
use std::str::FromStr;

use crate::*;
use ast::Arc;
use ast::Expr;
use ast::SmolStr;
use isotope_core_ast as ast;

mod builder;
mod to_ast;

pub use builder::Error;
pub use builder::*;
pub use to_ast::*;

#[cfg(feature = "parser")]
pub use isotope_core_parser as parser;

#[cfg(feature = "parser")]
impl FromStr for TermId {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match parser::expr(s).map_err(|_| Error::ParseError) {
            Ok(("", expr)) => {
                let mut builder = Builder::default();
                builder.expr(&expr, None, &mut ())
            }
            _ => Err(Error::ParseError),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_expr_parsing() {
        assert_eq!("#U0".parse::<TermId>().unwrap(), Universe::with_level(0));
        assert_eq!(
            "λx: #U0 => x".parse::<TermId>().unwrap(),
            Universe::with_level(0).into_id().id_fn().unwrap()
        );
    }
}
